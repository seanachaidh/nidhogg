import tkinter as tk
from . import BookWindow

class MainWindow(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.root = master
        self.pack()
        self.create_widgets()
    def create_widgets(self):
        self.book_but = tk.Button(self, text="Boeken", command=self.openbooks)
        self.book_but.pack(side="top")
        
        self.graph_button = tk.Button(self, text="Genen", command=self.opengenes)
        self.graph_button.pack(side='top')

        self.quit = tk.Button(self, text="Afsluiten", command=self.root.destroy)
        self.quit.pack(side='bottom')
    
    def openbooks(self):
        #Just for the side-effect
        BookWindow.BookWindow(self)
        
    def opengenes(self):
        tk.messagebox.showinfo(message='nog niet gemaakt')
