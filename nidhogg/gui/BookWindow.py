import tkinter as tk
from . import Table
from ..utils import ebooks
from ..utils import wordlist

class BookWindow(tk.Toplevel):
    def __init__(self, master=None):
        super().__init__(master=master)
        self.wm_title('Toegevoegde boeken')
        self.add_widgets()
    def add_widgets(self):
        self.menu = tk.Menu(master=self)
        file_menu = tk.Menu(self.menu)
        file_menu.add_command(label='Voeg toe', command=self.add_book)
        self.menu.add_cascade(label='Bestand', menu=file_menu)
        
        self['menu'] = self.menu

        self.create_book_table()


    def create_book_table(self):
        #hier de boeken ophalen
        words = wordlist.WordList('word_data')
        boeken = ebooks.EbookCorpus('small_corpus', words)
        for i in range(len(boeken)):
            current = boeken[i]
            current_title = tk.Label(self, text=current.meta.title)
            current_author = tk.Label(self, text=current.meta.author)

            current_title.grid(row=i, column=0)
            current_author.grid(row=i, column=1)
    def add_book(self):
        tk.messagebox.showinfo(message='nog niet gemaakt')

