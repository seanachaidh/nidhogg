from copy import deepcopy
from itertools import chain
from . import general
# from .MbtPipe import MBTPipe, MiddleDutchTagger, MiddleDutchLemma
import pandas as pd

class OnlyMeta(general.BookI):
    def __init__(self, filename=None):
        super().__init__(filename)
        self.meta = general.AttrDict()
    def add_value(self, key, val):
        self.meta[key] = val
    def __getitem__(self, key):
        return self.meta[key]
    def __setitem__(self, key, value):
        self.add_value(key, value)

    @classmethod
    def create_with_vals(cls, vals):
        retval = cls(None)
        for key, val in vals.items():
            retval.add_value(key, val)
        return retval

class ExcelCorpus(general.CorpusI):
    def __init__(self, filename):
        self.myframe = pd.read_excel(filename, engine="odf")
        self.records = self.myframe.to_dict('records')
        self.books = [OnlyMeta.create_with_vals(x) for x in self.records]

class SimpleBook(general.BookI):

    def __getstate__(self):
        d = dict(self.__dict__)
        del d['lemmatiser']
        del d['tagger']
        return d

    def _extract_metadata(self, handle):
        #Expect the metadata to be the first line
        handle.seek(0)
        myline = handle.readline()
        title, author, publisher = myline.split('|')
        self.meta = general.MetaData(author, title, publisher)
    
    def _extract_sentences(self, handle):
        retval = []
        handle.seek(0)
        next(handle)
        for line in handle:
            retval.append(line)
        return retval

    def __init__(self, filename, lang='dutch'):
        super().__init__(filename)
        handle = open(self.filename, 'r')
        self._extract_metadata(handle)
        self.tagger = None # Dutch specific
        self.lemmatiser = None
        
        self.tokenize()
        self.serialize()

    def _get_lemma(self):
        self.lemma = []
        len_tokens = len(self.tokens)
        for i, tokenpair in enumerate(self.tokens):
            percent = 100 * (i/len_tokens)
            val = self.lemmatiser.get_lemma(*tokenpair)
            print("Completed %d%%" % percent, end='\r')
            self.lemma.append(val[0])
        print()

    def tokenize(self):
        mys = self._extract_sentences(open(self.filename, 'r'))
        self.sentences = self.tagger.tag_sents(mys)
        self.raw = list(chain(*self.sentences))
        self.tokens = deepcopy(self.raw) # Er zijn in deze teksten geen leestekens
        self.only_tokens = [t[0].lower() for t in self.tokens]
        self.only_nouns = [t for t in self.tokens if t[1] == 'NOUN']
        self._get_lemma()

class SimpleCorpus(general.CorpusI):
    def __init__(self, directory, wlist = None):
        super().__init__(directory, wlist, '.txt', SimpleBook)
