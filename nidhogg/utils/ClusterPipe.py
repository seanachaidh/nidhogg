import pipes, tempfile, re
import pandas as pd
from itertools import chain
from ..utils.general import CorpusI
import tempfile

from operator import itemgetter

class ClusterPipe:
    @property
    def cluster_amount(self):
        if self._executed:
            return len(self.clusterids)
        else:
            return -1
    
    @property
    def executed(self):
        return self._executed

    @property
    def number_clusters(self):
        if self.clusterids is None:
            return 0
        else:
            return len(self.clusterids)

    def split_lines(self, cid, lines):
        raise NotImplementedError

    def _count_classes(self, clusterid: str):
        c = {}
        cluster = self.clusters[clusterid]
        for cl in cluster:
            book = cl['book']
            attr_book = getattr(book.meta, self.attribute, None)
            count = c.get(attr_book, 0)
            count += 1
            c[attr_book] = count
        return c

    def all_possible_labels(self):
        # set(map(lambda x: x['book'].get_meta_attribute(self.attribute), map(lambda y: y['book'], self.clusters.values())))
        result = set()
        for i in self.clusterids:
            current = self.clusters[i]
            for c in current:
                book_val = c['book'].get_meta_attribute(self.attribute)
                result.add(book_val)

        return result

    def dominant_classes(self):
        class_counts = [(i, self._count_classes(i)) for i in self.clusterids]
        max_classes = [max(x.items(), key=itemgetter(1)) for x in map(itemgetter(1), class_counts)] #TODO hier is er een fout
        all_authors = self.all_possible_labels()

        dominant_class_ids = {}

        for i, cl in enumerate(max_classes):
            name, count = cl
            if name in dominant_class_ids:
                classcount = dominant_class_ids[name][1]
                if classcount < count:
                    dominant_class_ids[name] = (self.clusterids[i], count)
            else:
                dominant_class_ids[name] = (self.clusterids[i], count)
        #TODO: assigning virtual cluster -1 to non dominants.

        for author in all_authors:
            if not author in dominant_class_ids:
                dominant_class_ids[author] = ('Cluster -1', 0)

        return dominant_class_ids

    def search_dominant_with_id(self, doms, id):
        for key, val in doms.items():
            myid, count = val
            if myid == id:
                return (myid, key, count)
        else:
            return None
    def cluster_labels(self):
        if self._cluster_labels is None:

            result = {}
            mydoms = self.dominant_classes()
            for i, name in enumerate(self.clusterids, start=1):
                dominant = self.search_dominant_with_id(mydoms, name)
                if dominant is None:
                    newname = 'unknown_' + str(i)
                    # result.append((name, newname))
                    result[name] = newname
                else:
                    result[dominant[0]] = dominant[1]
            self._cluster_labels = result
            return result
        else:
            return self._cluster_labels

    def to_dataframe(self):
        return pd.DataFrame.from_records(self.clusterdict, exclude=['book'])

    def make_lines(self, corpus: CorpusI = None):
        raise NotImplementedError
    
    def get_cluster_result(self, clusterid):
        raise NotImplementedError
    
    def execute(self, corpus: CorpusI = None):
        raise NotImplementedError

    def find_most_common_class(self):
        return map(lambda x: max(self._count_classes(x)), self.clusterids)

    def __init__(self, fasta, attribute: str = 'author', debug=False):
        self.clusterids = None
        self.attribute = attribute
        self.result = None
        self.fasta = fasta
        self.output = tempfile.NamedTemporaryFile(suffix='.clstr', delete=True)

        self.clusters = []
        self.clusterdict = {}
        self._cluster_labels = None

        self._executed = False
        self._pipe = pipes.Template()
        self._pipe.debug(debug)

