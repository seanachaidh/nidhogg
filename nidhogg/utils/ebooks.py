import os
import shutil
import pickle
import uuid
import unittest

import ebooklib
from ebooklib import epub

from urllib.parse import urlparse

from copy import deepcopy

from lxml import etree
from io import StringIO

from xml.etree import ElementTree as ET

from zipfile import ZipFile
from operator import attrgetter
from . import general

# NLTK modules
from nltk import tokenize
from nltk import wordpunct_tokenize
from nltk import pos_tag

from unidecode import unidecode

from bs4 import BeautifulSoup
from selectolax.parser import HTMLParser as FastParse
import requests, re

#Variablele voor het filteren van leestekens
leestekens = ['?', '!', '"', "'", ';', ',', '.', ':']

def content_for_element(elem):
    return elem.text + ''.join(etree.tostring(e) for e in elem)

def ebook_id_to_url(ebook_id: int, root: str) -> str:
    pass

# Bron van Reddit: https://www.reddit.com/r/learnpython/comments/1y1kit/my_script_to_download_top_100_books_from_project/
def download_corpus(mirrordir, outdir):
    '''This script will download Top 100 books of last 30 days from Project 
    Gutenberg and saves them with appropriate file name'''
    base_url = mirrordir + '/{book_id}/pg{book_id}.epub'
    r = requests.get('http://www.gutenberg.org/browse/scores/top')
    soup = BeautifulSoup(r.text)
    h2 = soup.find(id='books-last30')
    ol = h2.next_sibling.next_sibling
    maxcount = len(ol.find_all('a'))
    begincount = 0
    for a_tag in ol.find_all('a'):
        begincount += 1
        m = re.match(r'(.*)(\(\d+\))', a_tag.text)
        file_name = m.group(1).strip()

        file_name = re.sub(r'[^\w\-_\. ]', '', file_name)
        file_name = file_name.replace(' ', '_', -1)
        file_name += '.epub'

        m = re.match(r'/ebooks/(\d+)', a_tag.get('href'))
        book_id = m.group(1)
        url = base_url.format(book_id=book_id)
        r = requests.get(url)

        if not os.path.isdir(outdir):
            os.mkdir(outdir)
        outfile = os.path.join(outdir, file_name)
        print('Downloading: {f_url} ({f_current}/{f_end})'.format(f_url=url, f_current=begincount, f_end=maxcount))
        print('outfile:', outfile)

        f = open(os.path.join(outdir, file_name), 'wb')
        f.write(r.content)
        f.close()


# TODO: Test deze functie
def flatten_element(element):
    mylist = list(element.itertext())
    return ''.join(mylist)

def has_to_filter(element):
    if element.parent.name in ['style', 'script', '[document]', 'head', 'title']:
        return False
    elif re.match('<!--.*-->', str(element.encode('utf-8'))):
        return False
    elif not element.strip():
        return False
    return True

class structTOC:
    def __init__(self, id, text, content, order):
        self.id = id
        self.text = text
        self.order = order
        self.content = content


class EPUB(general.BookI):

# https://www.quora.com/How-can-I-extract-only-text-data-from-HTML-pages
    def _flatten_document(self, item: ebooklib.epub.EpubHtml):
        # mybody_content = item.get_body_content().decode('utf-8')
        mybody_content = item.get_body_content()
        soup = BeautifulSoup(mybody_content, features='lxml')
        data = soup.findAll(text=True)

        # htmlfile = etree.HTML(mybody_content)
        result_tmp = filter(has_to_filter, data)
        result = list(map(lambda x: x.strip(), result_tmp))

        return result

    def _get_text_selectolax(self, epubitem):
        html = epubitem.get_body_content()
        tree = FastParse(html)

        if tree.body is None:
            return None

        for tag in tree.css('script'):
            tag.decompose()
        for tag in tree.css('style'):
            tag.decompose()

        text = tree.body.text(separator='\n')
        return re.sub(r"[?!\"';,.:\(\)]", '', text.lower())

    def tokenize(self):

        def split_leestekens(tokens: list) -> list:
            result = []
            tmplist = []
            for tok in tokens:
                
                if not check_leesteken(tok):
                    tmplist.append(tok.lower())
                else:

                    tmplist.append(tok.lower())
                    result.append(deepcopy(tmplist))
                    tmplist = []

            return result

        def check_leesteken(tok):
            return any(map(lambda x: x in tok, leestekens))

        #tokenizes a complete book
        book = []
        myitems = [x for x in self.ebookfile.get_items() if x.get_type() == ebooklib.ITEM_DOCUMENT]
        for i, c in enumerate(myitems):
            print('scanning', self.meta.title)
            print('hoofdstuk', (i+1), 'van', len(myitems))
            lines = self._get_text_selectolax(c)

            # self.lines += lines

            # chaptertext = unidecode(' '.join(lines))
            book += self.tokenizer.tokenize(lines)
        # Verwijder leestekens

        self.raw = book
        self.tokens = book
        # self.sentences = split_leestekens(book)
        self.only_tokens = self.tokens

        #Laten we deze voorlopig nemen voor het nakijken van topics
        # self.only_nouns = [t[0].lower() for t in self.tokens if t[1] in ['NN', 'NNS', 'NNP']]

    def _get_metadata_item(self, namespace, item):
        retval = self.ebookfile.get_metadata(namespace, item)
        if not retval:
            return 'None'
        else:
            return retval[0][0]

    def __init__(self, filename, collection):
        super().__init__(filename, collection)

        self.ebookfile = ebooklib.epub.read_epub(filename)

        self.tokenizer = tokenize.treebank.TreebankWordTokenizer()
        
        self.meta = general.MetaData(self._get_metadata_item('DC', 'creator'),
            self._get_metadata_item('DC', 'title'),
            self._get_metadata_item('DC', 'publisher'))

        self.tokenize()
        #Serialisatie gebeurt altijd
        self.serialize()

class EbookCorpus(general.CorpusI):
    def __init__(self, directory, wlist = None):
        super().__init__(directory, wlist=wlist, extension='.epub', bookclass=EPUB)
