import pipes, tempfile, re
import pandas as pd
from itertools import chain
from ..utils.general import CorpusI
from .ClusterPipe import ClusterPipe


from operator import itemgetter

class CDHITPipe(ClusterPipe):

    extract_id = re.compile(r"(?<=Cluster\s)\d+")

    def split_lines(self, cid, lines):
        x = lines.split('\n')
        retval = []
        for l in filter(lambda z: z != '', x):
            items = re.split(r'\s+', l)
            if items[-1] == '*':
                percent = 100
            else:
                percent = float(items[-1][2:-1])
            idnum = int(items[0])
            extra = re.sub(r'\W+', '', items[1])
            if items[2].endswith('...'):
                title = items[2][1:-3]
            else:
                title = items[2][:-3]
            tmpval = {
                'idnum': idnum,
                'extra': extra,
                'title': title,
                'percent': percent,
                'clusterid': 'Cluster ' + str(cid)
            }
            retval.append(tmpval)
        return retval
        
    def make_lines(self, corpus: CorpusI = None):
        myclusters = (self.get_cluster_result(i) for i in map(lambda x: int(self.extract_id.findall(x)[0]), self.clusterids))
        mylines = list(chain(*map(lambda y: self.split_lines(*y), enumerate(myclusters))))
        if corpus:
            for line in mylines:
                mybook = corpus.get_book_by_name(line['title'])
                line.update({
                    'book': mybook,
                    'real_author': mybook.meta.author
                })

        return mylines

    def get_cluster_result(self, clusterid):
        cluster = '>Cluster ' + str(clusterid)
        reg = re.compile(r"(?<=" + cluster + r"\n)(.*\n)*?(?=(>Cluster|$))")

        match = reg.search(self.result.decode("utf-8")).group(0) # Geen idee over deze code
        return match

    def execute(self, corpus: CorpusI = None):
        self._writefile.write(open(self.fasta).read())
        self._writefile.close()
        self.output.seek(0)
        self.result = self.output.read() #Add a newline so that the regex expressions will work

        self.clusterids = re.findall(r"(?<=>)Cluster\s\d+", self.result.decode('utf-8'))

        if corpus:
            self.clusterdict = self.make_lines(corpus)
        else:
            self.clusterdict = self.make_lines()
        # self.clustids = set([x['clusterid'] for x in self.clusterdict])
        
        clusters = dict()
        for i in self.clusterids:
            mylist = list(filter(lambda j: j['clusterid'] == i, self.clusterdict))
            clusters[i] = mylist

        self.clusters = clusters
        if corpus:
            labels = self.cluster_labels()
            for c in self.clusterdict:
                c['label'] = labels[c['clusterid']]
                c['filename'] = c['book'].filename

        self._executed = True


    def determine_world_length(self, t):
        # Taken straight from the manual
        """
        -n 5 for thresholds 0.7 ~ 1.0
        -n 4 for thresholds 0.6 ~ 0.7
        -n 3 for thresholds 0.5 ~ 0.6
        -n 2 for thresholds 0.4 ~ 0.5
        """

        check_dict = {
            5: (0.7, 1),
            4: (0.6, 0.7),
            3: (0.5, 0.6),
            2: (0.4, 0.5)
        }

        for key, val in check_dict.items():
            if t >= val[0] and t <= val[1]:
                return key
        else:
            return None 

    def __init__(self, fasta, debug=False, attribute: str = 'author', accurate=False, threshold=0.9):
        super().__init__(fasta, attribute, debug)
        outstr = "$(echo $OUT | cut -d '.' -f 1)"
        params = (str(int(accurate)),threshold, outstr)

        self._pipe.append('cd-hit -T 2 -d 0 -g %s -n 3 -c %s -i $IN -o %s' % params, 'ff')

        self._writefile = self._pipe.open(self.output.name, 'w')
    
