import pipes, tempfile, re, os
from nltk import tag
from io import StringIO
from itertools import chain

import timbl

def transform_sent(sent):
    return ' '.join(sent) + '<utt>'

class MBTPipe:

    def __init__(self, myinput, taggerfile):
        self.myinput = myinput
        self.taggerfile = taggerfile

        self._mypipe = pipes.Template()
        self._prepare()
    
    def _prepare(self):
        self._mypipe.append('mbt -s %s' % self.taggerfile, '--')
        self._tmpfile = tempfile.NamedTemporaryFile(mode='w', delete=False) # Misschien beter in utf8?
    def encode(self):
        print("using temporary file: %s" % self._tmpfile.name)
        f = self._mypipe.open(self._tmpfile.name, 'w')
        f.write(self.myinput.read())
        f.close()
    def putout(self):
        print("putting out values")
        to_put_out = open(self._tmpfile.name, 'r').read()
        return to_put_out

class MiddleDutchLemma:
    def __init__(self):
        self.lemmatiser =  timbl.TimblClassifier(os.path.join('notebooks', 'lemma-class'), "-a 0 -k 1 -mL +D")

    def get_lemma(self, word, tag):
        val = self.lemmatiser.classify((word, tag))
        return val[0]

class MiddleDutchTagger(tag.api.TaggerI):

    def _transform_tags(self, iofile):
        retval = []

        tagger = MBTPipe(iofile, os.path.join('notebooks', 'output_tag', 'output.frog.settings'))
        tagger.encode()
        tag_text = tagger.putout()

        tag_sent = map(lambda x: x[:-5], tag_text.strip().split('\n')) # Hiet <utt> weggelaten worden
        
        for t in tag_sent:
            first_split = t.strip().split(' ')
            taglist = list(chain(*[re.split(r"\/{1,2}", t) for t in first_split]))
            taglist = [x.strip() for x in taglist] # Niet echt efficiënt maar ik kan voorlopig niks beters vinden

            #Dit werkt omdat de lijst bestaat uit achtereenvolgens een woord en een tag
            splitted_taglist = list(zip(taglist[0::2], taglist[1::2]))
            retval.append(splitted_taglist) # zou in een aparte functie kunnen maar dit is beter leesbaar
        return retval
        
    def tag_sents(self, sents):
        myio = StringIO()
        for s in sents:
            s_string = s + '<utt>'
            print(s_string, file=myio)
        myio.seek(0) # Ik weet niet of dit nodig is. Print lijkt de cursor niet te verplaatsen
        mytags = self._transform_tags(myio)
        
        return mytags

    def tag(self, tokens):
        # Beschouw de lijst met tokens als 1 zin
        myio = StringIO()
        s_string = ' '.join(tokens) +  '<utt>'
        print(s_string, file=myio)
        myio.seek(0)
        mytags = self._transform_tags(myio)
        
        return mytags[0]