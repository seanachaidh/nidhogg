from lxml import etree
from lxml import html

import requests, os, sys

from io import StringIO
import urllib

class DBNLPage:

    @property
    def current_page(self):
        numelement = self.root.xpath('//ul[@class="pagination sans"]/li[@class="details"]')[0].text.strip()
        return int(numelement.split('/')[0].strip())
    @property
    def nextpage(self):
        if self.current_page == self.numpages:
            return None
        else:
            next_element = self.root.xpath('//ul[@class="pagination sans"]/li/a[.="Volgende"]')[0]
            return DBNLPage('https://dbnl.org' + next_element.attrib['href'])
    @property
    def booklist(self):
        # Geeft de boekenlijst van de huidige pagina. We halen enkele elektronische boeken op
        elemlist = self.root.xpath('//table[@id="titles"]/tr/td[1]/div/a')
        return map(lambda x: x.attrib['href'], filter(lambda x: x.attrib['href'].endswith('.epub'), elemlist))

    @property
    def numpages(self):
        numelement = self.root.xpath('//ul[@class="pagination sans"]/li[@class="details"]')[0].text.strip()
        return int(numelement.split('/')[1].strip())

    def _load_url(self):
        r = requests.get(self.url)
        if r.status_code == 200:
            self.tree = html.parse(StringIO(r.text), parser=self.parser)
            self.root = self.tree.getroot()
        else:
            raise Exception('Status DBNL not right')
    def __init__(self, url):
        self.url = url
        self.parser = html.HTMLParser(encoding='utf8')
        self._load_url()

    def all_urls(self):
        return map(lambda x: 'https://dbnl.org' + x, self.book_iterator())

    def book_iterator(self):
        current = self
        while True:
            for x in current.booklist:
                yield x
            if current.nextpage is None:
                break
            else:
                current = current.nextpage

    def get_needed_data(self):
        retval = 0
        for i, item in enumerate(self.book_iterator(), 1):
            print('Calculating:', i)
            response = requests.get('https://dbnl.org' + item, stream=True)
            response_length = response.headers.get('content-length')

            if not response_length is None:
                retval += int(response_length)
        return retval

    def data_mine(self, out):
        if not os.path.isdir(out):
            os.mkdir(out)
        for i, item in enumerate(self.book_iterator(), 1):
            # MISSCHIEN HIER BETER EEN PATH OBJECT GEBRUIKEN

            # filename = out + '/' + item.split('/')[-1
            # ]
            filename = os.path.join(out, item.split('/')[-1])
            print("downloading book", i, filename)
            with open(filename, 'wb') as f:
                response = requests.get('https://dbnl.org' + item, stream=True)
                response_length = response.headers.get('content-length')

                if response_length is None:
                    f.write(response.content)
                else:
                    dl = 0
                    file_len = int(response_length)
                    for data in response.iter_content(chunk_size=4096):
                        dl += len(data)
                        f.write(data)
                        done = int(50*dl/file_len)
                        sys.stdout.write("\r[%s%s]" % ('=' * done, ' ' * (50-done)))
                        sys.stdout.flush()


