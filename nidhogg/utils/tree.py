import matplotlib.pyplot as pyplot
# eerst importeren we dendropy (The higher the score the better)
import dendropy
from dendropy.calculate import treescore
from copy import deepcopy
#To do matrix calculations
import numpy as np
import re


class TreeElement:
    def __init__(self, data=None, parent=None):
        self.children = []
        self._data = data
        self.parent = parent

        if not self.parent is None:
            self.parent.children.append(self)

    @property
    def data(self):
        return self._data

    def __repr__(self):
        if self.data is None:
            return ''
        else:
            return str(self.data)

    @property
    def leaf(self):
        if not self.children:
            return True
        else:
            return False

    def get_all_leaves(self):
        retval = []
        def dfs_func(x):
            if x.leaf:
                retval.append(x)
            return True
        self.dfs(dfs_func)
        return retval

    def get_branches(self):
        result = []
        leaves = self.get_all_leaves()
        for l in leaves:
            current = l
            branch = [current]
            while not current.parent is None:
                current = current.parent
                branch.append(current)
            result.append(branch)
        return result

    def add_children(self, children):
        for c in children:
            self.children.append(c)
            c.parent = self

    def get_path(self): # Heel kostelijke functie
        result = [self.data]
        current = self

        while not current.parent is None:
            result.append(current.parent.data)
            current = current.parent
        return result
    
    @property
    def depth(self):
        retval = 0
        def dfs_func(x):
            nonlocal retval
            retval += 1
            if not x.leaf:
                return True
            else:
                return False
        self.dfs(dfs_func)
        return retval

    @property
    def isRoot(self):
        if self.parent is None:
            return True
        else:
            return False
    
    @property
    def last_child(self):
        if self.parent is None:
            return False
        elif self.parent.children[-1] == self:
            return True
        else:
            return False 

    def to_nested_list(self):
        results = [self]

        children = []

        for c in self.children:
            child_val = c.to_nested_list()
            children += child_val
        
        if children:
            results.append(children)
        return results

    def to_newick_tree(self):
        def repl_fun(x):
            returned = x.group(0)
            return returned[-1]
        def mapfun(x):
            if x == ' ':
                return ''
            elif x == '[':
                return '('
            elif x == ']':
                return ')'
            else:
                return x
        nested_list = str(self.to_nested_list())
        newstr = ''.join(list(map(mapfun, nested_list)))

        newstr = re.sub(r'hidden[0-9]+,\(', repl_fun, newstr)

        return newstr + ';'

    def save_tree(self, filename):
        raise NotImplementedError

    def dfs(self, func, endcall = None):
        '''
            implements depth first search
        '''
        stack = [self] # We push itelf as the first element of the stack so the first node to be checked is root
        path = []
        while stack:
            current = stack.pop()
            path.append(current)
            result = func(current)

            if not result:
                return (path, current)
            else:
                for c in reversed(current.children):
                    stack.append(c)
        if not endcall is None:
            endcall(path)

        return (path,None)

class BookTreeElement(TreeElement):
    def save_tree(self, filename):
        print('saving:', filename)
# Taxons to tree
def list_to_tree(taxlist):
    result = '('
    for i in range(len(taxlist)):
        if i >= (len(taxlist) - 2):
            final1 = taxlist[i]
            final2 = taxlist[i+1]
            toadd = str(final1) + ',' + str(final2) + (')' * (len(taxlist) - 1)) + ';'
            result += toadd
            return result
        else:
            current = taxlist[i]
            result += str(current) + ',('

def neighbourhood(permutation, chars, taxa):
    for i in range(len(permutation)):
        for j in range(len(permutation)):
            if not i == j:
                newlist = deepcopy(permutation)
                
                tmp = newlist[i]
                newlist[i] = newlist[j]
                newlist[j] = tmp

                tmptree = dendropy.Tree.get_from_string(list_to_tree(newlist), 'newick', taxon_namespace=taxa)
                score_list = []
                tmpscore = treescore.parsimony_score(tmptree, chars, gaps_as_missing=False, score_by_character_list=score_list)

                yield (newlist, tmpscore)

#Now we perform the descent. Last parameter is a lambda
def parsimony_iterative_improvement(initial, chars, taxa, end_condition):
    iteration = 0
    
    current = list(deepcopy(initial))
    currentTree = dendropy.Tree.get_from_string(list_to_tree(current), 'newick', taxon_namespace=taxa)
    current = (current, treescore.parsimony_score(currentTree, chars, gaps_as_missing=False, score_by_character_list=None))
    previous = None
    while not end_condition(current, previous, iteration):
        currentlist, currentscore = current
        current_neighbourhood = neighbourhood(currentlist, chars, taxa)
        #First improvement
        for x in current_neighbourhood:
            newscore = x[1]
            if newscore >= currentscore:
                previous = current
                current = x
                break
        iteration += 1
        print('completed iteration:', iteration)
        print('score now:', currentscore)
    return current

def hamming_distance(x, y):
    x_repr = x.__str__()
    y_repr = y.__str__()
    return sum(map(lambda i, j: 0 if i == j else 1, x_repr, y_repr))

def find_smallest_distance(matrix):
    i, j = matrix.shape
    current = None
    
    for n in range(i):
        for m in range(j):
            # Dit kan eigenlijk ook met een AND statement uitgedrukt worden
            if not n == m:
                if current == None:
                    current = (n,m)
                else:
                    currentval = matrix[current]
                    newval = matrix[n,m]
                    if newval < currentval:
                        current = (n,m)
    return current

# Hamming distance amoung the taxa
def create_distances(chars):
    unique = []
    taxon_names = chars.taxon_namespace
    results = np.zeros((len(taxon_names), len(taxon_names)))
    for i in range(len(chars)):
        for j in range(len(chars)):
            if not i == j and (not (i, j) in unique or not (j, i) in unique):
                name1 = taxon_names[i]
                name2 = taxon_names[j]

                cur1 = chars[name1]
                cur2 = chars[name2]
                distance = hamming_distance(cur1, cur2)
                print("afstand tussen {0} en {1}: {2}".format(name1, name2, distance))
                unique.append((i, j))
                unique.append((j, i))
                results.append((cur1, cur2, distance))
    return results

def create_d(chars):
    names = chars.taxon_namespace
    result = np.zeros((len(names), len(names)))
    for i in range(len(names)):
        for j in range(len(names)):
            currenti = names[i]
            currentj = names[j]
            result[i,j] = hamming_distance(currentj, currenti)
    return np.matrix(result)

def create_q(matrix):
    n, m = matrix.shape
    result = np.zeros((n, m))

    for i in range(n):
        for j in range(m):
            d_ij = matrix[i,j]
            sum_i = sum(matrix[i,].getA().flatten())
            sum_j = sum(matrix[j,].getA().flatten())
            qval = (n-2)*d_ij - sum_i - sum_j
            
            result[i,j] = qval
            
    return np.matrix(result)

def update_d(matrix, ab):
    n = matrix.shape[0]
    a, b = ab
    d_ab = matrix[a,b]
    
    #set_trace()
    tmparray_a = matrix[a,:].getA().flatten()
    tmparray_b = matrix[b,:].getA().flatten()
    #set_trace()
    #Overbodige elementen verwijderen
    tmparray_a = np.delete(tmparray_a, [a,b])
    tmparray_b = np.delete(tmparray_b, [a,b])
    
    result = np.delete(matrix, [a,b], 0)
    result = np.delete(result, [a,b], 1)
    
    to_append = np.zeros(n-1)
    
    
    for i in range(n-2):
        b_elem = tmparray_a[i]
        a_elem = tmparray_b[i]
        
        #set_trace()
        final = (1/2) * (a_elem + b_elem - d_ab)
        to_append[i] = final
    
    # Ik weet niet zeker of ik heir beter een insert voor in de plaats zou gebruken
    result = np.append(result, np.matrix(to_append[:-1]), axis=0)
    # Een beetje een vreemde manier van uitdrukken
    result = np.append(result, np.matrix(to_append).transpose(), axis=1)
    
    return result

def not_complete(matrix):
    n = matrix.shape[0]
    if n <= 1:
        return False
    else:
        return True

def list_remove_multiple(li, locs):
    return [li[x] for x in range(len(li)) if not x in locs]


def neighbour_joining(charmatrix):
    names = list(charmatrix.taxon_namespace)
    clusters = [[x] for x in names]
    D = create_d(charmatrix)
    
    while not_complete(D):
        D_q = create_q(D)
        print("QMatrix:")
        print(D_q)
        small = find_smallest_distance(D_q)
        a, b = small
        print("neigbhours:", small)
        
        cluster1 = deepcopy(clusters[a])
        cluster2 = deepcopy(clusters[b])
        clusters = list_remove_multiple(clusters, [a,b])
        
        newcluster = cluster1 + cluster2
        clusters.append(newcluster)
        D = update_d(D, small)  
    return clusters

def better_neighbour_joining(charmatrix):
    current_id = 0

    def fuse_leaves(clusters, ind1, ind2):
        nonlocal current_id

        element1 = clusters[ind1]
        element2 = clusters[ind2]

        current_id += 1
        newelement = TreeElement(data='hidden{0}'.format(current_id))
        newelement.add_children([element1, element2])
        retval = list_remove_multiple(clusters, [ind1, ind2])

        retval.append(newelement)
        return retval


    names = list(charmatrix.taxon_namespace)
    clusters = [BookTreeElement(x) for x in names]
    D = create_d(charmatrix)

    while not_complete(D):
        D_q = create_q(D)
        print("QMatrix:")
        print(D_q)
        
        small = find_smallest_distance(D_q)
        a, b = small
        print('neighbours:', small)
        clusters = fuse_leaves(clusters, a, b)
        D = update_d(D, small)

    return clusters

def nexus_generate_tree(filename, name_for_tree, append_tree=True):
    taxa = dendropy.TaxonNamespace()
    chars = dendropy.DnaCharacterMatrix.get(
        path=filename,
        schema="nexus",
        taxon_namespace=taxa
    )

    # Eerst ordenen we de taxa zodat we een betere volgorde krijgen
    joinretval = better_neighbour_joining(chars)[0]
    
    final_newick = joinretval.to_newick_tree()
    
    # ~ pars_retval = parsimony_iterative_improvement(volgorde, chars, taxa, lambda x, y, z: True if z > 20 and not ((y[1] - x[1]) > 0) else False)
    # ~ taxons, score = pars_retval
    # ~ taxon_names = [str(x) for x in taxons]
    
    # ~ final_newick = list_to_tree(taxon_names)
    
    # ~ treeobj = dendropy.Tree.get_from_string(final_newick, schema='newick', taxon_namespace=taxa)
    
    # ~ score = treescore.parsimony_score(
		# ~ treeobj,
		# ~ chars,
		# ~ gaps_as_missing=False,
		# ~ score_by_character_list=[])
    
    # ~ print('Genrated tree {0} | score {1}'.format(final_newick, score))

    if append_tree:
        with open(filename, 'r+') as f:
            contents = f.readlines()
            for index, line in enumerate(contents):
                if "BEGIN TREES;" in line:
                    contents.insert(index+1, '\tTree {0}={1}\n'.format(name_for_tree, final_newick))
                    break
            else:
                contents.append('\nBEGIN TREES;\n')
                contents.append('\tTree {0}={1}\n'.format(name_for_tree, final_newick))
                contents.append('END;\n')
            f.seek(0)
            f.writelines(contents)
            
# TODO Dit hoeft niet in een klasse
class NexusFile:

    def __init__(self, filename, charactermatrix):
        self.filename = filename
        self.charactermatrix = charactermatrix
        self._dimension_check()

    def _dimension_check(self):
        first_length = len(self.charactermatrix[0][1])
        for taxa, sequence in self.charactermatrix:
            if not len(sequence) == first_length:
                raise RuntimeError('Length incorrect for {0}: {1} expected {2}'.format(taxa, len(sequence), first_length))
    def write_file(self):
        with open(self.filename, 'w') as f:
            print('#NEXUS', file=f)
            self._printout_taxa(f)
            self._printout_matrix(f)

    def _printout_matrix(self, f):
        print('BEGIN CHARACTERS;', file=f)

        print('\tDimensions NChar={0};'.format(len(self.charactermatrix[0][1])), file=f)

        print('\tFormat DataType=DNA Interleave=yes;', file=f)
        print('Matrix', file=f)

        for c in self.charactermatrix:
            print('{0}\t{1}'.format(c[0], c[1]), file=f) # Volgens mij kan ik hier gewoon een unpack doen
        print(';', file=f)
        print('END;', file=f)

    def _printout_taxa(self, f):
        print('BEGIN TAXA;', file=f)
        taxa_list = ' '.join([c[0] for c in self.charactermatrix])
        print('\tDIMENSIONS NTax={0};'.format(len(self.charactermatrix)), file=f)
        print('\tTAXLABELS {0};'.format(taxa_list), file=f)
        print('END;', file=f)
