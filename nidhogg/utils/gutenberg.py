import os, requests, io, sys
from lxml import etree, html
import urllib

class BookShelf:

    @property
    def books(self):
        query_results = self.root.xpath('.//a[starts-with(@title, "ebook:")]/@href')
        return (x.split('/')[-1] for x in query_results)

    def urls_iter(self):
        for b in iter(self.books):
            final_url = self.mirror + '/{book_id}/pg{book_id}.epub'.format(book_id=b)
            yield final_url


    def __init__(self, url):
        self.url = url
        #Hard coded. Dit kan misschien beter
        self.mirror = "http://gutenberg.readingroo.ms/cache/generated"

        self.htmlparser = html.HTMLParser(encoding='utf8')
        self.sourcefile = requests.get(url.geturl()).text

        self.sourcetree = html.parse(io.StringIO(self.sourcefile), parser=self.htmlparser)
        self.root = self.sourcetree.getroot()
    
    def download_all(self, outdir):
        if not os.path.isdir(str(outdir)):
            os.mkdir(str(outdir))
        for i, item in enumerate(self.urls_iter(), 1):
            filename = item.split('/')[-1]
            newpath = os.path.join(outdir, filename)

            print('downloading book {0} to {1}'.format(i, newpath))

            with open(newpath, 'wb') as f:
                response = requests.get(item, stream=True)
                if response.status_code == 200:
                    response_length = response.headers.get('content-length', None)
                    if response_length is None:
                        f.write(response.text)
                    else:
                        file_len = int(response_length)
                        dl = 0
                        buffer = response.iter_content(chunk_size=4096)
                        for data in buffer:
                            dl = dl + len(data)
                            f.write(data)

                            done = int((dl/file_len) * 50)
                            sys.stdout.write('\r[%s%s]' % ('=' *  done, ' ' * (50-done)))
                        print()

                else:
                    print('boek %d: %s niet gevonden' % (i, item))