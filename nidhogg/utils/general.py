import nltk, inspect, uuid, os, pickle
from copy import deepcopy # Gaat dit wel werken?
from itertools import chain
from operator import itemgetter
from dendropy import Tree, Node
import numpy as np
import pandas as pd
import pymongo as mongo
import gridfs
import hashlib
import json

def find_optimal(result_table: pd.DataFrame):
	return max(result_table.itertuples(), key=lambda x: 0.10*x.Purity + 0.90*x.Accuracy)

#Geïnspireerd op stackoverflow
class Singleton(type):
	_instances = {}
	def __call__(cls, *args, **kwargs):
		if not cls in cls._instances:
			cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
		return cls._instances[cls]

class MongoConnection(metaclass=Singleton):
	def __init__(self):
		self._connection = mongo.MongoClient()
		self._database = self.connection["nidhogg"]
		self._fs = gridfs.GridFS(self._database)
		
	@property
	def connection(self):
		return self._connection
	
	@property
	def database(self):
		return self._database
		
	@property
	def fs(self):
		return self._fs
	
class AttrDict(dict):
	def __getattr__(self, key):
		if key in self:
			return self[key]

	def __setattr__(self, key, value):
		self[key] = value

class MetaData:

	def __init__(self, author='None', title='None', publisher='None'):
		self.author = author
		self.title = title
		self.publisher = publisher

# GENERAL INTERFACES

class CorpusI:
	def __init__(self, directory, wlist = None, extension='.txt', bookclass=None):
		conn = MongoConnection()
		database = conn.database
		
		collection_names = database.list_collection_names()
		directory_base = os.path.basename(directory)
		self.collection_name = os.path.basename(directory)
		
		self.books = list()
		if not os.path.isdir(directory):
			raise Exception('Corpus directory ' + directory + ' does not exist')
		else:
			collection = database[directory_base]
			files = os.listdir(directory)
			for f in files:
				if f.endswith(extension):
					book = bookclass.load_book(directory_base, os.path.join(directory, f))
					if not wlist is None:
						book.remove_all_with_wordlist(wlist)
					self.books.append(book)
					

	def __len__(self):
		return len(self.books)
		
	def __getitem__(self, i):
		if type(i) == str:
			return next(filter(lambda x: i in str(x), self.books), None)
		else:
			return self.books[i]

	def __iter__(self):
		for book in self.books:
			to_yield = deepcopy(book)
			to_yield.fetch_gridfs_data()
			
			yield to_yield
	
	def get_text_array(self):
		return [x.text for x in self]

	def get_text_collection(self):
		textlist = [b.NLTK_text() for b in self]
		return nltk.text.TextCollection(textlist)
	
	def all_titles(self):
		return [x.meta.title for x in self.books]

	def get_all_with_meta(self, attr):
		return set(zip(self.books, map(lambda x: x.get_meta_attribute(attr), self.books)))

	def apply_genes(self, genelist, only_features=False, mutate=False):
		gene_instances = [x(self) for x in genelist]
		if mutate and len(gene_instances) != 2:
			raise RuntimeError('Mutatie enkel ondersteund voor twee genen')
		results = list()
		for b in self:
			if only_features:
				dna = []
			else:
				dna = ''
			for gene_instance in gene_instances:
				print('Applying {0} on book: {1}'.format(gene_instance.__class__.__name__, b))
				
				gene_instance.load_data(b)
				if only_features:
					dna += gene_instance.transform_gene(True)
				else:
					dna += gene_instance.transform_gene(False)
			results.append((str(b), dna))
		return results
		
	def get_book_by_name(self, name: str):
		return next(filter(lambda x: str(x) == name, self.books), None)

class BookI:
	def __init__(self, filename, collection):
		#~ self.book_id = uuid.uuid4().hex
		
		self.filename = filename
		self.collection = collection # Misschien deze naam veranderen naar corpus
		
		self.meta = MetaData()

		self.raw = []
		self.tokens = []
		self.sentences = []
		self.only_tokens = []
		self.only_nouns = []
		self.lemma = []
		self.lines = []
		
		self.book_id = hashlib.md5(str.encode(filename)).hexdigest()
		self.has_gridfs_data = False
		
	def to_json(self):
		return {
			"book_id": self.book_id,
			"filename": self.filename,
			"metadata": {
				"title": self.meta.title,
				"author": self.meta.author,
				"publisher": self.meta.publisher
			}
		}
	def meta_tuple(self):
		return (str(self), self.meta.title, self.meta.author, self.meta.publisher)
		
	def get_gridfs_data(self):
		return {
			"raw": self.raw,
			"tokens": self.tokens,
			"sentences": self.sentences,
			"only_tokens": self.only_tokens,
			"only_nouns": self.only_nouns,
			"lemma": self.lemma,
			"lines": self.lines
		}
	
	def text(self):
		return ' '.join(map(itemgetter(0), self.raw)) #TODO: Is dit wel de juiste variabele
	
	def NLTK_text(self):
		nltk_text = nltk.text.Text(self.only_tokens, name=str(self))
		return nltk.text.TextCollection([nltk_text]) # Moet dit wel een collectie zijn?
	
	def fetch_gridfs_data(self):
		print("fetching data for %s" % self)
		
		conn = MongoConnection()
		fs = conn.fs
		
		data = fs.get(self.book_id).read().decode()
		json_data = json.loads(data)
		
		for key, val in json_data.items():
			#~ print("fetching data for %s: %s" % (self, key))
			setattr(self, key, val)
		self.has_gridfs_data = True
	def remove_all_with_wordlist(self, wlist, use_lemmas=False):
		results = []

		if use_lemmas:
			myiter = iter(self.lemma)
		else:
			myiter = iter(self.tokens)
		
		for t in myiter:
			if not t[0].lower() in wlist.words:
				results.append(t)
		return results
	
	# Niet zeker over de waarde die ik hier teruggeef
	def only_in_wordlist(self, wlist, use_lemmas=False):
		results = []

		if use_lemmas:
			myiter = iter(self.lemma)
		else:
			myiter = iter(self.tokens)

		for t in myiter:
			if t in wlist.words:
				results.append(t)
		return results

	def get_meta_attribute(self, attribute_name: str) -> str:
		try:
			retval = getattr(self.meta, attribute_name)
		except:
			print('attribuut %s niet gevonden in %s' % (attribute_name, self))
		return retval

	def set_metadata_attribute(self, attribute_name: str, v):
		try:
			setattr(self.meta, attribute_name, v)
		except:
			print('Attribuut %s niet gevonden in %s' %(attribute_name, self))

	def serialize(self):
		conn = MongoConnection()
		mycol = conn.database[self.collection]
		fs = conn.fs
		
		print("Serializing %s" % self)
		
		json_obj = self.to_json()
		mycol.insert_one(json_obj)
		
		#TODO het kan zijn dat het inlezen nog niet werkt
		grid_data = self.get_gridfs_data()
		grid_fs_data = json.dumps(grid_data)
		fs.put(str.encode(grid_fs_data), _id=self.book_id)
		
		# De datavelden in gridfs verwijderen
		for k in grid_data.keys():
			#~ print("Deleteing %s from %s" % (k, self))
			delattr(self, k)
			

	def __repr__(self):
		first = ''.join(self.meta.title.split(' '))
		foute_tekens = list("'/;-,:.()")
		filtered = [x for x in first if not x in foute_tekens]
		return ''.join(filtered)

	def tokenize(self):
		raise NotImplementedError()

	@property
	def short_filename(self):
		return self.filename.split('/')[-1]

	@property
	def picklefile(self):
		return '.'.join(os.path.basename(self.filename).split('.')[0:-1]) + '.book'
	
	@classmethod
	def load_book(cls, collection, filename):
		conn = MongoConnection()
		col = conn.database[collection]
		
		fileid = hashlib.md5(str.encode(filename)).hexdigest()
		
		book = col.find_one({"book_id": fileid})
		
		if book is None:
			print("Book not in database. Creating new entry for %s" % filename)
			book = cls(filename, collection)
			return book
		else:
			
			book_obj = BookI(filename, collection)
            
			for key, val in chain(book.items()):
				#~ print("Setting value %s of loaded book %s" % (key, filename))
				if key == 'metadata':
					meta = MetaData(val['author'], val['title'], val['publisher'])
					book_obj.meta = meta
				else:
					setattr(book_obj, key, val)
			
			print("Book loaded in cache: %s " % filename)
			return book_obj
			
			
def infinite_numbers(start):
	current = None
	while True:
		if current == None:
			current = start
		else:
			current +=1
		yield current

def convert_fraction(num: float, original_frac: int, prefered_frac: int, integer = False) -> int:
	return (num/original_frac)*prefered_frac

def float_range(start, stop = None, step = None):
	current = None
	while True:
		if not stop is None and not current is None and current >= stop:
			break
		if current is None:
			current = start
		elif step is None:
			current += 1
		else:
			current += step
		yield current


def distribute(to_distribute: list) -> list:
	retval = deepcopy(to_distribute)
	consecutive = False
	for item in retval:
		if item == 0 and not consecutive:
			# find max item
			max_item = max(enumerate(retval), key=itemgetter(1))[0]
			retval[max_item] = retval[max_item] - 1
			consecutive = True
		elif not item == 0:
			consecutive = False
	return retval

def consecutive(x):
	current = None
	for i in x:
		if not i == current:
			current = i
			yield current

def split_up(lower: float, upper: float, n: int) -> list:
	# step = (upper-lower)/(n+1)
	# myrange = list(np.arange(lower, upper, step))
	# return ((myrange[i], myrange[i+1]) for i in range(len(myrange) - 1))
	myrange = np.linspace(lower, upper, n + 1)
	return ((myrange[i], myrange[i+1]) for i in range(len(myrange) - 1))


def split_n_chars(s: str, n: int):
	return (s[i:i+n] for i in range(0, len(s), n))

def singleton(klass):
	inst = {}
	def get_instance(*args, **kwargs):
		if klass not in inst:
			inst[klass] = klass(*args, **kwargs)
		return inst[klass]
	return get_instance

def binary_search(lts, item, fun = None):
	first = 0
	last = len(lts) - 1
	
	while first <= last:
		midway_point = (first + last) // 2
		if fun is None:
			found_item = lts[midway_point]
		else:
			found_item = fun(lts[midway_point])
			
		if found_item == item:
			return midway_point
		elif found_item < item:
			#Iets meer naar rechts...
			first = midway_point + 1
		else:
			last = midway_point - 1
	#Niets gevonden
	return -1

class Rhymer:
	def _populate_dict(self):
		for w, pron in self.entries:
			level_pron = pron[-self.level:]
			pron_key = '|'.join(level_pron)
			
			# ~ print(pron_key)
			
			pron_words = self.rhyme_dict.get(pron_key, set())
			
			# ~ print('Adding', w, 'to', pron_words)
			
			pron_words.add(w)
			self.rhyme_dict[pron_key] = pron_words
		
	def __init__(self, level):
		self.entries = list(nltk.corpus.cmudict.entries())
		self.level = level
		
		self.rhyme_dict = {}
		self._populate_dict()
		
	def does_rhyme(self, x: str, y: str) -> bool:
		ind_x = binary_search(self.entries, x, itemgetter(0))
		ind_y = binary_search(self.entries, y, itemgetter(0))
		
		pron_x = self.entries[ind_x][1]
		pron_y = self.entries[ind_y][1]

		ending_x = pron_x[-self.level:]
		ending_y = pron_y[-self.level:]

		# print('Comparing rhyme {0} and {1}'.format(ending_x, ending_y))
		return ending_x == ending_y

	def __getitem__(self, key):
		item_ind = binary_search(self.entries, key, itemgetter(0))
		pron = self.entries[item_ind][1]
		# print(pron)
		level_pron = pron[-self.level:]
		# ~ print(level_pron)
		pron_key = '|'.join(level_pron)
		
		return self.rhyme_dict.get(pron_key, set())
