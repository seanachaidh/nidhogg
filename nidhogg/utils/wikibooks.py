from nltk import tag
from nltk import tokenize
from . import general
from copy import deepcopy

class SimpleBook(general.BookI):
    def __init__(self, filename, lang='dutch'):
        super().__init__(filename)
        self.tokenize()
        self.serialize()
        self.tokenizer = tokenize.treebank.TreebankWordTokenizer()

    def tokenize(self):
        mys = open(self.filename, 'r')
        split_sentences = tokenize.sent_tokenize(mys.read())
        self.sentences = self.tagger.tag_sents(mys)
        # self.raw = 
        self.tokens = deepcopy(self.raw) # Er zijn in deze teksten geen leestekens
        self.only_tokens = [t[0].lower() for t in self.tokens]
        self.only_nouns = [t for t in self.tokens if t[1] == 'NOUN']
        self._get_lemma()


class SimpleCorpus(general.CorpusI):
    def __init__(self, directory, wlist = None):
        super().__init__(directory, wlist, '.txt', SimpleBook)
