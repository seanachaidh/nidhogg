default_elements = ['A', 'C', 'G', 'T']
import xml.etree.ElementTree as ET
from copy import deepcopy
from itertools import chain, starmap
from operator import itemgetter
from random import choice
from typing import Dict, List

import dendropy as dp
import lxml.etree as LET
import matplotlib.pyplot as plt
import networkx as nx
from joblib import load
from scipy import signal
from sklearn.preprocessing import KBinsDiscretizer

from . import ebooks, simpletext
from .ebooks import EbookCorpus
from .general import split_n_chars
from .tree import TreeElement, hamming_distance


def call_copus(corpus_spec, *args, **kwargs):
    corpus_type, corpus_dir = corpus_spec.split(':')
    if corpus_type == 'simple':
        return simpletext.SimpleCorpus(corpus_dir, **kwargs)
    if corpus_type == 'epub':
        return ebooks.EbookCorpus(corpus_dir, **kwargs)
    else:
        raise Exception('Unknown corpus type: %s' % corpus_type)

def bools_to_dna(bool_list):
	return ''.join([('A' if item else 'C') for item in bool_list])

class DNATreeElement(TreeElement):
	def save_tree(self, filename):
		mybranches = [[y.data for y in x] for x in self.get_branches()]
		max_branch_length = max(map(len, mybranches))
		filtered_branches = list(filter(lambda x: len(x) == max_branch_length, mybranches))
		gene_length = len(filtered_branches[0][0])

		root = ET.Element('genetree', {'depth': str(max_branch_length), 'length': str(gene_length)})
		for i in range(len(filtered_branches)):
			current = list(filtered_branches)[i]
			subelem = ET.SubElement(root, 'encoding', attrib={'count': str(len(current)), 'id': str(i+1)})

			for j in range(len(current)):
				gene = current[j]
				lower_limit = j * (1/max_branch_length)
				upper_limit = lower_limit + (1/max_branch_length)
				gene_elem = ET.SubElement(subelem, 'gene', {'lower': str(lower_limit), 'upper': str(upper_limit)})
				gene_elem.text = gene
		final_tree = ET.ElementTree(root)
		final_tree.write(filename, encoding='utf-8')

	def generate_networkx_graph(self, filename=None):
		G = nx.Graph()
		graph_dict = {}
		labeldict = {}
		
		id_number = 0

		def networkx_handle(data: DNATreeElement):
			nonlocal graph_dict, G, id_number
			id_number += 1
			# Een beetje een redeundante check
			if not data in graph_dict:
				G.add_node(id_number)
				graph_dict[data] = id_number
				labeldict[id_number] = data.data
				
				if not data.isRoot:
					parent_data = data.parent
					parent_id = graph_dict[parent_data]
					G.add_edge(parent_id, id_number)
			return True
		self.dfs(networkx_handle)

		if not filename is None:
			fig = plt.figure()
			nx.draw(G, with_labels=True, labels=labeldict)
			fig.savefig(filename)

		return G

class DNATreeLoader:

	@property
	def depth(self):
		return int(self._root.attrib.get('depth', 0))
	@property
	def length(self):
		return int(self._root.attrib.get('length', 0))
		
	def get_gene_amount(self, enc_id: int) -> int:
		query = ".//encoding[@id='{0}']/@count".format(enc_id)
		return int(self._tree.xpath(query)[0])
	
	def get_actual_gene_amount(self, enc_id: int):
		query = ".//encoding[@id='{0}']/gene".format(enc_id)
		return len(self._tree.xpath(query))

	def predict_value(self, enc_id: int, dna: str) -> float:
		query = ".//encoding[@id={0}]/gene[.={1}]".format(enc_id, dna)
		element = self._tree.xpath(query)[0] # Exact één element
		lower, upper = element.attrib.values()

		return (lower+upper)/2

	def get_limit(self, enc_id: int):
		query = ".//encoding[@id='{0}']".format(enc_id)
		element = self._tree.xpath(query)

		return (element.attrib['lower'], element.attrib['upper'])

	def classify_encoding(self, sequence: str, enc_id: int) -> int:
		query_encoding = ".//encoding[@id='{0}']/gene".format(enc_id)
		myenc = self._tree.xpath(query_encoding)
		
		for i, c in enumerate(myenc):
			if c.text == sequence:
				return i
		return -1

	def create_mutation_mapping(self):
		n = ['C', 'G', 'T']
		result = []
		last_element = self._root.xpath(".//gene[last()]")[0]
		codes = list(last_element.text)
		for c in codes:
			result.append([x for x in n if x != c])
		return result

	def save_copy(self, filename):
		with open(filename, 'wb') as f:
			self._tree.write(f, encoding='utf-8')

	#Ik ga er van nu af aan vanuit dat de encoding id altijd 1 gaat zijn.
	def convert(self, value: float, retbin=False):
		if self.model is None:
			query = ".//encoding[@id='{0}']/gene[@lower<='{1}' and @upper>='{1}']".format(1, value)
			# final_gene = self._tree.xpath(query)[0]
		else:
			bin_result = int(self.model.transform([[value]])[0][0])
			query = ".//encoding[@id={0}]/gene[{1}]".format(1, bin_result + 1)
			# final_gene = self._tree.xpath(query)[0]
		if retbin:
			newquery = query + '/preceding-sibling::*'
			return len(self._tree.xpath(newquery))
		else:
			return self._tree.xpath(query)[0].text

	def __init__(self, filename: str, sklearn_model: str = None):
		self._tree = LET.parse(filename)
		self._root = self._tree.getroot()
		self._encodings = self._root.findall('./encoding')

		if sklearn_model:
			self.model = load(sklearn_model)
		else:
			self.model = None
	
	def set_new_limits(self, encoding_id, Q):
		current = 0
		encgenes = self._tree.xpath(".//encoding[@id='{0}']".format(encoding_id))

		for i, gene in enumerate(encgenes):
			gene.attrib['lower'] = current
			current = current + Q[i]
			gene.attrib['upper'] = current

	def set_new_limits_two(self, encoding_id: int, Q: list):
		encgenes = self._tree.xpath(".//encoding[@id='{0}']/gene".format(encoding_id))

		for i, gene in enumerate(encgenes):
			lower, upper = Q[i]
			gene.attrib['lower'] = str(lower)
			gene.attrib['upper'] = str(upper)
	
	def get_all_limits(self, enc_id: int) -> list:
		query = ".//encoding[@id='{0}']".format(enc_id)
		q_result = self._tree.xpath(query)[0]

		return [(x.attrib['lower'], x.attrib['upper']) for x in q_result]
		
	def looped_train_encoding(self, times, encoding_id, genes, delta, alpha):
		for _ in range(times): self.train_encoding(encoding_id, genes, delta, alpha)

	def train_encoding(self, encoding_id, genes, delta, alpha):
		encgenes = self._tree.xpath(".//encoding[@id='{0}']".format(encoding_id))[0]
		Q = [float(x.attrib['upper']) - float(x.attrib['lower']) for x in encgenes.xpath("./gene")]
		split_gene = chain(*map(lambda x: split_n_chars(x, self.length), genes))
		for g in split_gene:
			found_gene = next((x for x in enumerate(encgenes) if x[1].text == g), None)
			if found_gene is None:
				raise Exception('Foute gen in leveling')
			else:
				q_i = found_gene[0]
				Q[q_i] = (alpha * max([Q[q_i] - delta, 0])) + ((1-alpha) * Q[q_i])
			# Normaliseren van de Q-Vector
			sum_q = sum(Q)
			Q = [x/sum_q for x in Q]
		self.set_new_limits(encoding_id, Q)
	def generate_dna_distribution(self, encoding_id, geneclass, corpusdir):
		
		encgenes = self._tree.xpath(".//encoding[@id='{0}']/gene".format(encoding_id))
		
		distr = [0] * len(encgenes)
		
		#first we parse the nexusfile
		corpus = corpusdir

		# actual_gene_class = genebase.get_gene_class(geneclass)
		allgenes = corpus.apply_genes([geneclass])
		
		for gene in allgenes:
			current_gene = gene[1]
			
			#TODO: Nog eens kijken of deze lijst wel klopt
			splitted_gene = [current_gene[x:x+self.length] for x in range(0, len(current_gene), self.length)]
			for gene_part in splitted_gene:
				for i, val in enumerate(iter(encgenes)):
					if val.text == gene_part:
						distr[i] = distr[i] + 1
						break
		return distr

def neighbourhood_filter(first: list, second: list) -> list:
	def filter_fun(x):
		for j in reversed(range(len(second))):
			path_element = second[j]
			if path_element == x:
				return False

			expected_hamming = len(second) - j

			got_hamming = hamming_distance(x, path_element)
			if not got_hamming == expected_hamming:
				return False
		return True
	return list(filter(filter_fun, first))


def generate_gene_neighbourhood(gene: str, alphabet: list) -> list:
	results = []
	for i in range(len(gene)):
		newgene = deepcopy(list(gene))
		newgene[i] = alphabet[1]
		final = ''.join(newgene)
		if not final == gene:
			results.append(final)

	return results

def get_children(current: str, alphabet: list, combo: list):

	N = generate_gene_neighbourhood(current, alphabet)
	N_accent = neighbourhood_filter(N, combo)
	return N_accent

def generate_combo(gene_length: int, alphabet=default_elements) -> list:
	first = alphabet[0] * gene_length
	in_encoding = [first]
	children = get_children(first, alphabet, in_encoding)

	while children:
		current = choice(children)
		in_encoding.append(current)

		print("Encoding {0}: {1}".format(current, len(in_encoding)))

		children = get_children(current, alphabet, in_encoding)
	return in_encoding

def get_children_tree(current: DNATreeElement, alphabet: list, combo: list):
	tree_path = list(reversed(current.get_path())) # Nogal kostelijk

	N = generate_gene_neighbourhood(current.data, alphabet)
	N_accent = neighbourhood_filter(N, tree_path)
	return N_accent

def generate_dna_tree(gene_length: int, alphabet=default_elements) -> list:
	first = alphabet[0] * gene_length
	in_tree = [first]
	root = DNATreeElement(data=first)

	stack = [root]

	while stack:
		current = stack.pop()

		current_children = get_children_tree(current, alphabet, in_tree)
		for c in current_children:
			in_tree.append(c)
			stack.append(DNATreeElement(data=c, parent=current))

	return root

def paths_to_encoding(paths: list, filename: str):
	max_branch_length = max([len(x) for x in paths])
	gene_length = len(paths[0][0])
	root = ET.Element('genetree', {'depth': str(max_branch_length), 'length': str(gene_length)})

	for i, current in enumerate(paths):
		subelem = ET.SubElement(root, 'encoding', attrib={'count': str(len(current)), 'id': str(i+1)})
		
		for index, p in enumerate(current):
			lower_limit = index * (1/max_branch_length)
			upper_limit = lower_limit + (1/max_branch_length)

			gene_elem = ET.SubElement(subelem, 'gene', {'lower': str(lower_limit), 'upper': str(upper_limit)})
			gene_elem.text = p
	final_tree = ET.ElementTree(root)
	final_tree.write(filename, encoding='utf-8')
