import os, requests
from io import StringIO
from lxml import etree, html


class VanDaeleRijm:
    def _load_url(self):
        r = requests.get(self.url, parser=self.parser)
        if r.status_code == 200:
            self.tree = html.parse(StringIO(r.text), parser=self.parser)
            self.root = self.tree.getroot()
        else:
            pass
    def __init__(self, url):
        self.url = url
        self.parser = html.HTMLParser(encoding='utf8')
