import matplotlib.pyplot as plt

def create_distribution_graph(values, filename, debug = True, labels = None):
	
	if debug:
		debugfilename = filename + '.debug'
		with open(debugfilename, 'w') as f:
			print('DEBUGGED VALUES', file=f)
			print('---------------', file=f)
			print('histogram:{0}'.format(values), file=f)
			
	
	plt.hist(values, density=True, facecolor='g')
	
	plt.xlabel('Classes')
	plt.ylabel('Gene amounts')
	plt.grid(True)
	
	plt.savefig(filename)
	
