from .general import singleton
from SPARQLWrapper import SPARQLWrapper, JSON

from operator import itemgetter
import urllib.parse as parser


def create_prefixes():
	"""
	PREFIX owl: <http://www.w3.org/2002/07/owl#>
	PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
	PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
	PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
	PREFIX foaf: <http://xmlns.com/foaf/0.1/>
	PREFIX dc: <http://purl.org/dc/elements/1.1/>
	PREFIX : <http://dbpedia.org/resource/>
	PREFIX dbpedia2: <http://dbpedia.org/property/>
	PREFIX dbpedia: <http://dbpedia.org/>
	PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
	"""
	retval = list()
	retval.append(('http://www.w3.org/2002/07/owl#', 'owl'))
	retval.append(('http://www.w3.org/2001/XMLSchema#', 'xsd'))
	retval.append(('http://www.w3.org/2000/01/rdf-schema#', 'rdfs'))
	retval.append(('http://www.w3.org/1999/02/22-rdf-syntax-ns#', 'rdf'))
	retval.append(('http://xmlns.com/foaf/0.1/', 'foaf'))
	retval.append(('http://purl.org/dc/elements/1.1/', 'dc'))
	retval.append(('http://dbpedia.org/resource/', ''))
	retval.append(('http://dbpedia.org/property/', 'dbpedia2'))
	retval.append(('http://dbpedia.org/', 'dbpedia'))
	retval.append(('http://www.w3.org/2004/02/skos/core#', 'skos'))

	return retval
@singleton
class DBPedia:
	def __init__(self):
		self.connection = SPARQLWrapper("http://dbpedia.org/sparql")
		self.connection.setReturnFormat(JSON)
		
		self.prefixes = {}
		self.vars = []
		
		
		# Een paar prefixen die we standaard gaan toevoegen voor het gemak
		self.add_prefix('', 'http://dbpedia.org/resource/')
		self.add_prefix('owl', 'http://www.w3.org/2002/07/owl#')
		self.add_prefix('foaf', 'http://xmlns.com/foaf/0.1/')
		self.add_prefix('rdfs', 'http://www.w3.org/2000/01/rdf-schema#')
			
	def add_prefix(self, name, uri):
		self.prefixes[name] = uri
	
	def ask(self, triplets):
		
		varnames = ' '.join('?' + str(x) for x in self.vars)
		
		prefixheading = map(lambda x: 'PREFIX {0}: <{1}>'.format(x[0], x[1]), self.prefixes.items())
		prefixstr = '\n'.join(prefixheading)
		
		triplet_list = map(lambda x: ''.join(x), triplets)
		tripletstr = ' '.join(triplet_list)
		
		final_query = """
			{1}
			SELECT {0} WHERE {{
				{2}
			}}
		""".format(varnames, prefixstr, tripletstr)
		
		# print(final_query)
		self.connection.setQuery(final_query)
		results = None
		
		try:
			results = self.connection.query().convert()
		except Exception as e:
			print(e)
		finally:
			return results
class ResourceRetriever:

	def _retrieve_info(self):
		self.info = {}
		self.connection.vars = ['hasValue', 'property']
		
		results = self.connection.ask(('<'+self.resource_uri+'>', '?property', '?hasValue'))
		
		for item in results["results"]["bindings"]:
			
			propertyValue = self.prefix_transform(item["property"]["value"])
			valueValue = item["hasValue"]["value"]
			if propertyValue in self.info:
				tmplist = self.info[propertyValue]
				tmplist.append(valueValue)
			else:
				self.info[propertyValue] = 	[valueValue]
		
	def __init__(self, resource_name):
		self.resource_uri = resource_name
		self.prefixes = create_prefixes()

		self.connection = DBPedia()
		# Dit is niet echt thread safe
		self._retrieve_info()
	def children(self):
		pass
	def prefix_transform(self, uri):
		#Ik kan dit eigenlijk op een inteligentere manier schrijven
		parsed_uri = parser.urldefrag(uri)
		#if we have a hashtag part
		if not parsed_uri.fragment == '':
			# Er voorlopig van uitgaande dat een URI met fragment altijd een prefix gaat hebben die op een hashtag eindigt
			url_uri = parsed_uri.url + '#'
			obtained_prefix = next((i for i in self.prefixes if i[0] == url_uri), None)
			if obtained_prefix:
				final_key = '{0}:{1}'.format(obtained_prefix[1], parsed_uri.fragment)
			else:
				final_key = uri

		else:
			splitted_url = parsed_uri.url.split('/')
			element_part = splitted_url[-1]
			uri_part = '/'.join(splitted_url[:-1])

			obtained_prefix = next((i for i in self.prefixes if i[0] == uri_part + '/'), None)
			if obtained_prefix:
				final_key = '{0}{1}'.format(obtained_prefix[1], element_part)
			else:
				final_key = uri
		return final_key
		