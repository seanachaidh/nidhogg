#ideeën: Clusterafstand
# https://stackoverflow.com/questions/17118643/determine-distance-between-two-random-nodes-in-a-tree

from dendropy import Tree, Node, Taxon
from copy import deepcopy
from itertools import chain
import numpy as np
from operator import itemgetter

import re

from sklearn.utils.multiclass import unique_labels
from sklearn.metrics import confusion_matrix

from enum import Enum

from  . import ebooks
from ..utils import general

#De sklearn metrics
from sklearn import metrics as skmetrics
from itertools import chain

# extra handige functies
# TODO: Verplaats deze functies naar het Trees bronbestand

def is_root(n: Node):
	return n.parent_node is None

def path_to_root(n: Node):
	result = []
	cn = deepcopy(n)
	while not is_root(cn):
		result.append(cn)
		cn = cn.parent_node
	return result

def path_to_node(n: Node, m: Node) -> list:
    result = []
    cn = deepcopy(n)
    while not is_root(cn) and not cn == m:
        result.append(cn)
        cn = cn.parent_node
    result.reverse()
    return result

def get_nodes_by_number(boom: Tree, numberlist: list):
    retval = []
    for i, item in enumerate(boom.inorder_node_iter()):
        if i in numberlist:
            retval.append(item)
    return retval

def my_distance_to_root(n: Node):
    return len(path_to_root(n))

def node_to_lists(n: Node) -> list:
    return [path_to_node(x, n) for x in n.leaf_iter()]

def my_distance_to_tip(n: Node):
    return max(map(len, node_to_lists(n)))

def vectors_to_numbers(v1, v2):
    woordenboek = {}
    counter = 0
    
    def get_item(item: str):
        nonlocal woordenboek, counter
        if item in woordenboek:
            return woordenboek[item]
        else:
            woordenboek[item] = counter
            counter += 1
            return (counter - 1)
    return(list(map(get_item, v1)), list(map(get_item, v2)))

def level_iterator(tree: Tree, level: int):
    # return tree.pre_node_iter(lambda x: x.distance_from_root() == level)
    visited_nodes = []

    def node_filter_fn(x: Node):
        nonlocal visited_nodes, level
        if my_distance_to_root(x) == level and not x in visited_nodes:
            visited_nodes.append(x)
            visited_nodes += x.child_nodes()

            return True
        elif my_distance_to_tip(x) < level and not x in visited_nodes:
            visited_nodes.append(x)
            visited_nodes += x.child_nodes()

            return True
        else:
            return False

    return tree.preorder_node_iter(node_filter_fn)

def lca(dendrotree: Tree, a: Node, b: Node) -> Node:
	roota = path_to_root(a)
	rootb = path_to_root(b)
	result = next(filter(lambda x: x[0] == x[1], map(lambda x, y: (x, y), roota, rootb)), None)[0]

	return result

# ref: https://stackoverflow.com/questions/17118643/determine-distance-between-two-random-nodes-in-a-tree

def distance_nodes(dendrotree, a: Node, b: Node) -> int:
	return a.distance_from_root() + b.distance_from_root() - 2 * lca(dendrotree, a, b).distance_from_root()


def extract_integer(to_extract: str) -> int:
    result = re.search(r'(?<=Cluster\s)\d+', to_extract)
    return int(result.group(0))

class MatrixType(Enum):
    NORMAL = 'normal'
    SIMPLE = 'simple'
    FILTERED = 'filtered'

#Een beetje vreemd om hiervan een string te aken maar het past beter binnen ons parsingsysteem
class ClusterListType:
    def __init__(self, clusterlist: str):
        if not clusterlist:
            raise Exception('No cluster list has been chosen')
        self.clusterlist = [int(x) for x in clusterlist.split(',')]

    def convert_to_nodes(self, boom: Tree):
        retval = []
        for i, item in enumerate(boom.levelorder_node_iter()):
            if i in self.clusterlist:
                retval.append(item)
        return retval

class MetricBase:
    '''
        maakt een nieuwe metric op basis van een boom
    '''

    @property
    def cluster_amount(self):
        return len(self.clustids)

    def __init__(self, clusterdict: dict, base_attribute_name: str):
        self.clusterdict = clusterdict
        self.clustids = set([extract_integer(x['clusterid']) for x in self.clusterdict])
        self.attribute = base_attribute_name
        
        clusters = dict()
        for i in self.clustids:
            mylist = list(filter(lambda j: j['clusterid'] == 'Cluster ' + str(i), self.clusterdict))
            clusters[i] = mylist

        self.clusters = clusters

    def _count_classes(self, clusterid: int):
        c = {}
        cluster = self.clusters[clusterid]
        for cl in cluster:
            book = cl['book']
            attr_book = getattr(book.meta, self.attribute, None)
            count = c.get(attr_book, 0)
            count += 1
            c[attr_book] = count
        return c

    def id_to_cluster_label(self, myid, mylist):
        for i, key in mylist:
            if i == myid:
                return key
        else:
            return None

    def search_dominant_with_id(self, doms, id):
        for key, val in doms.items():
            myid, count = val

            if myid == id:
                return (myid, key, count)
        else:
            return None

    def generate_cluster_labels(self):
        result = []

        mydoms = self.dominant_classes()
        for i in self.clustids:
            dominant = self.search_dominant_with_id(mydoms, i)
            if dominant is None:
                newname = 'unknown_' + str(i)
                result.append((i, newname))
            else:
                result.append((dominant[0], dominant[1]))
        return result

    def dominant_classes(self):
        class_counts = [self._count_classes(i) for i in self.clustids]
        max_classes = [max(x.items(), key=itemgetter(1)) for x in class_counts]
        all_authors = self.all_possible_labels()

        dominant_class_ids = {}

        for i, cl in enumerate(max_classes):
            name, count = cl
            if name in dominant_class_ids:
                classcount = dominant_class_ids[name][1]
                if classcount < count:
                    dominant_class_ids[name] = (i, count)
            else:
                dominant_class_ids[name] = (i, count)
        #TODO: assigning virtual cluster -1 to non dominants.

        for author in all_authors:
            if not author in dominant_class_ids:
                dominant_class_ids[author] = (-1, 0)

        return dominant_class_ids

    def find_most_common_class(self):
        return map(lambda x: max(self._count_classes(x)), self.clustids)
    
    def generate_vectors(self):
        results_pred = []
        results_expected = []
        for cluster in self.clustids:
            max_class = max(self._count_classes(cluster).items(), key=itemgetter(1))[0]
            expectation = [max_class] * len(self.clusters[cluster])
            predictions = list(map(lambda x: x.get_meta_attribute(self.attribute), map(lambda y: y['book'], self.clusters[cluster])))

            #GAAT DIT WERKEN!
            results_expected += expectation
            results_pred += predictions

        return (results_expected, results_pred)

    def cluster_label_to_id(self, label, mylist):
        for myid, key in mylist:
            if label == key:
                return myid
        else:
            return -1

    def expected_predicted(self, numbers=True):
        result_expected = []
        result_predicted = []

        clust_labels = self.generate_cluster_labels()
        for i in self.clustids:
            current_cluster = self.clusters[i]
            for x in current_cluster:
                b = x['book']
                battr = b.get_meta_attribute(self.attribute)
                if numbers:
                    expected_id = self.cluster_label_to_id(battr, clust_labels)
                else:
                    expected_id = battr
                result_expected.append(expected_id)
                if numbers:
                    result_predicted.append(i)
                else:
                    result_predicted.append(self.id_to_cluster_label(i, clust_labels))

        return(result_expected, result_predicted)

    def all_possible_labels(self):
        result = set()
        for i in self.clustids:
            current = self.clusters[i]
            for c in current:
                book_val = c['book'].get_meta_attribute(self.attribute)
                result.add(book_val)

        return result

    def generate_number_vectors(self):
        vectors = self.generate_vectors()

        for i, j in vectors:
            x, y = vectors_to_numbers(i, j)
            yield (x, y)

    def calculate_metric(self):
        raise NotImplementedError

class ConfusionMatrix(MetricBase):
    def create_confusion_matrix(self, y_test, y_pred):

        #Assuming regular string labels
        combo_labels = set(y_test + y_pred)
        no_unknowns = [x for x in combo_labels if not x.startswith('unknown_')]
        n = len(no_unknowns) + 1

        no_unknowns += ["unknown"]
        y_test_new = (x if not x.startswith('unknown_') else 'unknown' for x in y_test)
        y_pred_new = (z if not z.startswith('unknown_') else 'unknown' for z in y_pred)
        
        confusion = np.zeros((n,n), dtype=int)

        for x, y in zip(y_test_new, y_pred_new):
            x_ind = no_unknowns.index(x)
            y_ind = no_unknowns.index(y)
            confusion[x_ind, y_ind] = confusion[x_ind, y_ind] + 1
        return (confusion, no_unknowns)

    def calculate_metric(self):
        test, pred = self.expected_predicted(False)
        cm = self.create_confusion_matrix(test, pred)

        return cm

class Purity(MetricBase):

    def calculate_metric(self):
        vecs = self.generate_vectors()
        labels = vectors_to_numbers(*vecs)

        matrix = skmetrics.cluster.contingency_matrix(labels[1], labels[0])
        return np.sum(np.amax(matrix, axis=0)) / np.sum(matrix)        

class RandMeassure(MetricBase):

    def calculate_metric(self):
        vecs = self.generate_vectors()
        labels = vectors_to_numbers(*vecs)

        retval = skmetrics.adjusted_rand_score(*labels)
        return retval
    def __repr__(self):
        return super(RandMeassure, self).__repr__()

class Accuracy(MetricBase):
    def calculate_metric(self):
        labels = self.expected_predicted()
        return skmetrics.accuracy_score(*labels)

class MutualInformation(MetricBase):

    def calculate_metric(self):
        labels = self.expected_predicted()
        #TODO: Hier hoort nog een extra parameter die nader onderzocht moet worden
        return skmetrics.normalized_mutual_info_score(*labels)

class HomogenityMeasure(MetricBase):
    def calculate_metric(self):
        vecs = self.generate_vectors()
        labels = vectors_to_numbers(*vecs)

        return skmetrics.homogeneity_score(*labels)


exported_metrics = [Purity, MutualInformation, Accuracy]
