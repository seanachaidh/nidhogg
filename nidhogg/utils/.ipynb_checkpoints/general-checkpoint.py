import nltk, inspect, uuid, os, pickle
from copy import deepcopy # Gaat dit wel werken?
from operator import itemgetter
from dendropy import Tree, Node
import numpy as np

class AttrDict(dict):
	def __getattr__(self, key):
		if key in self:
			return self[key]

	def __setattr__(self, key, value):
		self[key] = value

class MetaData:

    def __init__(self, author='None', title='None', publisher='None'):
        self.author = author
        self.title = title
        self.publisher = publisher

# GENERAL INTERFACES


class CorpusI:
	def __init__(self, directory, wlist = None, extension='.txt', bookclass=None):
		self.books = list()
		if not os.path.isdir(directory):
			raise Exception('Corpus directory ' + directory + ' does not exist')
		else:
			files = os.listdir(directory)
			for f in files:
				if f.endswith(extension):
					book = bookclass.load_book(os.path.join(directory, f))
					if not wlist is None:
						book.remove_all_with_wordlist(wlist)
					self.books.append(book)
	def all_titles(self):
		return [x.meta.title for x in self.books]
	def __len__(self):
		return len(self.books)
	def __getitem__(self, i):
		if type(i) == str:
			return next(filter(lambda x: str(x) == i, self.books), None)
		else:
			return self.books[i]

	def get_all_with_meta(self, attr):
		return set(zip(self.books, map(lambda x: x.get_meta_attribute(attr), self.books)))

	def apply_genes(self, genelist):
		results = list()
		for b in self.books:
			dna = ''
			for gene in genelist:
				print('Applying {0} on book: {1}'.format(gene.__name__, b))

				gene_instance = gene()
				gene_instance.load_data(b)
				dna += gene_instance.transform_gene()
			results.append((str(b), dna))
		return results
	def get_book_by_name(self, name: str):
		return next(filter(lambda x: str(x) == name, self.books), None)

class BookI:
	def __init__(self, filename):
		self.book_id = uuid.uuid4().hex
		self.filename = filename
		self.meta = MetaData()

		self.raw = []
		self.tokens = []
		self.sentences = []
		self.only_tokens = []
		self.only_nouns = []
		self.lemma = []

	def remove_all_with_wordlist(self, wlist, use_lemmas=False):
		results = []

		if use_lemmas:
			myiter = iter(self.lemma)
		else:
			myiter = iter(self.tokens)
		
		for t in myiter:
			if not t[0].lower() in wlist.words:
				results.append(t)
		return results
	
	# Niet zeker over de waarde die ik hier teruggeef
	def only_in_wordlist(self, wlist, use_lemmas=False):
		results = []

		if use_lemmas:
			myiter = iter(self.lemma)
		else:
			myiter = iter(self.tokens)

		for t in myiter:
			if t[0].lower() in wlist.words:
				results.append(t)
		return results

	def get_meta_attribute(self, attribute_name: str) -> str:
		try:
			retval = getattr(self.meta, attribute_name)
		except:
			print('attribuut %s niet gevonden in %s' % (attribute_name, self))
		return retval

	def set_metadata_attribute(self, attribute_name: str, v):
		try:
			setattr(self.meta, attribute_name, v)
		except:
			print('Attribuut %s niet gevonden in %s' %(attribute_name, self))

	def serialize(self):
		if not os.path.isdir('serial_books'):
			os.mkdir('serial_books')
		final_path = os.path.join('serial_books', self.picklefile)
		with open(final_path, 'wb+') as f:
			pickle.dump(self, f)

	def __repr__(self):
		first = ''.join(self.meta.title.split(' '))
		foute_tekens = list("'/;-,:.()")
		filtered = [x for x in first if not x in foute_tekens]
		return ''.join(filtered)

	def tokenize(self):
		raise NotImplementedError()

	@property
	def short_filename(self):
		return self.filename.split('/')[-1]

	@property
	def picklefile(self):
		return '.'.join(os.path.basename(self.filename).split('.')[0:-1]) + '.book'
	
	@classmethod
	def load_book(cls, bookfile):
		picklefile = '.'.join(os.path.basename(bookfile).split('.')[0:-1]) + '.book'
		if os.path.isfile(os.path.join('serial_books', picklefile)):
			with open(os.path.join("serial_books", picklefile), 'rb') as f:
				loaded_book = pickle.load(f)
				print('book serialized in chache. Loaded', loaded_book.meta.title)
				return loaded_book
		else:
			print('book not cached. Creating new book:', picklefile)
			loaded_book = cls(bookfile)
			return loaded_book

def infinite_numbers(start):
	current = None
	while True:
		if current == None:
			current = start
		else:
			current +=1
		yield current

def convert_fraction(num: float, original_frac: int, prefered_frac: int, integer = False) -> int:
	return (num/original_frac)*prefered_frac

def float_range(start, stop = None, step = None):
	current = None
	while True:
		if not stop is None and not current is None and current >= stop:
			break
		if current is None:
			current = start
		elif step is None:
			current += 1
		else:
			current += step
		yield current


def distribute(to_distribute: list) -> list:
	retval = deepcopy(to_distribute)
	consecutive = False
	for i, item in enumerate(retval):
		if item == 0 and not consecutive:
			# find max item
			max_item = max(enumerate(retval), key=itemgetter(1))[0]
			retval[max_item] = retval[max_item] - 1
			consecutive = True
		elif not item == 0:
			consecutive = False
	return retval

def consecutive(x):
	current = None
	for i in x:
		if not i == current:
			current = i
			yield current

def split_up(lower: float, upper: float, n: int) -> list:
	# step = (upper-lower)/(n+1)
	# myrange = list(np.arange(lower, upper, step))
	# return ((myrange[i], myrange[i+1]) for i in range(len(myrange) - 1))
	myrange = np.linspace(lower, upper, n + 1)
	return ((myrange[i], myrange[i+1]) for i in range(len(myrange) - 1))


def split_n_chars(s: str, n: int):
	return (s[i:i+n] for i in range(0, len(s), n))

def recursive_classes(module_name: str) -> list:
	retval = list()
	first_module = inspect.importlib.import_module(module_name)
	stack = [first_module]

	while stack:
		current = stack.pop()
		for member in map(itemgetter(1), inspect.getmembers(current)):
			if inspect.isclass(member):
				retval.append(member)
			#ensuring we only dive into strict parent modules
			elif inspect.ismodule(member) and module_name in member.__name__:
				stack.append(member)
	return retval


def singleton(klass):
	inst = {}
	def get_instance(*args, **kwargs):
		if klass not in inst:
			inst[klass] = klass(*args, **kwargs)
		return inst[klass]
	return get_instance

#Geïnspireerd op stackoverflow
class Singleton(type):
	_instances = {}
	def __call__(cls, *args, **kwargs):
		if not cls in cls._instances:
			cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
		return cls._instances[cls]

def binary_search(lts, item, fun = None):
	first = 0
	last = len(lts) - 1
	
	while first <= last:
		midway_point = (first + last) // 2
		if fun is None:
			found_item = lts[midway_point]
		else:
			found_item = fun(lts[midway_point])
			
		if found_item == item:
			return midway_point
		elif found_item < item:
			#Iets meer naar rechts...
			first = midway_point + 1
		else:
			last = midway_point - 1
	#Niets gevonden
	return -1

class Rhymer:
	def _populate_dict(self):
		for w, pron in self.entries:
			level_pron = pron[-self.level:]
			pron_key = '|'.join(level_pron)
			
			# ~ print(pron_key)
			
			pron_words = self.rhyme_dict.get(pron_key, set())
			
			# ~ print('Adding', w, 'to', pron_words)
			
			pron_words.add(w)
			self.rhyme_dict[pron_key] = pron_words
		
	def __init__(self, level):
		self.entries = list(nltk.corpus.cmudict.entries())
		self.level = level
		
		self.rhyme_dict = {}
		self._populate_dict()
		
	def does_rhyme(self, x: str, y: str) -> bool:
		ind_x = binary_search(self.entries, x, itemgetter(0))
		ind_y = binary_search(self.entries, y, itemgetter(0))
		
		pron_x = self.entries[ind_x][1]
		pron_y = self.entries[ind_y][1]

		ending_x = pron_x[-self.level:]
		ending_y = pron_y[-self.level:]

		# print('Comparing rhyme {0} and {1}'.format(ending_x, ending_y))
		return ending_x == ending_y

	def __getitem__(self, key):
		item_ind = binary_search(self.entries, key, itemgetter(0))
		pron = self.entries[item_ind][1]
		# print(pron)
		level_pron = pron[-self.level:]
		# ~ print(level_pron)
		pron_key = '|'.join(level_pron)
		
		return self.rhyme_dict.get(pron_key, set())
