import os
import unittest

from ..utils.ebooks import EPUB

class WordList:

    def __init__(self, directory):
        self.words = []
        if not os.path.isdir(directory):
            raise Exception('Wordlist directory not found')
        else:
            allfiles = os.listdir(directory)
            for x in allfiles:
                with open(os.path.join(directory, x), 'r') as f:
                    for line in f:
                        if not line.startswith('//'):
                            toadd = line.strip()
                            self.words.append(toadd)
        # Volgens mij worden verzamelingen standaard altijd gesorteerd
        self.words = set(sorted(self.words, reverse=True))
    def __len__(self):
        return len(self)

    #TODO ik denk dat deze functie niet meer nodig
    def search_words(self, ebook, maxwin = 3):
        results = []
        tokens = ebook.tokens
        for i in range(len(tokens)):
            #TODO: Kijk na of dit een goede waarde teruggeeft
            tokenlist = [tokens[i:(i+w)] for w in range(1, (maxwin + 1))]
            tokenlist = [x[0] for x in tokenlist]
            for t in tokenlist:
                if not t == []:
                    fusedt = ' '.join(t)
                    if fusedt in self.words:
                        results.append((i, len(t)))
        return results
