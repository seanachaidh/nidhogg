from . import ClusterPipe
import re

class OmegaPipe(ClusterPipe.ClusterPipe):
    def __init__(self, fasta, attribute='author', debug=False, kimura=False, distmat_output=None, cluster_size=10):
        super().__init__(fasta, attribute, debug)

        if kimura:
            kimura_string = ' --use-kimura'
        else:
            kimura_string = ''

        command = "clustalo -i $IN --infmt=fa --cluster-size="+str(cluster_size)+(' --distmat-out=%s' % distmat_output if distmat_output else '')+" --clustering-out=$OUT" +kimura_string+ " -o /dev/null --force -v"
        self._pipe.append(command, 'ff')
        self._writefile = self._pipe.open(self.output.name, 'w')

    def get_cluster_number(self, clusterid):
        return int(re.search(r"\d+", clusterid)[0])

    def make_lines(self, corpus):
        results = []
        for c in self.clusterids:
            for t in self.get_cluster_result(c):
                book = corpus[t]
                tmpval = {
                    'idnum': self.get_cluster_number(c),
                    'extra': None,
                    'percent': None,
                    'title': t,
                    'clusterid': c,
                    'real_author': book.meta.author,
                    'book': book
                }
                results.append(tmpval)
        return results

    def get_cluster_result(self, clusterid):
        # (?<=\(=seq\s).+?(?=\s)
        lines = re.findall(r"^"+ clusterid + r":.+$", self.result, re.MULTILINE)

        result = []
        for l in lines:
            print("processing %s" % l)
            m = re.search(r"(?<=\(=seq\s).+?(?=\s)", l)
            result.append(m[0])
        return result

    def execute(self, corpus):
        # Clusterids ^Cluster\s\d+(?=\:)

        self._writefile.write(open(self.fasta).read())
        self._writefile.close()

        self.output.seek(0)
        self.result = self.output.read().decode('utf-8')
        self.clusterids = list(set(re.findall(r"^Cluster\s\d+(?=\:)", self.result, re.MULTILINE)))

        self.clusterdict = self.make_lines(corpus)
        self.clusters = {k: [x for x in self.clusterdict if x['clusterid'] == k] for k in self.clusterids}
        labels = self.cluster_labels()
        for c in self.clusterdict:
            c['label'] = labels[c['clusterid']]
            c['filename'] = c['book'].filename
