import os, inspect, json
from operator import itemgetter
gene_settings_dir = os.path.join('.', 'genesettings')

def recursive_classes(module_name: str) -> list:
	retval = list()
	first_module = inspect.importlib.import_module(module_name)
	stack = [first_module]

	while stack:
		current = stack.pop()
		for member in map(itemgetter(1), inspect.getmembers(current)):
			if inspect.isclass(member):
				retval.append(member)
			#ensuring we only dive into strict parent modules
			elif inspect.ismodule(member) and module_name in member.__name__:
				stack.append(member)
	return retval

def get_gene_class(gene_name: str):
	gene_classes = recursive_classes('nidhogg.genes')
	return next(filter(lambda x: x.__name__ == gene_name and Gene in inspect.getmro(x), gene_classes))

def collect_genes(genes):
	gene_list = genes.split(',')
	all_classes = recursive_classes('nidhogg.genes')
	filtered_genes = list(filter(lambda x: x.__name__ in gene_list and Gene in inspect.getmro(x), all_classes))
	return filtered_genes

class Gene:
	setting_names = []

	@property
	def has_settings(self):
		jsonname = self.__class__.__name__ + '.json'
		final_filename = os.path.join(gene_settings_dir, jsonname)
		if os.path.isfile(final_filename):
			return True
		else:
			return False

	@property
	def json_filename(self):
		jsonname = self.__class__.__name__ + '.json'
		final_filename = os.path.join(gene_settings_dir, jsonname)

		return final_filename
	
	def _get_setting_members(self):
            return [x for x in self.__class__.__dict__.keys() if x.startswith('setting_')]

	def settings_to_json(self):
		new_settings = {}
		members = self._get_setting_members()
		for m in members:
			name = m[8:] # Denk ik!
			member_val = getattr(self, m)
			new_settings[name] = member_val
		return new_settings
	
	def __init__(self, corpus):
		self.corpus = corpus
		if self.has_settings:
			print("Settings exist for: {0}".format(self.json_filename))
			self.load_settings()
		else:
			settings = self.settings_to_json()
			with open(self.json_filename, 'w') as f:
				json.dump(settings, f)
	
	def load_settings(self):
		jsonname = self.__class__.__name__ + '.json'
		final_filename = os.path.join(gene_settings_dir, jsonname)
		with open(final_filename, 'r') as f:
			jsonobj = json.load(f)
		
		for setting in iter(jsonobj):
			newname = 'setting_' + setting
			setattr(self, newname, jsonobj[setting])
		
	def __repr__(self):
		return self.__class__.__name__

	def save_settings(self):
		jsonname = self.__class__.__name__ + '.json'
		jsonsettings = self.settings_to_json()
		final_filename = os.path.join(gene_settings_dir, jsonname)
		with open(final_filename, 'w') as f:
			json.dump(jsonsettings, f)

	def load_data(self, ebook):
		raise NotImplementedError

	def transform_gene(self, only_features=False):
		raise NotImplementedError
	
	def summary(self):
		raise NotImplementedError
