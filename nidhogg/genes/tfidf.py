import os
import pandas as pd

from . import genebase
from operator import itemgetter
from nltk.probability import FreqDist

from ..utils.dna import DNATreeLoader
from ..utils.wordlist import WordList

from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import TfidfTransformer, CountVectorizer

def normalized_items(items):
    maximum = max(items, default=0, key=itemgetter(1))[1]
    minimum = min(items, default=0, key=itemgetter(1))[1]
    
    newitems = [(x, (y - minimum)/(maximum - minimum)) for x, y in items if not (maximum - minimum) == 0]
    return newitems

class TfIdf(genebase.Gene):
    setting_encoding_tree = os.path.join('encoding_trees', 'bigtree.xml')
    setting_encoding_id = 1
    setting_use_lemma = False
    setting_sklearn_model = os.path.join("datadir", "tfidf.model")
    setting_function_words = os.path.join("datadir", "function_words")

    def _train_gene(self, corpus:list, vocab: list) -> Pipeline:
        # Start SKLearn pipeline
        pipe = Pipeline([
            ('counter', CountVectorizer(vocabulary=vocab)),
            ('tfidf', TfidfTransformer())
        ])

        corpus_texts = [x[1] for x in self.prepared_corpus]
        print("TFIDF: Training corpus")
        results = pipe.fit_transform(corpus_texts)

        return results

    def _generate_common_words(self):
        results = {}
        for w in self.used_wordlist.words:
            results[w] = 1
        return results

    def save_mode(self, outfile: str):
        raise NotImplementedError

    def _prepare_corpus(self):
        textlist = []

        #overloop het corpus dat geladen is met dit dna
        for b in self.corpus:
            print("TF-IDF gene loading book: %s" % b)
            text = ' '.join(b.only_tokens)
            textlist.append((str(b), text))
        return textlist
    def __init__(self, corpus):
        super(TfIdf, self).__init__(corpus)
        self.used_wordlist = WordList(self.setting_function_words)
        self.dna_tree = DNATreeLoader(self.setting_encoding_tree, self.setting_sklearn_model)

        self.most_common_words = self._generate_common_words()

        self.prepared_corpus = self._prepare_corpus()
        self.corpus_names = [x[0] for x in self.prepared_corpus]
        self._model = self._train_gene(self.prepared_corpus, self.used_wordlist.words)

    def generate_count(self, text, words):
        result = {x: 0 for x in words}
        for t in text:
            tmpres = result[t] # We gaan ervan uit dat er geen onbekende sleutelwaarden voorkomen in de words variabele
            result[t] = tmpres + 1
        return result

    def load_data(self, ebook):
        self.current_ebook = ebook
        used_function_words = list(map(lambda x: x, ebook.only_in_wordlist(self.used_wordlist, self.setting_use_lemma)))
        self.most_common_words = self.generate_count(used_function_words, self.used_wordlist.words)

    def transform_gene(self, only_features=False):
        to_search = str(self.current_ebook)
        iloc = self.corpus_names.index(to_search)

        #TODO Debogue ceci
        tf_vector = self._model.getrow(iloc).toarray().ravel()
        genes = map(self.dna_tree.convert, tf_vector)
        if only_features:
            return list(tf_vector)
        else:
            return ''.join(genes)

    def summary(self):
        results = []
        dict_itmes = self.most_common_words.items()
        sorted_dict_items = sorted(dict_itmes, key=itemgetter(0), reverse=True)
        normalized_dict_items = normalized_items(sorted_dict_items)

        features = (item[1] for item in normalized_dict_items)
        dna = (self.dna_tree.convert(item[1]) for item in normalized_dict_items)
        names = map(itemgetter(0), sorted_dict_items)

        for name, feature, d in zip(names, features, dna):
            results.append({
                'name': name,
                'feature': feature,
                'dna': d
            })
        return pd.DataFrame.from_records(results)

