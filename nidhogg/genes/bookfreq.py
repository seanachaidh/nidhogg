import os, unittest, sys, pickle
from copy import copy, deepcopy
from nltk.probability import FreqDist
from nltk.corpus import wordnet

from itertools import chain
from operator import itemgetter
from copy import copy, deepcopy

import xml.etree.ElementTree as ET

from ..utils import ebooks

from .import genebase


def check_item_in_results(results, item):
    for r in range(len(results)):
        current = results[r]
        if any(i in current[0] for i in item[0]):
            return r
    return -1

def title_to_filename(title):
    #Eerst verwijderen we alle spaties en vervangen we ze door onderliggende streepjes
    first = '_'.join(title.split(' '))
    foute_tekens = list("'/;,:.()")
    filtered = [x for x in first if not x in foute_tekens]
    return ''.join(filtered) + '.freq'


def extract_synonyms(word):
    results = []
    syns = wordnet.synsets(word, 'n')
    #We nemen hier enkele het eerste woord. Ik weet niet direct of dit goed is.
    if len(syns) > 0:
        j = syns[0]
        # hypo = list(chain(*[x.lemma_names() for x in j.hyponyms()]))
        lemmas = j.lemma_names()
        hyper = list(chain(*[x.lemma_names() for x in j.hypernyms()]))
        results = lemmas + hyper
    return results

class BookFreq:

    def serialize(self):
        serial_filename = title_to_filename(self.freqtitle)

        if not os.path.isdir('frequencies'):
            os.mkdir('frequencies')
        final_path = os.path.join('frequencies', serial_filename)
        print('serializing frequency:', final_path)

        with open(final_path, 'wb+') as f:
            pickle.dump(self, f)

    def _search_thesaurus(self):
        results = []

        for t in range(len(self.common)):
            print('searching thesaurus', self.freqtitle, 'done:', round((t/len(self.common))*100))
            token = self.common[t]
            word = token[0]
            syn = extract_synonyms(word)
            newwords = set([word] + syn)

            found_other = False

            for r in range(len(results)):
                current = results[r]
                if word in current:
                    newfreq = current[1] + token[1]

                    results[r] = (current[0], newfreq)
                    found_other = True
            if not found_other:
                results.append((newwords, token[1]))
        return results

    def __init__(self, ebook):
        self.dist = FreqDist(ebook.only_nouns)
        # Code van stack overflow
        #TODO maak hier een parameter van
        self.freqtitle = ebook.meta.title
        self.common = self.dist.most_common()
        self.syno_freq = self._search_thesaurus()
        self.serialize()
    @staticmethod
    def load_freq(ebook):
        serial_filename = title_to_filename(ebook.meta.title)

        if not os.path.isdir('frequencies'):
            os.mkdir('frequencies')
        final_path = os.path.join('frequencies', serial_filename)
        if os.path.isfile(final_path):
            with open(final_path, 'rb') as f:
                loaded_freq = pickle.load(f)
                print('loading frequency', loaded_freq.freqtitle, 'from cache')
                return loaded_freq
        else:
            print('Creating new frequency', serial_filename)
            return BookFreq(ebook)

class DNAMapLoader(genebase.Gene):

    def _parsefile(self):
        results = []
        topics = self.tree_root.findall('./topic')

        for t in topics:
            words = t.findall('./word')
            wordset = set([w.text for w in words])
            wfreq = int(t.attrib.get('frequency', '0'))
            results.append((wordset, wfreq))
        return results

    def __init__(self, mapfilename):
        self.mapfilename = mapfilename
        self.tree = ET.parse(self.mapfilename)
        self.tree_root = self.tree.getroot()

        self.topics = self._parsefile()
    
    def create_dna(self, bfreq: BookFreq) -> str:
        results = ''
        for t in self.topics:
            val = check_item_in_results(bfreq.syno_freq, t)
            if val > 0:
                results += 'A'
            else:
                results += 'C'
        return results

    def transform_gene(self, book):
        freq = BookFreq(book)
        retval = self.create_dna(freq)
        return retval

        
class DNAMapCreator:

    def create_topic_list(self):
        results = []

        for f in self.freqs:
            print('content dna map:', f.freqtitle)
            freqlist = f.syno_freq
            for item in freqlist:
                index = check_item_in_results(results, item)
                if not index < 0:
                    found_result = results[index]
                    newset = item[0].union(found_result[0])
                    new_frequency = found_result[1] + item[1]

                    bfreq = found_result[2] + 1

                    newresult = (newset, new_frequency, bfreq)
                    results[index] = newresult
                else:
                    wordset, freq = item
                    results.append((wordset, freq, 0))
        return sorted(results, key=itemgetter(1), reverse=True)

    def safe_topic_map(self, outfile):
        rootattr = {
            'corpusdir': self.corpusdir
        }
        root = ET.Element('topicmap', rootattr)

        for f in range(len(self.fused_freqs)):
            current = self.fused_freqs[f]
            attributes = {
                'order': str(f + 1),
                'frequency': str(current[1])
            }
            topic = ET.SubElement(root, 'topic', attrib=attributes)
            for word in current[0]:
                w = ET.SubElement(topic, 'word')
                w.text = word
        
        with open(outfile, 'wb') as f:
            tree = ET.ElementTree(root)
            tree.write(f)

    def _create_frequencies(self):
        results = []
        for c in self.corpus.books:
            print('Creating frequencies for:', c.meta.title)
            freq = BookFreq.load_freq(c)
            results.append(freq)
        return results

    def __init__(self, corpusdir, wlist = None):
        self.corpusdir = corpusdir
        self.corpus = ebooks.EbookCorpus(corpusdir, wlist=wlist)
        
        self.freqs = self._create_frequencies()
        self.fused_freqs = self.create_topic_list()
