from ..utils import ebooks
from . import genebase
from ..utils import dna

from operator import attrgetter
import pickle, os

from gensim.corpora.dictionary import Dictionary
from operator import itemgetter
from ..utils.general import Singleton


import gensim.models.ldamodel as lda

class LDATopic(genebase.Gene, metaclass=Singleton):

    setting_encoding = os.path.join("encoding_trees", "bigtree.xml")
    setting_model_filename = os.path.join("datadir", "model.gensim")
    setting_encoding_id = 1
    setting_corpus = "corpus"
    
    # Extra instellingen voor het encoderen
    setting_simple_encoding = True

    def __init__(self):

        super(LDATopic, self).__init__()

        self.corpus = ebooks.EbookCorpus(self.setting_corpus)
        self.corpus_list = [book.only_nouns for book in self.corpus.books]
        self.common_dictionary = Dictionary(self.corpus_list)
        self.common_corpus = map(lambda x: self.common_dictionary.doc2bow(x), self.corpus_list)

        self._enctree = dna.DNATreeLoader(self.setting_encoding)

        # Een beetje meer dan de default. Lijkt me goed
        if not os.path.exists(self.setting_model_filename):
            self.model = lda.LdaModel(corpus=self.common_corpus, num_topics=100)
            self.save_model(self.setting_model_filename) # Hier is eigenlijk geen aparte functie voor nodig
        else:
            #Laad het model van het opgeslagen bestand
            self.model = lda.LdaModel.load(self.setting_model_filename)


    def predict_topics(self, ebook: ebooks.EPUB, simple: bool):
        doc = ebook.only_nouns
        bow = self.common_dictionary.doc2bow(doc)
        print('docbow {0} met lengte {1}'.format(bow, len(bow)))
        distr = self.model.get_document_topics(bow, (None if simple else 0))
        return distr

    def save_model(self, filename):
        self.model.save(filename)
    
    @staticmethod
    def load_from_file(filename):
        pass

    # Implemented methods
    def load_data(self, ebook):
        self.loaded_book = ebook
        self.loaded_distr = self.predict_topics(ebook, self.setting_simple_encoding)
    def transform_gene(self):
        #Transoforms the gene of the current loaded ebook
        if self.setting_simple_encoding:
            return dna.bools_to_dna(map(lambda x: (True if x in map(itemgetter(0), self.loaded_distr) else False), range(100)))
        else:
            return ''.join(map(lambda x: self._enctree.convert(self.setting_encoding_id, x[1]), self.loaded_distr))
