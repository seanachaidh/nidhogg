import os
import numpy as np
from itertools import islice
from operator import itemgetter
from . import genebase
from ..utils import dna
from ..utils import ebooks
from ..utils.general import Singleton
from copy import deepcopy

def window(seq, n=2):
    "Returns a sliding window (of width n) over data from the iterable"
    "   s -> (s0,s1,...s[n-1]), (s1,s2,...,sn), ...                   "
    it = iter(seq)
    result = list(islice(it, n))
    if len(result) == n:
        yield result    
    for elem in it:
        result = result[1:] + [elem]
        yield result

def calcuate_stat(wordlist: list) -> float:
    unique = set(wordlist)
    return len(unique)/len(wordlist)

class VocVar(genebase.Gene):
    setting_encoding_id = 1
    setting_encoding = os.path.join("encoding_trees", "bigtree.xml")
    setting_window_length = 500
    setting_sample_length = 250


    def load_data(self, ebook: ebooks.EPUB):

        words = deepcopy(ebook.only_nouns) #Waarom maak ik hier een kopie van?
        # all_unique = set(words)
        # self.statistic = len(all_unique)/len(words)
        windows = window(words, self.setting_window_length)
        tmp_stat_values = [calcuate_stat(x) for x in windows]
        stat_values = np.array(tmp_stat_values)

        idx = np.round(np.linspace(0, len(stat_values) - 1, self.setting_sample_length)).astype(int)

        stepped_stat_values = stat_values[idx]
        self.gene_iter = (self.mytree.convert(self.setting_encoding_id, x) for x in stepped_stat_values)

    def __init__(self):
        super(VocVar, self).__init__()
        self.mytree = dna.DNATreeLoader(self.setting_encoding)
    
    def transform_gene(self):
        # return self.mytree.convert(self.setting_encoding_id, self.statistic)
        # return ''.join(map())
        return ''.join(self.gene_iter)
