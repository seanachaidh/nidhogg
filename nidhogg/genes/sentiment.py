import os
from ..utils.ebooks import EPUB
from ..utils.dna import DNATreeLoader

from nltk.sentiment import SentimentAnalyzer
from nltk.sentiment.util import mark_negation
from . import genebase


# Enkele utilityfuncties
def create_lexicon(filename):
	lexdict = dict()
	with open(filename,'r') as f:
		for line in f:
			if not line.startswith('#') and not line == '':
				items = line.split('\t')
				#print(items)
				posscore = items[2]
				negscore = items[3]
				
				if not negscore == '' and not posscore == '':
					
					words = items[4].split(' ')
					#print(negscore)				
					for w in words:
						#print((w.split('#')[0],posscore,negscore))
						neutralscore = 1 - (float(posscore) + float(negscore))
						lexdict[w.split('#')[0]] = (float(posscore), float(negscore), neutralscore)
	return lexdict


class Sentiment(genebase.Gene):
	# Same encoding
	setting_encoding = os.path.join('encoding_trees', 'bigtree.xml')
	setting_encoding_id = 1
	setting_function_words = os.path.join("datadir", "function_words")
	setting_lexicon = os.path.join("datadir", "lexicon.txt")

	def __init__(self):
		super(Sentiment, self).__init__()
		self.analyzer = SentimentAnalyzer()
		self.dna_tree = DNATreeLoader(self.setting_encoding)

		self.totalneg = 0
		self.totalpos = 0

		self.lexicon = create_lexicon(self.setting_lexicon)

	def extract_features(self, docs,lex):
		'''
			Voordat een document met deze functie ganalyseerd wordt moeten de woorden eerst
			gemarkeerd worden op negatie. Te doen met NLTK
		'''
		
		for d in range(len(docs)):
			word = docs[d] # We doen hier eigenlijk niets met de tweede waarde van de tupel
			if word.endswith('_NEG'):
				negate = True
				word = word[:-4]
			else:
				negate = False
			
			pos, neg, neutral = lex.get(word,(0,0,0)) # Weet niet echt wat ik moet doen met de neutrale waarde
			if pos < neg:
				if negate:
					self.totalpos += 1
				else:
					self.totalneg += 1
			else:
				if negate:
					self.totalneg += 1
				else:
					self.totalpos += 1
				
		
	def load_data(self, ebook):
		negative_marked_book = mark_negation([x[0] for x in ebook.raw])
		self.extract_features(negative_marked_book, self.lexicon)

	def transform_gene(self):
		percent_neg = self.totalneg / (self.totalpos + self.totalneg)
		percent_pos = self.totalpos / (self.totalpos + self.totalneg)
		
		negdna = self.dna_tree.convert(self.setting_encoding_id, percent_neg)
		posdna = self.dna_tree.convert(self.setting_encoding_id, percent_pos)

		return negdna + posdna
		
