import os
from . import genebase
from nltk.probability import FreqDist
from ..utils.ebooks import EPUB
from ..utils.wordlist import WordList
from ..utils.dna import DNATreeLoader
from..utils.general import Singleton
from ..utils.general import AttrDict
from operator import itemgetter
import pandas as pd

class Person(genebase.Gene, metaclass=Singleton):
    setting_encoding = os.path.join('encoding_trees', 'bigtree.xml')
    setting_encoding_id = 1
    setting_datafile = os.path.join('datadir', 'personen.ods')
    setting_datafields = [
        "Leeftijd",
        "Lengte"
    ]

    def normalized_item(self, item, label):
        minimum, maximum = self.limvals[label]
        
        return (item - minimum)/(maximum - minimum)

    def __fill_limvals (self):
        for d in self.setting_datafields:
            maxval = max(map(lambda x: x[d], self.myframe.to_dict('records')))
            minval = min(map(lambda x: x[d], self.myframe.to_dict('records')))
            self.limvals[d] = (minval, maxval)


    def __init__(self):
        super(Person, self).__init__()
        self.current_data = None
        self.dna_tree = DNATreeLoader(self.setting_encoding)
        self.myframe = pd.read_excel(self.setting_datafile, engine="odf")

        self.limvals = dict()
        self.__fill_limvals()


    def load_data(self, book):
        self.current_data = book # De parameternaam "book" is hier een beetje vreemd gezien het hier niet over boeken gaat
    def transform_gene(self):
        results = str()
        for d in self.setting_datafields:
            val = self.current_data[d]
            norm_val = self.normalized_item(val, d)
            dna_val = self.dna_tree.convert(self.setting_encoding_id, norm_val)

            results += dna_val
        return results
