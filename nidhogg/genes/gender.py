import os
import pandas as pd

from . import genebase
from nltk.probability import FreqDist

from ..utils.dna import DNATreeLoader
from ..utils.wordlist import WordList

from operator import itemgetter

def normalized_items(items):
    maximum = max(items, default=0, key=itemgetter(1))[1]
    minimum = min(items, default=0, key=itemgetter(1))[1]
    
    newitems = [(x, (y - minimum)/(maximum - minimum)) for x, y in items if not (maximum - minimum) == 0]
    return newitems

class Gender(genebase.Gene):
    #TODO Maak hiervoor een default in de basisklasse
    setting_encoding = os.path.join('encoding_trees', 'bigtree.xml')
    setting_encoding_id = 1
    setting_use_lemma = False
    setting_sklearn_model = os.path.join('datadir', 'gender.model')

    setting_function_words = os.path.join("datadir", "function_words")
    
    def __init__(self, corpus):
        super(Gender, self).__init__(corpus)
        
        self.used_wordlist = WordList(self.setting_function_words)
        self.most_common_words = self._generate_common_words()

        self.dna_tree = DNATreeLoader(self.setting_encoding, self.setting_sklearn_model)

    def _generate_common_words(self):
        results = {}
        for w in self.used_wordlist.words:
            results[w] = 1
        return results

    def generate_count(self, text, words):
        result = {x: 0 for x in words}
        for t in text:
            tmpres = result[t] # We gaan ervan uit dat er geen onbekende sleutelwaarden voorkomen in de words variabele
            result[t] = tmpres + 1
        return result

    def load_data(self, ebook):
        used_function_words = list(map(lambda x: x, ebook.only_in_wordlist(self.used_wordlist, self.setting_use_lemma)))
        self.most_common_words = self.generate_count(used_function_words, self.used_wordlist.words)

    def transform_gene(self, only_features=False):
        results = str()
        dict_itmes = self.most_common_words.items()
        # Items worden alfabetisch gesorteerd
        sorted_dict_items = sorted(dict_itmes, key=itemgetter(0), reverse=True)
        normalized_dict_items = normalized_items(sorted_dict_items)
        if only_features:
            return [item[1] for item in normalized_dict_items]
        else:
            for item in normalized_dict_items:
                dna_item = self.dna_tree.convert(item[1])
                results += dna_item
            return results

    def summary(self):
        results = []
        dict_itmes = self.most_common_words.items()
        sorted_dict_items = sorted(dict_itmes, key=itemgetter(0), reverse=True)
        normalized_dict_items = normalized_items(sorted_dict_items)

        features = (item[1] for item in normalized_dict_items)
        dna = (self.dna_tree.convert(item[1]) for item in normalized_dict_items)
        names = map(itemgetter(0), sorted_dict_items)

        for name, feature, d in zip(names, features, dna):
            results.append({
                'name': name,
                'feature': feature,
                'dna': d
            })
        return pd.DataFrame.from_records(results)
