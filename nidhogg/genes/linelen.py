import os
from numpy import array_split as window
from numpy import linspace, nan, isnan
from copy import deepcopy
import pandas as pd
from operator import itemgetter

from nltk.tokenize import word_tokenize

from . import genebase
from ..utils.dna import DNATreeLoader

def normalized_items(items):
    maximum = max(items, default=0)
    minimum = min(items, default=0)
    
    newitems = [(y - minimum)/(maximum - minimum) for y in items if not (maximum - minimum) == 0]
    return newitems

def calculate_mean(sentences):
    words = map(word_tokenize, sentences)
    lenghts = map(len, words)
    return sum(lenghts) / len(sentences)

class Linelen(genebase.Gene):
    setting_encoding = os.path.join('encoding_trees', 'bigtree.xml')
    setting_encoding_id = 1
    setting_window_amount = 291
    setting_sklearn_model = os.path.join("datadir", "Linelen.model")

    def interpolate(self, sequence):
        newsequence: list = deepcopy(sequence)
        diff = abs(len(sequence) - self.setting_window_amount)
        space = linspace(0, len(sequence)-1, num=diff, dtype=int)

        for i, index in enumerate(space): newsequence.insert(index+i, nan)
        retval: list = deepcopy(newsequence)

        for i, r in enumerate(retval):
            if isnan(r):
                if i == 0:
                    retval[i] = retval[i+1]
                if i == len(newsequence) - 1:
                    retval[i] = retval[i-1]
                else:
                    retval[i] = retval[i-1] + retval[i+1] / 2
        return retval
    def __init__(self, corpus):
        super(Linelen, self).__init__(corpus)
        self.dna_tree = DNATreeLoader(self.setting_encoding, self.setting_sklearn_model)

    def load_data(self, ebook):
        self.ebook = ebook
        self.sequences = list(filter(lambda x: x.size != 0, window(ebook.lines, self.setting_window_amount)))
    def transform_gene(self, only_features=False):
        means = list(map(calculate_mean, self.sequences))
        if len(means) < 291:
            means = self.interpolate(means)

        norm_means = normalized_items(means)
        if only_features:
            return norm_means
        else:
            return ''.join(list(self.dna_tree.convert(k) for k in norm_means))
