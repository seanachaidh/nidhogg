from . import gene_settings_dir
from . import genebase

import os, json

#SKLEARN
from sklearn.neighbors import NearestNeighbors
from nltk.probability import FreqDist
from nltk import ngrams

example_booklist = []
# Wij gebruiken prob_classify voor dit
class PureStylo(genebase.Gene):

    def __check_settings__(self):
        filename = os.path.join(gene_settings_dir, 'settings_purestylo.json')
        if os.path.exists(filename):
            with open(filename, 'r') as f:
                jobj = json.load(f)
            self.alg = jobj['algorithm']
        else:
            newobj = dict()
            newobj['algorithm'] = 'knn'

            self.alg = 'knn'
            with open(filename, 'w') as f:
                json.dump(newobj, newobj)


    def train(self, bookset):
        
        #TODO Ik moet nog eens kijken hoeveel buren ik precies moet gebruiken
        self.agg = NearestNeighbors()
        bookX = []
        
        for b in bookset:
            fdist = FreqDist(b.tokens)
            common = fdist.most_common(100)
            
            inputlist = []
            for c in common:
                inputlist.append(c[0])
                inputlist.append(c[1])
            bookX.append(inputlist)
        self.agg.fit(bookX)
            

    def classify(self, book):
        fdist = FreqDist(book.tokens)
        common = fdist.most_common(100)
        
        X = []
        for c in common:
            X.append(c[0])
            X.append(c[1])
        
        return self.agg.kneighbors(X)
        
    def __init__(self):
        pass
        
# END OF CLASS
