import nltk, os
from . import genebase
from operator import itemgetter

from ..utils import ebooks, dna
from ..utils.general import Rhymer, Singleton


entries = list(nltk.corpus.cmudict.entries())

class RhymerCache(metaclass=Singleton):
	def __init__(self):
		self.cache = {}
	def get_rhymer(self, level: int) -> Rhymer:
		try:
			return self.cache[level]
		except KeyError:
			obj = Rhymer(level)
			self.cache[level] = obj
			return obj


#Misschien deze functie best gewoon in utils plaatsen?
def rhyme(inp, level):
	syllables = [(word, syl) for word, syl in entries if word == inp]
	rhymes = []
	for (word, syllable) in syllables:
		 rhymes += [word for word, pron in entries if pron[-level:] == syllable[-level:]]
	return set(rhymes)

class Rhyme(genebase.Gene, metaclass=Singleton):
	setting_encoding = os.path.join("encoding_trees", "bigtree.xml")
	setting_encoding_id = 1
	setting_level = 2
	setting_window_width = 4

	def _does_any_rhyme(self, words):
		retval = list()
		listwords = list(words)
		if not self.pattern:
			for x in range(len(listwords)):
				for y in range(x + 1, len(listwords)):
					if self.rhymer.does_rhyme(listwords[x], listwords[y]):
						retval.append((x, y))
		else:
			for x in range(len(listwords)):
				pattern_id = self.pattern[x]
				for y in range(x + 1, len(listwords)):
					if self.pattern[y] == pattern_id and self.rhymer.does_rhyme(listwords[x], listwords[y]):
						retval.append((x, y))
		return retval

	def truth_value_analysis(self, book: ebooks.EPUB, rhymes: list) -> list:
		retval = [False] * len(book.scentences)
		for i in map(itemgetter(0), rhymes):
			retval[i] = True
		return retval

	def __init__(self):
		super(Rhyme, self).__init__()
		self.rhymer = Rhymer(self.setting_level)
		self.dna_tree = dna.DNATreeLoader(self.setting_encoding)

		self.pattern = []

	def load_data(self, ebook: ebooks.EPUB):
		retval = set()
		window = []

		for i, s in enumerate(ebook.scentences):
			#skip the punctuaion, get the last word
			if len(s) == 1:
				continue
			
			last_word = s[-2]
			if len(window) < self.setting_window_width:
				window.append((i, last_word))
			else:
				window = window[1:]
				window.append((i, last_word))
			rhymed = self._does_any_rhyme(map(itemgetter(1), window))
			retval = retval.union(rhymed)
		self.rhymes = retval
		self.ebook = ebook

	def transform_gene(self):
		N = len(self.ebook.scentences)
		truth = self.truth_value_analysis(self.ebook, self.rhymes)
		k = sum(map(int, truth))
		to_transform = k/N

		return self.dna_tree.convert(self.setting_encoding_id, to_transform)