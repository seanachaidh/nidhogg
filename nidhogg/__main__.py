import sys
import os
import pdb
import urllib
import importlib
import inspect
import tabulate
from . import utils, genes, commands
from tabulate import tabulate

import dendropy

from operator import attrgetter

def get_all_commands(modname):
    result = []
    module = inspect.importlib.import_module(modname)
    to_handle = [module]

    while to_handle:
        current = to_handle.pop()
        for _, x in inspect.getmembers(current):
            if inspect.isclass(x) and commands.basecommands.BaseCommand in inspect.getmro(x):
                result.append(x)
            # De startswith is nodig om ervoor te zorgen dat enkel modules binnen de module in de parameter
            # modname worden geïmporteerd
            elif inspect.ismodule(x) and x.__name__.startswith(modname):
                to_handle.append(x)
    return result


class Application:
    def __init__(self, args):
        self.args = args

    def print_spec(self, arg_spec: dict):
        itemiterator = arg_spec.items()
        table_iterator = ((x, y, z.__name__) for x, (y, z) in itemiterator)
        to_print = tabulate(table_iterator, headers=['parameter', 'default', 'type'])
        print(to_print)

    def execute(self):
        # pdb.set_trace()
        found_commands = get_all_commands('nidhogg.commands')
        to_execute = self.args[1]
        command_class = commands.basecommands.get_command_class(commands.basecommands.command_to_class_name(to_execute), found_commands)
        if command_class is None:
            print("Unknown command:", to_execute)
            sys.exit(1)
        
        #Activate help
        if self.args[2] == '--help':
            self.print_spec(command_class.arg_spec)
            sys.exit(1)

        command_args = self.args[2:]
        print('executing command:', command_class.__name__)
        print('arguments:', command_args)
        obj = command_class(command_args)
        obj.execute()


myapp = Application(sys.argv)
myapp.execute()
