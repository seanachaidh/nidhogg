import sys
import pandas as pd

from . import basecommands
from ..utils import ebooks, general, tree
from .. import genes

import inspect
from enum import Enum
from ..utils.CDHITPipe import CDHITPipe

#Biopython modules
from Bio import Phylo
from Bio import Seq
from Bio import SeqIO
from Bio import SeqRecord
from Bio.Alphabet import DNAAlphabet
from Bio.Align.Applications import TCoffeeCommandline


class DNAFileType(Enum):
    NEXUS = 'nexus'
    FASTA = 'fasta'

class AlignDna(basecommands.BaseCommand):
    arg_spec = {
        'input': ('input.fasta', str),
        'output': ('output.aln', str)
    }

    def execute(self):
        mycmd = TCoffeeCommandline(infile=self.config.get('input'), output='clustalw', outfile=self.config.get('output'))
        print('executing command: %s' % mycmd)
        mycmd()

class GenerateClusterTable(basecommands.BaseCommand):
    arg_spec = {
        'input': ('input.fasta', str),
        'output': ('output.csv', str),
        'threshold': (0.9, float)
    }

    def execute(self):
        mypipe = CDHITPipe(self.config.get('input'), threshold=self.config.get('threshold'))
        mypipe.execute()
        frame = mypipe.to_dataframe()
        frame.to_csv(self.config.get('output'), sep=';', index=False)

class CreateDna(basecommands.BaseCommand):
    arg_spec = {
        'genes': ('', str),
        'corpus': ('corpus', str),
        'output': ('output.nexus', str),
        'format': (DNAFileType.NEXUS, DNAFileType)
    }

    def _write_nexus(self):
        my_nexus = tree.NexusFile(self.config.get('output'), self.dna_matrix)
        my_nexus.write_file()
    def _write_fasta(self):
        sequence_list = list()

        for bname, dna in self.dna_matrix:
            current_book = self.my_corpus.get_book_by_name(bname)
            b_id, b_title, b_author, b_publisher, _ = current_book.meta_tuple()

            current_seq = Seq.Seq(dna, DNAAlphabet.letters)
            current_record = SeqRecord.SeqRecord(current_seq, id=b_id, name=b_title,
                description="{0} by {1} from {2}".format(b_title, b_author, b_publisher))

            sequence_list.append(current_record)
        for x in sequence_list:
            print('created record %s' % x)
        
        SeqIO.write(sequence_list, self.config.get('output'), 'fasta')

    def _collect_genes(self):
        gene_list = self.config.get('genes').split(',')
        all_classes = general.recursive_classes('nidhogg.genes')
        filtered_genes = list(filter(lambda x: x.__name__ in gene_list and genes.genebase.Gene in inspect.getmro(x), all_classes))
        self.collected_genes = filtered_genes
    def execute(self):
        self._collect_genes()
        # HAHAHA now the magic will happen!!!
        #function_words = utils.wordlist.WordList(os.path.join('datadir', 'function_words'))

        self.my_corpus = basecommands.call_copus(self.config.get('corpus'))
        self.dna_matrix = self.my_corpus.apply_genes(self.collected_genes)

        output_mode = self.config.get('format')
        if output_mode == DNAFileType.NEXUS:
            self._write_nexus()
        elif output_mode == DNAFileType.FASTA:
            self._write_fasta()
        else:
            print('Uknown file format in dna generator')
            sys.exit(1)
