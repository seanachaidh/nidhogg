import os, sys, re, inspect, tabulate, dendropy, urllib, math
from operator import attrgetter, itemgetter
from ..utils import ebooks, metrics, tree, gutenberg, dna, general, simpletext
import pandas as pd

from nltk import FreqDist
import matplotlib.pyplot as plt

from enum import Enum

import json

from .. import genes

def call_copus(corpus_spec, *args, **kwargs):
    corpus_type, corpus_dir = corpus_spec.split(':')
    if corpus_type == 'simple':
        return simpletext.SimpleCorpus(corpus_dir, **kwargs)
    if corpus_type == 'epub':
        return ebooks.EbookCorpus(corpus_dir, **kwargs)
    if corpus_type == 'meta':
        return simpletext.ExcelCorpus(corpus_dir)
    else:
        raise Exception('Unknown corpus type: %s' % corpus_type)

def get_all_commands():
    module = inspect.importlib.import_module('nidhogg.commands')
    return inspect.getmembers(module, lambda x: inspect.isclass(x) and BaseCommand in inspect.getmro(x))

def get_command_class(name: str, classes: list):
    try:
        return next(x for x in classes if x.__name__ == name)
    except StopIteration:
        return None
    

def command_to_class_name(command: str) -> str:
    return ''.join(map(str.capitalize, command.split('-')))

class BaseCommand:
    #Klassevariabelen

    '''
        specifiecatie
            dit is een woordenboek met als sleutelwaarden de namen van de argumenten die door kunnnen gegeven worden aan de opdracht
            Iedere naam is verbonden aan een tupel die verder specifiëert hoe het argument afgehandelt moet worden
    '''
    arg_spec = {}

    def _set_defaults(self):
        for key, spec in self.arg_spec.items():
            default = spec[0]
            self.config[key] = default

    # Privémethodes
    def _parse_arguements(self):
        current_option = None
        for arg in self.arguments:
            if arg.startswith('--'):
                current_option = arg[2:]
                spec = self.arg_spec.get(current_option)

                if spec is None: raise Exception('Illegal argument in command: %s' % current_option)
                
                option_type = spec[1]
            else:
                if option_type == bool:
                    self.config[current_option] = True
                else:
                    self.config[current_option] = option_type(arg)

    def __init__(self, arguments):
        self.config = {}
        self.arguments = arguments

        self._set_defaults()
        self._parse_arguements()

    def execute(self):
        raise NotImplementedError

class ListDetails(BaseCommand):
    arg_spec = {
        'corpus': ('corpus', str),
        'corpusmode': ('simple', str),
        'filter': ('', str),
        'output': ('', str)
    }
    def execute(self):
        corpus_dir = self.config.get('corpus')
        corpus_mode = self.config.get('corpusmode')
        
        if corpus_mode == 'simple':
            my_corpus = simpletext.SimpleCorpus(corpus_dir)
        elif corpus_mode == 'epub':
            my_corpus == ebooks.EbookCorpus(corpus_dir)

        my_corpus = ebooks.EbookCorpus(corpus_dir)
        toprint = zip(map(attrgetter('filename'), my_corpus.books), map(str, my_corpus.books))

        if not self.config.get('filter') == '':
            toprint = filter(lambda x: x[1].contains(self.config.get('filter')), toprint)
        
        out = self.config.get('output')
        if out == '':
            print(tabulate.tabulate(toprint, headers=['Filename', 'Title']))
        else:
            pass
class ClusterTree(BaseCommand):
    arg_spec = {
        'input': ('default.nexus', str),
        'level': (3, int),
        'output': ('', str),
        'append': (False, bool)
    }

    def execute(self):
        clusters = dict()

        input_file = self.config.get('input')
        #TODO: Extra parameters suppress_internal_node_taxa en suppress_leaf_node taxa toegevoegd om fouten te voorkomen
        tree = dendropy.Tree.get(schema='nexus', path=input_file, tree_offset=0, suppress_edge_lengths=True)
        iterator = list(metrics.level_iterator(tree, self.config.get('level'))) # Moet nog een parameter maken om het niveau van de iterator te kunnen veranderen

        for i, node in enumerate(iterator):
            key = 'cluster %d' % i
            for leaf in node.leaf_iter():

                current_list = clusters.get(key, [])
                current_list.append(leaf.taxon.label)
                clusters[key] = current_list #is dit wel nodig?

        # Moet ik hier wel rekening houden met het aanvoegen van een boom?

        if self.config.get('output') == '':
            for k, i in clusters.items():
                print("taxa in cluster %s" % k)
                for t in i:
                    print('\t%s' % t)
        else:
            jsonstring = json.dumps(clusters)
            with open(self.config.get('output'), 'w') as f:
                f.write(jsonstring)

class DownloadCorpus(BaseCommand):
    arg_spec = {
        'url': ('', str),
        'output': ('corpus', str)
    }

    def execute(self):
        parsed_url = urllib.parse.urlparse(self.config.get('url'))
        if parsed_url.netloc == 'www.gutenberg.org':
            obj = gutenberg.BookShelf(parsed_url)
            # hier moet dit een map voorstellen. Ik kan dit misschien softwarematig nakijken
            obj.download_all(self.config.get('output'))
        else:
            raise Exception('Unknown location: %s' % (parsed_url.netloc))

class DnaTree(BaseCommand):

    arg_spec = {
        'length': (100, int),
        'amount': (3, int),
        'output': ('mytree.xml', str)
    }

    def execute(self):
        encodings = []
        for i in range(self.config.get('amount')):
            newcombo = dna.generate_combo(self.config.get('length'))
            print("Combo {0} generated: {1}".format(i, newcombo))
            encodings.append(newcombo)
        dna.paths_to_encoding(encodings, self.config.get('output'))

class GenerateTree(BaseCommand):
    arg_spec = {
        'input': ('input.nexus', str),
        'name': ('kabbalah', str)
    }
    def execute(self):
        tree.nexus_generate_tree(self.config.get('input'), self.config.get('name'))

class BalanceTree(BaseCommand):
    arg_spec = {
        'tree': ('tree.xml', str),
        'encid': (1, int),
        'output': ('outtree.xml', str),
        'gene': ('Gender', str),
        'corpus': ('corpus', str)
    }
    
    def count_sum(self):
        return sum(map(itemgetter(1), self.dna_count))

    #TODO: is dit wel economisch?
    def percents(self):
        accu_rem = 0
        ga = self.tree.get_gene_amount(1)
        s = self.count_sum()
        result = []
        for xi, xj in self.dna_count:
            # yj = ((xj/self.count_sum)/100)*ga
            yj = (ga*xj)//s
            accu_rem += (ga*xj)%s
            result.append((xi, yj))

        return (result, (ga - sum(map(itemgetter(1), result))))

    def part_share(self):
        # amount = self.tree.get_gene_amount(1)
        res, rem = self.percents()
        px = list(map(itemgetter(1), res))
        px = general.distribute(px)
        for _ in range(rem):
            max_i, max_val = max(enumerate(px), key=itemgetter(1))
            px[max_i] = max_val + 1

        # perc = map(lambda x: 100*x, map(itemgetter(1), self.percents()))
        return list(zip(map(itemgetter(0), res), px))

    def _generate_dna_count(self):
        geneclass = get_gene_class(self.config.get('gene'))
        counts = self.tree.generate_dna_distribution(self.config.get('encid'), geneclass,call_copus(self.config.get('corpus')))

        return list(zip(self.tree.get_all_limits(self.config.get('encid')), counts))

    def _redistribute(self):
        new_limits = []

        parts = list(self.part_share())
        consecutive = False

        current_limit_begin = None
        current_limit_end = None

        for l, p in parts:
            lower, upper = l
            if p == 0:
                if consecutive:
                    current_limit_end = upper
                else:
                    consecutive = True
                    current_limit_begin = lower
                    current_limit_end = upper
            else:
                if consecutive:
                    consecutive = False # consecutive END
                    new_limits.append((current_limit_begin, current_limit_end))
                split = list(general.split_up(float(lower), float(upper), int(p)))
                new_limits += split
        if consecutive:
            #TODO: Hier is een foutje. De tellingen moeten nog herverdeeld worden
            new_limits.append((current_limit_begin, current_limit_end))
        return new_limits

    def execute(self):
        # self.matrix = dendropy.DnaCharacterMatrix.get(path=self.config.get('input'), schema='nexus')
        self.tree = dna.DNATreeLoader(self.config.get('tree'))
        # self.corpus = ebooks.Corpus(self.config.get('corpus'))


        self.dna_count = self._generate_dna_count()
        limits = self._redistribute()
        self.tree.set_new_limits_two(1, limits)
        self.tree.save_copy(self.config.get('output'))

def get_gene_class(gene_name: str):
	gene_classes = general.recursive_classes('nidhogg.genes')
	return next(filter(lambda x: x.__name__ == gene_name and genes.genebase.Gene in inspect.getmro(x), gene_classes))

class TableOutputType(Enum):
    CSV = 'csv'
    LATEX = 'latex'

class CorpusMode(Enum):
    EPUB = 'epub'
    SIMPLE = 'simple'

    
class FilterByMeta(BaseCommand):
    # Wanneer er geen expressie is opgegeven matchen we met alles van een gegeven veld

    arg_spec = {
        'field': ('author', str),
        'list': (False, bool),
        'corpus': ('corpus', str),
        'expr': ('.*', str),
        'output': ('', str),
        'outtype': (TableOutputType.CSV, TableOutputType)
    }


    #TODO: Dit staat misschien best in de EBook module
    def get_book_value(self, book, field):
        metabook = getattr(book, 'meta', None)
        if metabook is None:
            raise Exception('Geen metadata in boek gevonden %s' % book)
        else:
            meta_field = getattr(metabook, field, None)
        
        if meta_field is None:
            raise Exception('Metadata %s niet gevonden in boek %s' % (field, book))
        else:
            return meta_field

    def execute(self):
        patt = self.config.get('expr')

        corpus = call_copus(self.config.get('corpus'))
        
        field_list = self.config.get('field').split(',')
        filtered = filter(lambda x: any(map(lambda y: re.search(patt, self.get_book_value(x, y)), field_list)), corpus.books)

        # filtered = filter(lambda x: True if not re.search(patt, self.get_book_value(x, field)) is None else True, corpus.books)
        # table = map(lambda x: (x.filename, self.get_book_value(x, field)), filtered)

        table = map(lambda x: [x.short_filename] + [self.get_book_value(x, y) for y in field_list], filtered)

        out = self.config.get('output')
        if self.config.get('list'):
            for t in table:
                print(t[0])
        else:
            if out == '':
                print(tabulate.tabulate(table, headers=['filename'] + field_list))
            else:
                dframe = pd.DataFrame(data=map(lambda x: x[1:], table), columns=field_list)
                outtype = self.config.get('outtype')
                with open(out, 'w') as f:
                    if outtype == TableOutputType.CSV:
                        dframe.to_csv(f, header=True)
                    elif outtype == TableOutputType.LATEX:
                        dframe.to_latex(buf=f, header=True, index=False)

