from enum import Enum
from math import sqrt
from operator import itemgetter
from statistics import mean
from typing import List
from itertools import chain, repeat, cycle
from os import path, mkdir, listdir, path
import os

import numpy as np
import pandas as pd
import tabulate
from Bio import Phylo, SeqIO, AlignIO
from Bio.Phylo.TreeConstruction import DistanceCalculator, DistanceTreeConstructor


from dendropy import Node, Taxon, Tree
from joblib import dump, load
from matplotlib import pyplot as plt
from nltk import FreqDist
# SKLearn modules
from sklearn.metrics import confusion_matrix, accuracy_score, adjusted_mutual_info_score, f1_score
from sklearn.metrics.cluster import contingency_matrix

from sklearn.model_selection import KFold
from sklearn.preprocessing import KBinsDiscretizer, LabelEncoder
from sklearn.utils.multiclass import unique_labels
from sklearn.feature_selection import SelectKBest
from sklearn.tree import plot_tree, DecisionTreeClassifier
from sklearn.preprocessing import OrdinalEncoder
from sklearn.pipeline import Pipeline as SKPipe

from .. import genes
from ..utils import CDHITPipe, OmegaPipe, dna, ebooks, metrics, simpletext, tree, wordlist, general
from . import basecommands, biocommands

import seaborn as sns

from scipy.stats import pearsonr as pearson_correlation
from scipy.spatial.distance import euclidean as dist_euclid

class SimpleMetricSummary(basecommands.BaseCommand):
    arg_spec = {
        'resultfolder': ('input', str),
        'cdhit': ('cdhit_result.csv', str),
        'omega': ('omega_result.csv', str)
    }
    def calculate_purity(self, exp, pred):
        matrix = contingency_matrix(exp, pred)
        return np.sum(np.amax(matrix, axis=0)) / np.sum(matrix)

    def create_result(self, filename, name):
        encoder = OrdinalEncoder(dtype=int)
        input_frame = pd.read_csv(filename, sep='|')
        real_authors = input_frame['real_author'].to_list()
        predicted_authors = input_frame['label'].to_list()

        filtered_predicted = [("unknown" if x.startswith("unknown") else x) for x in predicted_authors]
        encoder.fit(np.reshape(np.array(real_authors + ["unknown"]), (-1,1)))
        
        vals_real = np.ravel(encoder.transform(np.reshape(np.array(real_authors), (-1,1))))
        vals_pred = np.ravel(encoder.transform(np.reshape(np.array(filtered_predicted), (-1,1))))

        purity = self.calculate_purity(vals_real, vals_pred)
        accuracy = accuracy_score(vals_real, vals_pred)
        # f1 = f1_score(vals_real, vals_pred)
        mutual = adjusted_mutual_info_score(vals_real, vals_pred)
        return "%s,%.2f,%.2f,%.2f" % (name, purity, mutual, accuracy)
    def execute(self):
        output_dir = self.config.get('resultfolder')
        cdhit_file = self.config.get('cdhit')
        omega_file = self.config.get('omega')
        output_file = os.path.join(output_dir, 'metric_results.csv')

        result_cdhit = self.create_result(cdhit_file, 'CD-HIT')
        result_omega = self.create_result(omega_file, 'Clustal')

        with open(output_file, 'w') as f:
            print('method,purity,mutual information,accuracy', file=f)
            print(result_cdhit, file=f)
            print(result_omega, file=f)
        

class BulkMetricSummary(basecommands.BaseCommand):
    arg_spec = {
        'resultfolder': ('input', str),
        'cdhit': ('cdhit_result.csv', str),
        'omega': ('omega_result.csv', str)
    }
    
    def calculate_purity(self, exp, pred):
        matrix = contingency_matrix(exp, pred)
        return np.sum(np.amax(matrix, axis=0)) / np.sum(matrix) 

    def create_result(self, filename, name):
        encoder = OrdinalEncoder(dtype=int)
        input_frame = pd.read_csv(filename, sep='|')
        real_authors = input_frame['real_author'].to_list()
        predicted_authors = input_frame['label'].to_list()

        filtered_predicted = [("unknown" if x.startswith("unknown") else x) for x in predicted_authors]
        encoder.fit(np.reshape(np.array(real_authors + ["unknown"]), (-1,1)))
        
        vals_real = np.ravel(encoder.transform(np.reshape(np.array(real_authors), (-1,1))))
        vals_pred = np.ravel(encoder.transform(np.reshape(np.array(filtered_predicted), (-1,1))))

        purity = self.calculate_purity(vals_real, vals_pred)
        accuracy = accuracy_score(vals_real, vals_pred)
        # f1 = f1_score(vals_real, vals_pred)
        mutual = adjusted_mutual_info_score(vals_real, vals_pred)
        return "%s,%.2f,%.2f,%.2f\n" % (name, purity, mutual, accuracy)

    def execute(self):
        results_cdhit = []
        results_omega = []
        inputdir = self.config.get('resultfolder')

        to_calculate = os.listdir(self.config.get('resultfolder'))
        for c in to_calculate:
            print("Calculating %s" % c)
            cdhit_file = os.path.join(inputdir, c, "clusters_cdhit.csv")
            omega_file = os.path.join(inputdir, c, "clusters_omega.csv")
            
            result_omega = self.create_result(omega_file, c)
            result_cdhit = self.create_result(cdhit_file, c)
            results_cdhit.append(result_cdhit)
            results_omega.append(result_omega)
        
        with open(self.config.get('cdhit'), 'w') as f: f.writelines(results_cdhit)
        with open(self.config.get('omega'), 'w') as f: f.writelines(results_omega)

class MetricSummary(basecommands.BaseCommand):
    arg_spec = {
        'output': ('', str),
        'corpus': ('epub:corpus', str),
        'fastafile': ('input.fasta', str),
        'threshold': (0.9, float),
        'attribute': ('author', str)
    }

    def _do_analysis(self):
        mypipe = CDHITPipe.CDHITPipe(self.config.get('fastafile'), self.config.get('threshold'))
        mypipe.execute()

        return list(mypipe.make_lines(self.mycorpus))

    def _execute_metric(self, x: type):
        obj = x(self.cdict, self.config.get('attribute'))
        return obj.calculate_metric()

    def execute(self):
        self.mycorpus = basecommands.call_corpus(self.config.get('corpus'))
        self.cdict = self._do_analysis()

        metricresults = map(self._execute_metric, metrics.exported_metrics)
        metric_names = map(lambda x: x.__name__, metrics.exported_metrics)
        toprint = zip(metric_names, metricresults)

        output = self.config.get('output')
        if output == '':
            print(tabulate.tabulate(toprint, headers=["Metric name", "Result"]))
        else:
            myframe = pd.DataFrame(data=toprint, columns=["Metric name", "Result"])
            with open(output, 'w') as f:
                myframe.to_latex(f)


class CalculatePurity(basecommands.BaseCommand):
    arg_spec = {
        'output': ('', str),
        'corpus': ('epub:corpus', str),
        'fastafile': ('input.fasta', str),
        'threshold': (0.9, float)
    }

    def _do_output(self, result):
        output_string = self.config.get('output')
        if not output_string == '':
            myframe = pd.DataFrame.from_records(result)
            myframe.to_csv(output_string, sep=';', index=False)
        else:
            print('calculate-purity: No output file selected')

    def _do_analysis(self):
        mypipe = CDHITPipe.CDHITPipe(self.config.get('fastafile'), self.config.get('threshold'))
        mypipe.execute()

        return list(mypipe.make_lines(self.mycorpus))

    def execute(self):
        self.mycorpus = basecommands.call_corpus(self.config.get('corpus'))
        
        cdict = self._do_analysis()
        mymetric = metrics.Purity(cdict, 'author')
        metricresult = mymetric.calculate_metric()

        print('Cluster zuiverheid:', metricresult)

class CreateConfusionMatrix(basecommands.BaseCommand):
    # De default waarde komt van de eerste optimalisatie die we ooit deden
    arg_spec = {
        "corpus": ('epub:fantasy_corpus', str),
        "fastafile": ('input.fasta', str),
        "threshold": (0.9666, float),
        "output": ("", str),
        "simple": (False, bool),
        "omega": (False, bool),
        "kimura": (False, bool),
        "size": (10, int)
    }
    def draw_matrix(self, filename: str, cm, classes):
        img = plt.matshow(cm) # TODO Hier moet nog een goede colourmap gekozen worden
        ax = img.axes
        
        ax.set_yticklabels([""] + list(classes))
        ax.set_xticklabels([""] + list(classes))
        plt.setp(ax.get_xticklabels(), rotation=45, ha="left", rotation_mode="anchor")
        fmt = "d"
        
        for i in range(cm.shape[0]):
            for j in range(cm.shape[1]):
                ax.text(j, i, format(cm[i, j], fmt), ha="center", va="center")
        plt.colorbar()
        if not filename == '':
            plt.savefig(filename, bbox_inches="tight") # is misschien niet direct de beste kwaliteit

    def _do_analysis(self):
        if self.config.get('omega'):
            mypipe = OmegaPipe.OmegaPipe(self.config.get('fastafile'), debug=True, kimura=self.config.get('kimura'), cluster_size=self.config.get('size'))
        else:
            mypipe = CDHITPipe.CDHITPipe(self.config.get('fastafile'), accurate=True, threshold=self.config.get('threshold'))
        mypipe.execute(self.mycorpus)

        return list(mypipe.make_lines(self.mycorpus))

    def execute(self):
        self.mycorpus = basecommands.call_corpus(self.config.get('corpus'))
        cdict = self._do_analysis()
        mymetric = metrics.ConfusionMatrix(cdict, 'author') # Hier nog een inputparameter van maken: atribuut

        cm = mymetric.calculate_metric()
        self.draw_matrix(self.config.get('output'), *cm)

# Plot of the frequenies 
class CreateFeaturePlot(basecommands.BaseCommand):
    arg_spec = {
        "corpus": ("epub:corpus", str),
        "output": ("output", str),
        "genes": ("Gender", str), # Eigenlijk ga ik ervan uit dat diet altijd via Gender wordt uitgevoerd
    }

    def _plot_book(self, book, words: wordlist.WordList):
        plt.bar(list(range(1, len(words.words) + 1)), book[1])
        plt.title(book[0])

        final_path = path.join(self.config.get('output'), book[0]+'.png')
        print("Bewaren afbeelding %s" % final_path)
        plt.savefig(final_path)
        plt.close()

    def execute(self):
        if not path.isdir(self.config.get('output')):
            os.makedirs(self.config.get('output'))

        corpus = basecommands.call_corpus(self.config.get('corpus'))
        my_genes = genes.genebase.collect_genes(self.config.get('genes'))
        features = corpus.apply_genes(my_genes, only_features=True)

        # Het gebruik van deze parameter is niet zo proper
        my_wordlist = wordlist.WordList(genes.gender.Gender.setting_function_words)

        for f in features:
            self._plot_book(f, my_wordlist)

class CalculateMutualInformation(basecommands.BaseCommand):
    arg_spec = {
        'output': ('', str),
        'corpus': ('epub:corpus', str),
        'fastafile': ('input.fasta', str),
        'threshold': (0.9, float)
    }

    def _do_analysis(self):
        mypipe = CDHITPipe.CDHITPipe(self.config.get('fastafile'), self.config.get('threshold'))
        mypipe.execute()

        return list(mypipe.make_lines(self.mycorpus))

    def execute(self):
        self.mycorpus = basecommands.call_corpus(self.config.get('corpus'))
        cdict = self._do_analysis()
        mymetric = metrics.MutualInformation(cdict, 'author')
        metricresult = mymetric.calculate_metric()

        print('Mutual information: %f' % metricresult)

class PlotDispersion(basecommands.BaseCommand):
    arg_spec = {
        "corpus": ("epub:fantasy_corpus", str),
        "output": ("output", str)
    }

    #Functie afkomstig van https://www.nltk.org/_modules/nltk/draw/dispersion.html
    def dispersion_plot(self, text, words, ignore_case=False, title="Lexical Dispersion Plot", output=None):

        """
        Generate a lexical dispersion plot.

        :param text: The source text
        :type text: list(str) or enum(str)
        :param words: The target words
        :type words: list of str
        :param ignore_case: flag to set if case should be ignored when searching text
        :type ignore_case: bool
        """

        try:
            import matplotlib.pyplot as plt
        except ImportError:
            raise ValueError(
                'The plot function requires matplotlib to be installed.'
                'See http://matplotlib.org/'
            )

        text = list(text)
        words.reverse()

        if ignore_case:
            words_to_comp = list(map(str.lower, words))
            text_to_comp = list(map(str.lower, text))
        else:
            words_to_comp = words
            text_to_comp = text

        points = [
            (x, y)
            for x in range(len(text_to_comp))
            for y in range(len(words_to_comp))
            if text_to_comp[x] == words_to_comp[y]
        ]
        if points:
            x, y = list(zip(*points))
        else:
            x = y = ()
        plt.plot(x, y, "b|", scalex=0.1)
        plt.yticks(list(range(len(words))), words, color="b")
        plt.ylim(-1, len(words))
        plt.title(title)
        plt.xlabel("Word Offset")
        if output:
            plt.savefig(output)
        else:
            plt.show()


    def create_plots(self):
        corpus_names = [str(x) for x in self.corpus.books]
        # corpus_texts = [b.NLTK_text() for b in self.corpus]
        output_dir = self.config.get('output')
        
        for b, name in zip(self.corpus, corpus_names):
            tokens = b.only_tokens

            freq = FreqDist(tokens)
            most_common = freq.most_common(20)
            common_words = [x[0] for x in most_common]
            
            final_path_name = os.path.join(output_dir, name + '.png')
            final_title = "Dispersion plot for " + name
            self.dispersion_plot(tokens, common_words, output=final_path_name, title=final_title) 
    
    def execute(self):
        self.corpus = basecommands.call_corpus(self.config.get('corpus'))
        if not os.path.isdir(self.config.get('output')):
            print("Map %s bestaat niet. Map aanmaken" % self.config.get('output'))
            os.makedirs(self.config.get('output'))
        
        self.create_plots()

class PerformAnova(basecommands.BaseCommand):
    arg_spec = {
        "corpus": ("epub:corpora/fantasy_corpus", str),
        "genes": ("Gender", str),
        "output": ("output.csv", str)
    }

    def calculate_support(self, data: pd.DataFrame):
        tmp_y = data['author'].values.tolist()
        y = LabelEncoder().fit_transform(tmp_y)

        X = data.drop(["author"], axis=1)

        print(y);print(X)

        result = []
        for colname, vals in X.iteritems():
            print("Calculating correlation for %s" % colname)
            c = pearson_correlation(vals, y)
            result.append(c)
        
        result_frame = pd.DataFrame(data={
            'pearson': [x[0] for x in result],
            'pval': [x[1] for x in result]
        }, columns=['pearson', 'pval'], index=X.columns)

        print(result_frame)

        return result_frame

    def collect_results(self, corpus, gene: genes.genebase.Gene):
        result = pd.DataFrame()
        for b in corpus:
            gene.load_data(b)
            summary: pd.DataFrame = gene.summary()

            used_info = summary[['name', 'feature']]
            record = {key: val for key, val in used_info.itertuples(index=False)}
            record['author'] = b.meta.author # Add the author
            result = result.append(record, ignore_index=True)
        
        print(result)
        return result

    def execute(self):
        mygenes = genes.genebase.collect_genes(self.config.get('genes'))
        corpus = basecommands.call_corpus(self.config.get('corpus'))

        gene = mygenes[0]
        #instantiate the gene
        data = self.collect_results(corpus, gene(corpus))
        data_support = self.calculate_support(data)

        output_file = self.config.get('output')
        data_support.to_csv(output_file, sep=";")

class DnaSummary(basecommands.BaseCommand):
    arg_spec = {
        "corpus": ('epub:fantasy_corpus', str),
        "output": ('output_folder', str),
        "gene": ('Gender', str),
        "distout": ('', str)
    }



    def create_results(self, corpus, gene: genes.genebase.Gene):
        vector_space = []
        outfile = self.config.get('distout')
        
        for b in corpus:
            print("Creating summary for %s" % b)
            gene_obj = gene(corpus)
            gene_obj.load_data(b)
            mysummary: pd.DataFrame = gene_obj.summary()
            
            vector = mysummary['feature'].tolist()
            vector_space.append((str(b), vector))

            filename = str(b) if len(str(b)) < 50 else str(b)[:50]

            output_file = os.path.join(self.config.get('output'), filename + '.csv')
            mysummary.to_csv(output_file, sep='|')
        

        if outfile:
            distmat = np.zeros((len(vector_space), len(vector_space)))
            names = list(map(itemgetter(0), vector_space))
            for i, x in enumerate(vector_space):
                for j, y in enumerate(vector_space):
                    _, vecx = x
                    _, vecy = y
                    distance = dist_euclid(vecx, vecy)
                    distmat[i,j] = distance
            distmat_frame = pd.DataFrame(data=distmat, columns=names, index=names)

            distmat_frame.sort_index(axis=0, inplace=True)
            distmat_frame.sort_index(axis=1, inplace=True)

            distmat_frame.to_csv(outfile)
    def execute(self):
        corpus = basecommands.call_corpus(self.config.get('corpus'))
        genelist = genes.genebase.collect_genes(self.config.get('gene'))

        if not os.path.isdir(self.config.get('output')):
            os.makedirs(self.config.get('output'))

        if not genelist:
            raise Exception('Gene not found: % s' % self.config.get('gene'))
        else:
            self.create_results(corpus, genelist[0])



class OptimalizeDiscretization(basecommands.BaseCommand):
    # Mogelijks nog een step parameter toevoegen
    arg_spec = {
        "corpus": ("epub:fantasy_corpus", str),
        "genes": ("Gender", str),
        "output": ("output.csv", str),
        "lower": (5, int),
        "upper": (100, int)
    }
    def calculate_cross_validation(self, bins, data):

        def compare(y, y_hat):
            mymean = mean(map(lambda i, j: pow(i - j, 2), y, y_hat))
            return sqrt(mymean)

        def calculate_prediction(model, number):
            edges = model.bin_edges_[0]
            value = int(number)
            print("Predicting bin value:", value)
            x, y = (edges[value], edges[value+1])
            return (x+y)/2
        
        folding = KFold(n_splits=10)
        
        sample = list()
        counter = 0
        for train_index, test_index in folding.split(data):
            counter = counter + 1
            print("Performing training iteration:", counter)
            
            # print(data[test_index]); print(data[train_index])
            
            X = np.array(data[train_index])
            y = np.array(data[test_index])
            
            model = KBinsDiscretizer(n_bins=bins, encode='ordinal')
            model.fit(X)
            
            y_hat = model.transform(y).ravel()
            print("predicted values:", y_hat)

            predicted_y_hat = (calculate_prediction(model, i) for i in y_hat)
            result = compare(y.ravel(), predicted_y_hat)
            sample.append(result)
            
        return mean(sample) # ik denk dat het voldoende is om gewoon het gemiddelde te nemen
    def execute(self):
        corpus = basecommands.call_corpus(self.config.get('corpus'))
        mygenes = genes.genebase.collect_genes(self.config.get('genes'))
        
        newdata = list(chain(*map(itemgetter(1), corpus.apply_genes(mygenes, True))))
        
        data = np.array(newdata).reshape(-1, 1)
        results = []

        #TODO Implementeer hier nog andere metrics
        for i in range(self.config.get('lower'), self.config.get('upper')):
            print("Beginning iteration %d" % i)
            value = self.calculate_cross_validation(i, data)
            results.append({
                "bins": i,
                "MSE": value 
            })
        
        myframe = pd.DataFrame.from_records(results)
        myframe.to_csv(self.config.get('output'), sep=';', index=False)


class OptimalizeCluster(basecommands.BaseCommand):
    arg_spec = {
        'corpus': ('epub:corpus', str),
        'lower': (0.90, float),
        'upper': (0.9999, float),
        'datapoints': (10, int),
        'fastafile': ('input.fasta', str),
        'attribute': ('author', str),
        'output': ('output.csv', str),
        'plot': ('', str),
        'heuristic': (False, bool)
    }
    def _do_analysis(self, mythreshold, corpus):
        mypipe = CDHITPipe.CDHITPipe(self.config.get('fastafile'), accurate=True, threshold=mythreshold)
        mypipe.execute()

        return list(mypipe.make_lines(corpus))

    def _execute_metric(self, cdict: dict, x: type):
        obj = x(cdict, self.config.get('attribute'))
        return obj.calculate_metric()
    
    def _do_plot(self, frame):
        pass

    def execute(self):
        myspace = np.linspace(self.config.get('lower'), self.config.get('upper'), num=self.config.get('datapoints'))
        mycorpus = basecommands.call_corpus(self.config.get('corpus'))

        results = list()

        for i in myspace:
            result = dict()
            result['threshold'] = i
            print("Test uitvoeren met threshold %f" % i)
            analysis = self._do_analysis(i, mycorpus)
            
            print("calculating metrics for %f" % i)
            resulted_metrics = (self._execute_metric(analysis, x) for x in metrics.exported_metrics)
            names = (x.__name__ for x in metrics.exported_metrics)

            for x, y in zip(names, resulted_metrics):
                result[x] = y
            print("Result metircs: %s" % result)
            cluster_amount = len(set(x['clusterid'] for x in analysis))
            result['Clusters'] = cluster_amount

            results.append(result)
        myframe = pd.DataFrame.from_records(results)
        myframe.to_csv(self.config.get('output'), sep=';', index=False)

        if not self.config.get('plot') == '':
            self._do_plot(myframe)

class ShowClusters(basecommands.BaseCommand):
    arg_spec = {
        'input': ('input.nexus', str)
    }

    def execute(self):
        tree = Tree.get(path=self.config.get('input'), schema='nexus', tree_offset=0)
        for i, x in enumerate(tree.levelorder_node_iter()):
            x.label = str(i)
            # pass
        tree.print_plot(show_internal_node_labels=True)


class DistributionType(Enum):
    lemma = 'lemma'
    token = 'token'
    tag = 'tag'

class BookType(Enum):
    simple = 'simple'
    epub = 'epub'

class CreateDistribution(basecommands.BaseCommand):
    arg_spec = {
        'input': ('', str),
        'output': ('out.png', str),
        'distype': (DistributionType.token, DistributionType),
        'btype': (BookType.epub, BookType)
    }

    def create_graph(self, loaded_ebook, output):

        distype = self.config.get('distype')
        if distype == DistributionType.token:
            words = map(itemgetter(0), loaded_ebook.tokens)
        elif distype == DistributionType.tag:
            words = map(itemgetter(1), loaded_ebook.tokens)
        elif distype == DistributionType.lemma:
            words = loaded_ebook.lemma

        words = map(itemgetter(0), loaded_ebook.tokens)
        plt.ion()
        fd = FreqDist(words)
        fd.plot(30, title='Book freqdist plot')
        plt.savefig(output)
        plt.ioff()
        plt.close()
        # plt.show()

    def execute(self):
        invoer = self.config.get('input')
        if ':' in invoer:
            # on va faire l'assomption que output est un dossier
            if not path.isdir(self.config.get('output')):
                mkdir(self.config.get('output'))
            # We gaan er hier van uit dat het om een corpus gaat
            corpus = basecommands.call_corpus(self.config.get('input'))
            for b in corpus.books:
                new_filename = path.basename('.'.join(b.filename.split('.')[:-1])) #Enlever le dot du fichier
                full_new_filename = path.join(self.config.get('output'), new_filename + '.png')
                print('Creating graph for %s with output %s' % (b.filename, full_new_filename))
                self.create_graph(b, full_new_filename)
        else:
            inputfile = self.config.get('input')
            collection_name = os.path.basename(os.path.dirname(inputfile))

            print("loading ebook: %s" % self.config.get('input'))
            btype = self.config.get('btype')
            if btype == BookType.simple:
                loaded_ebook = simpletext.SimpleBook.load_book(collection_name, self.config.get('input'))
            elif btype == BookType.epub:
                loaded_ebook = ebooks.EPUB.load_book(collection_name, self.config.get('input'))
            self.create_graph(loaded_ebook, self.config.get('output'))

class PlotTree(basecommands.BaseCommand):
    arg_spec = {
        'input': ('input.nexus', str),
        'output': ('output.eps', str),
        'title': ('title', str)
    }

    def execute(self):
        plt.ion()
        tree = Phylo.read(self.config.get('input'), 'newick')
        plt.figure(figsize=(8, 6))
        Phylo.draw(tree)
        plt.title(self.config.get('title'))
        plt.savefig(self.config.get('output'), bbox_inches='tight')
        plt.ioff()
        plt.show()

class CreateTree(basecommands.BaseCommand):
    arg_spec = {
        'input': ('input.fasta', str),
        'output': ('output.tre', str),
        'distout': ('matrix.mat', str)
    }

    def execute(self):
        records = list(AlignIO.parse(self.config.get('input'), 'fasta'))[0]
        calculator = DistanceCalculator(model='identity')
        constructor = DistanceTreeConstructor(calculator)

        print("Calculating distance matrix for tree")
        dm = calculator.get_distance(records)
        matrix = np.array(dm, dtype=float)
        njtree = constructor.nj(dm)

        distance_dataframe = pd.DataFrame(data=matrix, columns=dm.names, index=dm.names)
        
        distance_dataframe.sort_index(axis=0, inplace=True)
        distance_dataframe.sort_index(axis=1, inplace=True)

        distance_dataframe.to_csv(self.config.get('distout'))

        # with open(self.config.get('output'), 'w') as f:
        #     f.write(njtree.format('newick'))
        Phylo.write([njtree], self.config.get('output'), 'newick', plain=True)

class PlotDnatree(basecommands.BaseCommand):
    arg_spec = {
        'input': ('input.csv', str),
        'output': ('output.png', str)
    }
    
    def node_yielder(self):
        start = 0
        while True:
            start = start + 1
            yield(str(start))

    def execute(self):
        import graphviz as gv

        dot = gv.Digraph(name='DNA tree plot')
        yielder = self.node_yielder()

        input_file = pd.read_csv(self.config.get('input'), sep=';',names=['DNA', 'authors'])
        dna = input_file['DNA'].to_list()
        X = [list(x) for x in dna]
        y = input_file['authors'].to_list()
        len_dna = len(X[0])
        nodelist = [str(x) for x in range(len_dna)]

        dot.node('start')
        for x in set(y): dot.node(x, shape='box', label='')

        currents = ['start'] * len(X)

        for i, seqs in enumerate(zip(*X)):
            
            done = [False] * 4
            for j, s in enumerate(seqs):
                if not i == (len_dna - 1):
                    if s == 'A' and not done[0]:
                        done[0] = True
                        # newnode = next(yielder)
                        newnode = str(i) + 'A'
                        dot.node(newnode)
                        dot.edge(currents[j], newnode)
                        currents[j] = newnode
                    elif s == 'T' and not done[1]:
                        done[1] = True
                        # newnode = next(yielder)
                        newnode = str(i) + 'T'
                        dot.node(newnode)
                        dot.edge(currents[j], newnode, color='red')
                        currents[j] = newnode
                    elif s == 'C' and not done[2]:
                        done[2] = True
                        # newnode = next(yielder)
                        newnode = str(i) + 'C'
                        dot.node(newnode)
                        dot.edge(currents[j], newnode, color='green')
                        currents[j] = newnode
                    elif s == 'G' and not done[3]:
                        done[3] = True
                        # newnode = next(yielder)
                        newnode = str(i) + 'G'
                        dot.node(newnode)
                        dot.edge(currents[j], newnode, color='blue')
                        currents[j] = newnode
                    else:
                        newnode = str(i) + s
                        currents[j] = newnode
                else:
                    dot.edge(currents[j], y[j])
        with open(self.config.get('output'), 'w') as f:
            print(dot.source, file=f)

class CompareDiscretization(basecommands.BaseCommand):
    arg_spec = {
        'input': ('epub:first_corpus', str),
        'gene': ('Gender', str),
        'tree': ('encoding_trees/bigtree.xml', str),
        'model': ('datadir/gender.model', str),
        'width': ('equal_width.png', str),
        'frequency': ('equal_frequency.png', str)
    }

    def execute(self):

        mycorpus = basecommands.call_corpus(self.config.get('input'))
        mygenes = genes.genebase.collect_genes(self.config.get('gene'))

        features = list(chain(*map(itemgetter(1), mycorpus.apply_genes(mygenes, only_features=True))))
        loader1 = dna.DNATreeLoader(self.config.get('tree'))

        result_freq = [0] * loader1.get_gene_amount(1)
        loader2 = dna.DNATreeLoader(self.config.get('tree'), self.config.get('model'))
        result_width = [0] * loader2.get_gene_amount(1)

        used_bins_width = [loader1.convert(f, retbin=True) for f in features]
        used_bins_freq = [loader2.convert(f, retbin=True) for f in features]

        for x in used_bins_width: result_width[x] = result_width[x] + 1
        for y in used_bins_freq: result_freq[y] = result_freq[y] + 1

        plt.figure(1)
        plt.hist(used_bins_width, bins=101)
        plt.title("Equal width discretization")
        plt.figure(2)
        plt.hist(used_bins_freq, bins=101)
        plt.title("Equal frequency discretization")

        plt.show()
        # sns.distplot(used_bins_freq)


class AnalyseCollection(basecommands.BaseCommand):
    arg_spec = {
        "input": ("input", str)
    }

    def execute(self):
        inputdir = self.config.get('input')
        if not os.path.exists(inputdir):
            print("input dir %s does not exist" % inputdir)
            exit(1)
        paths = (os.path.join(inputdir, x) for x in os.listdir(inputdir))

        for x in paths:
            print("processing corpus %s" % x)
            cname = os.path.basename(x)
            final_result = os.path.join('results', os.path.basename(inputdir), cname)
            analysis = CompleteAnalysis([
                "--input", 'epub:' + x,
                "--output", final_result,
                "--genes", "TfIdf",
                "--name", "Parametric " + cname,
                "--tcount", "10"
            ])
            analysis.execute()

class CompleteAnalysis(basecommands.BaseCommand):
    arg_spec = {
        'input': ("input_files", str),
        'output': ('output_files', str),
        'genes': ('Gender', str),
        'name': ('Noname', str),
        'tcount': (10, int),
        'summary': (False, bool)
    }
    def execute(self):
        possible_genes = self.config.get('genes').split(',')

        output_files = self.config.get('output')
        if not os.path.isdir(output_files):
            os.makedirs(output_files)
        
        if self.config.get('summary'):
            for gene in possible_genes:
                print("creating summary for: %s" % gene)
                DnaSummary([
                    "--corpus", self.config.get('input'),
                    "--output", os.path.join(output_files, 'summary_' + gene),
                    "--gene", gene,
                    "--distout", os.path.join(output_files, "distmat_" + gene + '.csv')
                ]).execute()

        # PlotDispersion([
        #     "--corpus", self.config.get("input"),
        #     "--output", os.path.join(output_files, 'dispersion_plots')
        # ]).execute()

        # Toevoegen der commando's
        biocommands.CreateDna([
            '--corpus', self.config.get('input'),
            '--output', os.path.join(output_files, 'fastafile.fasta'),
            '--genes', self.config.get('genes'),
            '--mutate',
            '--format', 'fasta',
            '--shuffle'
        ]).execute()

        basecommands.FullCompress([
            "--input", os.path.join(output_files, 'fastafile.fasta'),
            "--output", os.path.join(output_files, 'fastafile_compr.fasta'),
            "--tcount", str(self.config.get('tcount'))
        ]).execute()

        CreateTree([
            '--input', os.path.join(output_files, 'fastafile_compr.fasta'),
            '--output', os.path.join(output_files, 'phylo.tre'),
            "--distout", os.path.join(output_files, 'distance_matrix.csv')
        ]).execute()

        PlotTree([
            "--input", "phylo.tre",
            "--output", "phylo.png",
            "--title", self.config.get('name')
        ])

        # biocommands.SortOnTree([
        #     "--input", os.path.join(output_files, "fastafile_compr.fasta"),
        #     "--output", os.path.join(output_files, "fastafile_sorted.fasta"),
        #     "--treefile", os.path.join(output_files, "phylo.tre")
        # ]).execute()
        
        OptimalizeCluster([
            '--corpus', self.config.get('input'),
            '--lower', '0.6',
            '--upper', '0.7',
            '--datapoints', '100',
            '--fastafile', os.path.join(output_files, 'fastafile_compr.fasta'),
            '--output', os.path.join(output_files, 'optim.csv')
        ]).execute()

        optim_frame = pd.read_csv(os.path.join(output_files, 'optim.csv'), sep=";")
        my_optimal = general.find_optimal(optim_frame)
        my_optimal_threshold = my_optimal.threshold

        print("Optimal threshold found %.2f" % my_optimal_threshold)

        CreateConfusionMatrix([
            '--corpus', self.config.get('input'),
            '--fastafile', os.path.join(output_files, 'fastafile_compr.fasta'),
            '--threshold', str(my_optimal_threshold),
            '--output', os.path.join(output_files, 'confusion_matrix.png')
        ]).execute()

        CreateConfusionMatrix([
            '--corpus', self.config.get('input'),
            '--fastafile', os.path.join(output_files, 'fastafile_compr.fasta'),
            '--threshold', str(my_optimal_threshold),
            '--output', os.path.join(output_files, 'confusion_matrix_omega.png'),
            '--omega',
            '--size', str(self.config.get('tcount'))
        ]).execute()
        biocommands.GenerateClusterTable([
            '--input', os.path.join(output_files, 'fastafile_compr.fasta'),
            '--output', os.path.join(output_files, 'clusters_cdhit.csv'),
            '--corpus', self.config.get('input'),
            '--threshold', str(my_optimal_threshold)
        ]).execute()
        biocommands.GenerateClusterTable([
            '--input', os.path.join(output_files, 'fastafile_compr.fasta'),
            '--output', os.path.join(output_files, 'clusters_omega.csv'),
            '--corpus', self.config.get('input'),
            '--threshold', str(my_optimal_threshold),
            '--omega',
            '--size', str(self.config.get('tcount'))
        ]).execute()