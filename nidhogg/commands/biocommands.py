import sys
import pandas as pd
import numpy as np
from itertools import chain, cycle
from operator import itemgetter
from joblib import load, dump
from random import shuffle
from copy import deepcopy
from os.path import dirname
from os.path import join as pathjoin

from . import basecommands
from ..utils import ebooks, tree, dna
from .. import genes

import inspect
from enum import Enum
from ..utils.CDHITPipe import CDHITPipe
from ..utils.OmegaPipe import OmegaPipe

from sklearn.preprocessing import KBinsDiscretizer
#Biopython modules
from Bio import Phylo
from Bio import Seq
from Bio import SeqIO
from Bio import SeqRecord
from Bio import Align
from Bio.Alphabet import DNAAlphabet
from Bio.Align.Applications import TCoffeeCommandline

from dendropy import Tree as DendoTree
from dendropy import Node as DendroNode

class DNAFileType(Enum):
    NEXUS = 'nexus'
    FASTA = 'fasta'

class AlignDna(basecommands.BaseCommand):
    arg_spec = {
        'input': ('input.fasta', str),
        'output': ('output.aln', str)
    }

    def execute(self):
        mycmd = TCoffeeCommandline(infile=self.config.get('input'), output='clustalw', outfile=self.config.get('output'))
        print('executing command: %s' % mycmd)
        mycmd()

class GenerateClusterTable(basecommands.BaseCommand):
    arg_spec = {
        'input': ('input.fasta', str),
        'output': ('output.csv', str),
        'corpus': ('epub:fantasy_corpus', str),
        'threshold': (0.9, float),
        'omega': (False, bool),
        'size': (10, int)
    }

    def execute(self):
        distmat_output_file = pathjoin(dirname(self.config.get('output')), 'clustal_distmat.mat')
        if self.config.get('omega'):
            mypipe = OmegaPipe(self.config.get('input'), debug=True, cluster_size=self.config.get('size'))
        else:
            mypipe = CDHITPipe(self.config.get('input'), accurate=True, threshold=self.config.get('threshold'))
        my_corpus = basecommands.call_corpus(self.config.get('corpus'))
        mypipe.execute(corpus=my_corpus)
        
        frame = mypipe.to_dataframe()
        frame.to_csv(self.config.get('output'), sep='|', index=False)

class TrainSklearn(basecommands.BaseCommand):
    arg_spec = {
        'bins': (101, int),
        'output': ('datadir/disc.model', str), # hier misschien path join van maken
        'corpus': ('epub:fantasy_corpus', str),
        'kmeans': (False, bool),
        'genes': ('Gender', str)
    }

    def extract_features(self, corpus, genes):
        #TODO hier een aanpassing gedaan
        retval = list(map(itemgetter(1), corpus.apply_genes(genes,only_features=True)))
        # retval = list(chain(*map(itemgetter(1), corpus.apply_genes(genes,only_features=True))))
        return retval

    def execute(self):
        corpus = basecommands.call_corpus(self.config.get('corpus'))
        mygenes = genes.genebase.collect_genes(self.config.get('genes'))

        if not self.config.get('kmeans'):
            model = KBinsDiscretizer(self.config.get('bins'), encode='ordinal', strategy='quantile')
        else:
            model = KBinsDiscretizer(self.config.get('bins'), strategy='kmeans', encode='ordinal')
        #TODO hier een aanpassing gedaan
        features = np.array(self.extract_features(corpus, mygenes))
        # features = np.array(self.extract_features(corpus, mygenes)).reshape(-1,1)
        
        #TODO hier een aanpassing gedaan
        model.fit(features.reshape(-1,1))
        # model.fit(features)
        dump(model, self.config.get('output'))


class ClassifyFasta(basecommands.BaseCommand):
    arg_spec = {
        'fastacluster': ('input.fasta', str),
        'fastafile': ('books.fasta', str),
        'clusterfile': ('clusters.csv', str),
        'output': ('output.csv', str)
    }

    def calculate_distance(self, seq1: SeqRecord.SeqRecord, seq2: SeqRecord.SeqRecord):
        # Ervan uitgaande dat de eerste de identiteit is

        print("Comparing alignment %s and %s" % (seq1.name, seq2.name))

        result = self.aligner.score(str(seq1.seq), str(seq2.seq))
        return result

    def execute(self):
        self.aligner = Align.PairwiseAligner()

        new_results = []

        cluster_sequences = SeqIO.read(self.config.get('fastacluster'), 'fasta')
        corpus_sequences = SeqIO.read(self.config.get('fastafile'), 'fasta')

        cluster_table = pd.read_csv(self.config.get('clusterfile'), sep='|')
        identities = cluster_table[cluster_table["percent"] == 100]

        for corpus_sequence in corpus_sequences:
            current_max = None
            for iden in identities["name"]:
                sequence_cluster = next(filter(lambda x: x.name == iden, cluster_sequences))
                result = self.calculate_distance(corpus_sequence, sequence_cluster)
                if current_max is None:
                    current_max = (sequence_cluster.name, result)
                elif current_max[1] < result:
                    current_max = (sequence_cluster.name, result)
            new_results.append(current_max)
        result_frame = pd.DataFrame.from_records(new_results, columns=["Cluster", "Score"])
        result_frame.to_csv(self.config.get('output'), sep='|', index=False)

class SortOnTree(basecommands.BaseCommand):
    arg_spec = {
        'input': ('input.fasta', str),
        'output': ('output.fasta', str),
        'treefile': ('treefile.fasta', str)
    }

    def execute(self):
        results = []

        records = list(SeqIO.parse(self.config.get('input'), format='fasta'))
        mytree = DendoTree.get(path=self.config.get('treefile'), schema='newick')
        node_iterator = mytree.preorder_node_iter(lambda node: node.is_leaf())

        for n in node_iterator:
            nid = n.taxon.label
            record = next(filter(lambda x: x.id == nid, records))
            print('Sorting: %s' % record.id)
            results.append(record)
        SeqIO.write(results, self.config.get('output'), format='fasta')


class SortDna(basecommands.BaseCommand):
    arg_spec = {
        'input': ('input.fasta', str),
        'output': ('output.fasta', str)
    }

    def records_to_matrix(self, records) -> pd.DataFrame:
        counter = 0

        reclen = len(records)
        recrange = list(range(len(records)))

        result = np.zeros((reclen, reclen))
        
        for i in recrange:
            percentage = (counter/(reclen**2)*100)
            print("Creating similarity matrix: %d%%" % percentage, end='\r')
            for j in recrange:
                rec1 = records[i]
                rec2 = records[j]

                result[i][j] = self.hamming(rec1.seq, rec2.seq)
                counter = counter + 1
        print("\ndone!")
        result = np.tril(result)
        return pd.DataFrame(data=result, index=list(range(len(records))), columns=list(range(len(records))))

    def hamming(self, first, second):
        return sum(map(int, map(lambda x, y: x != y, first, second)))

    def iter_matrix(self, matrix: pd.DataFrame):
        x, y = matrix.shape
        for i in range(x):
            for j in range(y):
                yield (i, j)

    def solve(self, frame: pd.DataFrame):

        print("Begin frame:\n%s" % frame)

        matrix_frame = deepcopy(frame)
        result = []
        max_item = max([((i,j), matrix_frame[i][j]) for i,j in self.iter_matrix(matrix_frame)], key=itemgetter(1))

        result.append(max_item[0][0])
        result.append(max_item[0][1])

        print("Removing %d" % result[0])

        matrix_frame.drop(result[0], axis=0, inplace=True)
        matrix_frame.drop(result[0], axis=1, inplace=True)
        print("frame:\n%s" % matrix_frame)
        
        current = result[1]

        row = False

        for _ in range(frame.shape[0]-2):
            if row:
                newarr = matrix_frame.loc[:,current]
            else:
                newarr = matrix_frame.loc[current,:]
            newcurr = newarr.idxmax()

            print("Removing %d" % current)

            matrix_frame.drop(current, axis=0, inplace=True)
            matrix_frame.drop(current, axis=1, inplace=True)

            print("frame:\n%s" % matrix_frame)
            
            result.append(newcurr)
            current = newcurr

            row = (not row)
        return result

    def execute(self):
        records = list(SeqIO.parse(self.config.get('input'), 'fasta'))
        matrec = self.records_to_matrix(records)
        my_res = self.solve(matrec)

        new_dna = [records[i] for i in my_res]
        SeqIO.write(new_dna, self.config.get('output'), 'fasta')

class CreateDna(basecommands.BaseCommand):
    arg_spec = {
        'genes': ('', str),
        'corpus': ('corpus', str),
        'output': ('output.nexus', str),
        'mutate': (False, bool),
        'mutationtree': (None, str),
        'format': (DNAFileType.NEXUS, DNAFileType),
        'shuffle': (False, bool)
    }

    def _write_nexus(self):
        my_nexus = tree.NexusFile(self.config.get('output'), self.dna_matrix)
        my_nexus.write_file()
    def _write_fasta(self):
        sequence_list = list()

        for bname, dna in self.dna_matrix:
            current_book = self.my_corpus.get_book_by_name(bname)
            b_id, b_title, b_author, b_publisher = current_book.meta_tuple()

            current_seq = Seq.Seq(dna, DNAAlphabet.letters)
            current_record = SeqRecord.SeqRecord(current_seq, id=b_id, name=b_title,
                description="{0} by {1} from {2}".format(b_title, b_author, b_publisher))

            #TODO enkel unieke ids
            if not b_id in (x.id for x in sequence_list):
                sequence_list.append(current_record)
        for x in sequence_list:
            print('created record %s' % x)
        
        SeqIO.write(sequence_list, self.config.get('output'), 'fasta')

    def _perform_mutation(self):
        result = []
        # mapping = mytree.create_mutation_mapping()
        for book, d in self.dna_matrix:
            newd = []
            print("Mutating: %s" % book)
            split_len = int(len(d) / 2)
            split_x = d[:split_len]; split_y = d[split_len:]

            for x, y in zip(list(split_x), list(split_y)):
                # trueval = list(set(['C', 'T', 'G']).difference(set(m)))[0]
                if x == 'C' and y == 'C':
                    newd.append('C')
                elif x == 'A' and y == 'A':
                    newd.append('A')
                elif x == 'C' and y == 'A':
                    newd.append('T')
                elif x == 'A' and y == 'C':
                    newd.append('G')
                else:
                    raise Exception("Dit kan niet!")
            result.append((book, ''.join(newd)))
        return result
    def execute(self):
        self.collected_genes = genes.genebase.collect_genes(self.config.get('genes'))
        # HAHAHA now the magic will happen!!!
        #function_words = utils.wordlist.WordList(os.path.join('datadir', 'function_words'))

        self.my_corpus = basecommands.call_corpus(self.config.get('corpus'))
        self.dna_matrix = self.my_corpus.apply_genes(self.collected_genes)

        if self.config.get('mutate'):
            self.dna_matrix = self._perform_mutation()

        if self.config.get('shuffle'):
            shuffle(self.dna_matrix)

        output_mode = self.config.get('format')
        if output_mode == DNAFileType.NEXUS:
            self._write_nexus()
        elif output_mode == DNAFileType.FASTA:
            self._write_fasta()
        else:
            print('Uknown file format in dna generator')
            sys.exit(1)
