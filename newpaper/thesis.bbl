\begin{thebibliography}{}

\bibitem [\protect \citeauthoryear {%
Aha%
, Kibler%
\BCBL {}\ \BBA {} Albert%
}{%
Aha%
\ \protect \BOthers {.}}{%
{\protect \APACyear {1991}}%
}]{%
instbase}
\APACinsertmetastar {%
instbase}%
\begin{APACrefauthors}%
Aha, D\BPBI W.%
, Kibler, D.%
\BCBL {}\ \BBA {} Albert, M\BPBI K.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{1991}{Jan}{01}.
\newblock
{\BBOQ}\APACrefatitle {Instance-based learning algorithms} {Instance-based
  learning algorithms}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Machine Learning}{6}{1}{37--66}.
\newblock
\begin{APACrefURL} \url{https://doi.org/10.1007/BF00153759} \end{APACrefURL}
\newblock
\begin{APACrefDOI} \doi{10.1007/BF00153759} \end{APACrefDOI}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Amigo%
, Gonzalo%
, Artiles%
\BCBL {}\ \BBA {} Verdejo%
}{%
Amigo%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2009}}%
}]{%
Amigo2009}
\APACinsertmetastar {%
Amigo2009}%
\begin{APACrefauthors}%
Amigo, E.%
, Gonzalo, J.%
, Artiles, J.%
\BCBL {}\ \BBA {} Verdejo, F.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2009}{Aug}{01}.
\newblock
{\BBOQ}\APACrefatitle {A comparison of extrinsic clustering evaluation metrics
  based on formal constraints} {A comparison of extrinsic clustering evaluation
  metrics based on formal constraints}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Information Retrieval}{12}{4}{461--486}.
\newblock
\begin{APACrefURL} \url{https://doi.org/10.1007/s10791-008-9066-8}
  \end{APACrefURL}
\newblock
\begin{APACrefDOI} \doi{10.1007/s10791-008-9066-8} \end{APACrefDOI}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Androutsopoulos%
\ \protect \BOthers {.}}{%
Androutsopoulos%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2000}}%
}]{%
spam}
\APACinsertmetastar {%
spam}%
\begin{APACrefauthors}%
Androutsopoulos, I.%
, Paliouras, G.%
, Karkaletsis, V.%
, Sakkis, G.%
, Spyropoulos, C\BPBI D.%
\BCBL {}\ \BBA {} Stamatopoulos, P.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2000}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Learning to filter spam e-mail: A comparison of a naive
  bayesian and a memory-based approach} {Learning to filter spam e-mail: A
  comparison of a naive bayesian and a memory-based approach}.{\BBCQ}
\newblock
\APACjournalVolNumPages{arXiv preprint cs/0009009}{}{}{}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Bell%
, Brenier%
, Gregory%
, Girand%
\BCBL {}\ \BBA {} Jurafsky%
}{%
Bell%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2009}}%
}]{%
function_words_extra}
\APACinsertmetastar {%
function_words_extra}%
\begin{APACrefauthors}%
Bell, A.%
, Brenier, J\BPBI M.%
, Gregory, M.%
, Girand, C.%
\BCBL {}\ \BBA {} Jurafsky, D.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2009}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Predictability effects on durations of content and
  function words in conversational English} {Predictability effects on
  durations of content and function words in conversational english}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Journal of Memory and Language}{60}{1}{92 - 111}.
\newblock
\begin{APACrefURL}
  \url{http://www.sciencedirect.com/science/article/pii/S0749596X08000600}
  \end{APACrefURL}
\newblock
\begin{APACrefDOI} \doi{https://doi.org/10.1016/j.jml.2008.06.003}
  \end{APACrefDOI}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Bird%
, Klein%
\BCBL {}\ \BBA {} Loper%
}{%
Bird%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2009}}%
}]{%
nltk}
\APACinsertmetastar {%
nltk}%
\begin{APACrefauthors}%
Bird, S.%
, Klein, E.%
\BCBL {}\ \BBA {} Loper, E.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYear{2009}.
\newblock
\APACrefbtitle {Natural language processing with Python: analyzing text with
  the natural language toolkit} {Natural language processing with python:
  analyzing text with the natural language toolkit}.
\newblock
\APACaddressPublisher{}{O Reilly Media Inc}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Castelluccio%
}{%
Castelluccio%
}{%
{\protect \APACyear {2006}}%
}]{%
musicgenome}
\APACinsertmetastar {%
musicgenome}%
\begin{APACrefauthors}%
Castelluccio, M.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2006}{}{}.
\newblock
{\BBOQ}\APACrefatitle {The music genome project} {The music genome
  project}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Strategic Finance}{}{}{57--59}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Chung%
\ \BBA {} Pennebaker%
}{%
Chung%
\ \BBA {} Pennebaker%
}{%
{\protect \APACyear {2007}}%
}]{%
function_words}
\APACinsertmetastar {%
function_words}%
\begin{APACrefauthors}%
Chung, C.%
\BCBT {}\ \BBA {} Pennebaker, J\BPBI W.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2007}{}{}.
\newblock
{\BBOQ}\APACrefatitle {The psychological functions of function words} {The
  psychological functions of function words}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Social communication}{1}{}{343--359}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Covington%
\ \BBA {} McFall%
}{%
Covington%
\ \BBA {} McFall%
}{%
{\protect \APACyear {2010}}%
}]{%
better_richness}
\APACinsertmetastar {%
better_richness}%
\begin{APACrefauthors}%
Covington, M\BPBI A.%
\BCBT {}\ \BBA {} McFall, J\BPBI D.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2010}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Cutting the Gordian Knot: The Moving-Average Type-Token
  Ratio (MATTR)} {Cutting the gordian knot: The moving-average type-token ratio
  (mattr)}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Journal of Quantitative Linguistics}{17}{}{94-100}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Daelemans%
, Zavrel%
, Van Den~Bosch%
\BCBL {}\ \BBA {} Van~der Sloot%
}{%
Daelemans%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2003}}%
}]{%
mbt}
\APACinsertmetastar {%
mbt}%
\begin{APACrefauthors}%
Daelemans, W.%
, Zavrel, J.%
, Van Den~Bosch, A.%
\BCBL {}\ \BBA {} Van~der Sloot, K.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2003}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Mbt: Memory-based tagger} {Mbt: Memory-based
  tagger}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Reference Guide: ILK Technical Report-ILK}{}{}{03--13}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Daelemans%
, Zavrel%
, Van Der~Sloot%
\BCBL {}\ \BBA {} Van~den Bosch%
}{%
Daelemans%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2004}}%
}]{%
daelemans2004timbl}
\APACinsertmetastar {%
daelemans2004timbl}%
\begin{APACrefauthors}%
Daelemans, W.%
, Zavrel, J.%
, Van Der~Sloot, K.%
\BCBL {}\ \BBA {} Van~den Bosch, A.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2004}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Timbl: Tilburg memory-based learner} {Timbl: Tilburg
  memory-based learner}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Tilburg University}{}{}{}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Fitch%
}{%
Fitch%
}{%
{\protect \APACyear {1971}}%
}]{%
parsimony}
\APACinsertmetastar {%
parsimony}%
\begin{APACrefauthors}%
Fitch, W\BPBI M.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{1971}{12}{}.
\newblock
{\BBOQ}\APACrefatitle {{Toward Defining the Course of Evolution: Minimum Change
  for a Specific Tree Topology}} {{Toward Defining the Course of Evolution:
  Minimum Change for a Specific Tree Topology}}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Systematic Biology}{20}{4}{406-416}.
\newblock
\begin{APACrefDOI} \doi{10.1093/sysbio/20.4.406} \end{APACrefDOI}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Gascuel%
\ \BBA {} Steel%
}{%
Gascuel%
\ \BBA {} Steel%
}{%
{\protect \APACyear {2006}}%
}]{%
joining_review}
\APACinsertmetastar {%
joining_review}%
\begin{APACrefauthors}%
Gascuel, O.%
\BCBT {}\ \BBA {} Steel, M.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2006}{07}{}.
\newblock
{\BBOQ}\APACrefatitle {{Neighbor-Joining Revealed}} {{Neighbor-Joining
  Revealed}}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Molecular Biology and Evolution}{23}{11}{1997-2000}.
\newblock
\begin{APACrefURL} \url{https://doi.org/10.1093/molbev/msl072} \end{APACrefURL}
\newblock
\begin{APACrefDOI} \doi{10.1093/molbev/msl072} \end{APACrefDOI}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Gray%
\ \BBA {} Atkinson%
}{%
Gray%
\ \BBA {} Atkinson%
}{%
{\protect \APACyear {2003}}%
}]{%
gray}
\APACinsertmetastar {%
gray}%
\begin{APACrefauthors}%
Gray, R\BPBI D.%
\BCBT {}\ \BBA {} Atkinson, Q\BPBI D.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2003}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Language-tree divergence times support the Anatolian
  theory of Indo-European origin} {Language-tree divergence times support the
  anatolian theory of indo-european origin}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Nature}{426}{6965}{435--439}.
\newblock
\begin{APACrefURL} \url{https://doi.org/10.1038/nature02029} \end{APACrefURL}
\newblock
\begin{APACrefDOI} \doi{10.1038/nature02029} \end{APACrefDOI}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Gysseling%
}{%
Gysseling%
}{%
{\protect \APACyear {1980}}%
}]{%
gysseling}
\APACinsertmetastar {%
gysseling}%
\begin{APACrefauthors}%
Gysseling, M.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYear{1980}.
\newblock
\APACrefbtitle {Corpus van middelnederlandse teksten: tot en met het jaar 1300.
  Literaire handschriften. Fragmenten} {Corpus van middelnederlandse teksten:
  tot en met het jaar 1300. literaire handschriften. fragmenten}\ (\BVOL~2).
\newblock
\APACaddressPublisher{}{Nijhoff}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
HOLMES%
}{%
HOLMES%
}{%
{\protect \APACyear {1998}}%
}]{%
holmes}
\APACinsertmetastar {%
holmes}%
\begin{APACrefauthors}%
HOLMES, D\BPBI I.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{1998}{09}{}.
\newblock
{\BBOQ}\APACrefatitle {{The Evolution of Stylometry in Humanities Scholarship}}
  {{The Evolution of Stylometry in Humanities Scholarship}}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Digital Scholarship in the Humanities}{13}{3}{111-117}.
\newblock
\begin{APACrefURL} \url{https://doi.org/10.1093/llc/13.3.111} \end{APACrefURL}
\newblock
\begin{APACrefDOI} \doi{10.1093/llc/13.3.111} \end{APACrefDOI}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Jurafsky%
\ \BBA {} Martin%
}{%
Jurafsky%
\ \BBA {} Martin%
}{%
{\protect \APACyear {2014}}%
}]{%
jurafsky}
\APACinsertmetastar {%
jurafsky}%
\begin{APACrefauthors}%
Jurafsky, D.%
\BCBT {}\ \BBA {} Martin, J\BPBI H.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYear{2014}.
\newblock
\APACrefbtitle {Speech and language processing} {Speech and language
  processing}\ (\BVOL~3).
\newblock
\APACaddressPublisher{}{Pearson London}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Kestemont%
, Daelemans%
\BCBL {}\ \BBA {} De~Pauw%
}{%
Kestemont%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2010}}%
}]{%
weigh}
\APACinsertmetastar {%
weigh}%
\begin{APACrefauthors}%
Kestemont, M.%
, Daelemans, W.%
\BCBL {}\ \BBA {} De~Pauw, G.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2010}{08}{}.
\newblock
{\BBOQ}\APACrefatitle {{Weigh your words—memory-based lemmatization for
  Middle Dutch}} {{Weigh your words—memory-based lemmatization for Middle
  Dutch}}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Digital Scholarship in the Humanities}{25}{3}{287-301}.
\newblock
\begin{APACrefURL} \url{https://doi.org/10.1093/llc/fqq011} \end{APACrefURL}
\newblock
\begin{APACrefDOI} \doi{10.1093/llc/fqq011} \end{APACrefDOI}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Kestemont%
\ \BBA {} van Dalen-Oskam%
}{%
Kestemont%
\ \BBA {} van Dalen-Oskam%
}{%
{\protect \APACyear {2009}}%
}]{%
walewein}
\APACinsertmetastar {%
walewein}%
\begin{APACrefauthors}%
Kestemont, M.%
\BCBT {}\ \BBA {} van Dalen-Oskam, K.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2009}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Predicting the past: memory-based copyist and author
  discrimination in medieval epics} {Predicting the past: memory-based copyist
  and author discrimination in medieval epics}.{\BBCQ}
\newblock
\BIn{} \APACrefbtitle {Proceedings of the Twenty-first Benelux Conference on
  Artificial Intelligence (BNAIC 2009)} {Proceedings of the twenty-first
  benelux conference on artificial intelligence (bnaic 2009)}\ (\BPGS\
  121--128).
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Koppel%
, Argamon%
\BCBL {}\ \BBA {} Shimoni%
}{%
Koppel%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2002}}%
}]{%
gender}
\APACinsertmetastar {%
gender}%
\begin{APACrefauthors}%
Koppel, M.%
, Argamon, S.%
\BCBL {}\ \BBA {} Shimoni, A\BPBI R.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2002}{11}{}.
\newblock
{\BBOQ}\APACrefatitle {{Automatically Categorizing Written Texts by Author
  Gender}} {{Automatically Categorizing Written Texts by Author
  Gender}}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Digital Scholarship in the Humanities}{17}{4}{401-412}.
\newblock
\begin{APACrefURL} \url{https://doi.org/10.1093/llc/17.4.401} \end{APACrefURL}
\newblock
\begin{APACrefDOI} \doi{10.1093/llc/17.4.401} \end{APACrefDOI}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Lutoslawski%
}{%
Lutoslawski%
}{%
{\protect \APACyear {1898}}%
}]{%
platoart}
\APACinsertmetastar {%
platoart}%
\begin{APACrefauthors}%
Lutoslawski, W.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{1898}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Principes de stylométrie appliqués à la chronologie
  des œuvres de Platon} {Principes de stylométrie appliqués à la
  chronologie des œuvres de platon}.{\BBCQ}
\newblock

\newblock
\begin{APACrefDOI} \doi{10.3406/reg.1898.5847} \end{APACrefDOI}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Manning%
, Raghevan%
\BCBL {}\ \BBA {} Schütze%
}{%
Manning%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2008}}%
}]{%
retrieval}
\APACinsertmetastar {%
retrieval}%
\begin{APACrefauthors}%
Manning, C\BPBI D.%
, Raghevan, P.%
\BCBL {}\ \BBA {} Schütze, H.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYear{2008}.
\newblock
\APACrefbtitle {Introduction to Information Retrieval} {Introduction to
  information retrieval}.
\newblock
\APACaddressPublisher{}{Cambridge University Press}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Màrquez%
\ \BBA {} Rodríguez%
}{%
Màrquez%
\ \BBA {} Rodríguez%
}{%
{\protect \APACyear {2006}}%
}]{%
postag}
\APACinsertmetastar {%
postag}%
\begin{APACrefauthors}%
Màrquez, L.%
\BCBT {}\ \BBA {} Rodríguez, H.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2006}{04}{}.
\newblock
{\BBOQ}\APACrefatitle {Part-of-speech tagging using decision trees}
  {Part-of-speech tagging using decision trees}.{\BBCQ}
\newblock
\BIn{} (\BVOL\ 1398, \BPG~25-36).
\newblock
\begin{APACrefDOI} \doi{10.1007/BFb0026668} \end{APACrefDOI}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Saitou%
\ \BBA {} Nei%
}{%
Saitou%
\ \BBA {} Nei%
}{%
{\protect \APACyear {1987}}%
}]{%
joining}
\APACinsertmetastar {%
joining}%
\begin{APACrefauthors}%
Saitou, N.%
\BCBT {}\ \BBA {} Nei, M.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{1987}{07}{}.
\newblock
{\BBOQ}\APACrefatitle {{The neighbor-joining method: a new method for
  reconstructing phylogenetic trees.}} {{The neighbor-joining method: a new
  method for reconstructing phylogenetic trees.}}{\BBCQ}
\newblock
\APACjournalVolNumPages{Molecular Biology and Evolution}{4}{4}{406-425}.
\newblock
\begin{APACrefURL} \url{https://doi.org/10.1093/oxfordjournals.molbev.a040454}
  \end{APACrefURL}
\newblock
\begin{APACrefDOI} \doi{10.1093/oxfordjournals.molbev.a040454} \end{APACrefDOI}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Sipser%
}{%
Sipser%
}{%
{\protect \APACyear {2012}}%
}]{%
intro_computation}
\APACinsertmetastar {%
intro_computation}%
\begin{APACrefauthors}%
Sipser, M.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYear{2012}.
\newblock
\APACrefbtitle {Introduction to the Theory of Computation} {Introduction to the
  theory of computation}.
\newblock
\APACaddressPublisher{}{Cengage Learning}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Stamatatos%
}{%
Stamatatos%
}{%
{\protect \APACyear {2009}}%
}]{%
survey}
\APACinsertmetastar {%
survey}%
\begin{APACrefauthors}%
Stamatatos, E.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2009}{}{}.
\newblock
{\BBOQ}\APACrefatitle {A survey of modern authorship attribution methods} {A
  survey of modern authorship attribution methods}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Journal of the American Society for Information Science
  and Technology}{60}{3}{538-556}.
\newblock
\begin{APACrefURL}
  \url{https://onlinelibrary.wiley.com/doi/abs/10.1002/asi.21001}
  \end{APACrefURL}
\newblock
\begin{APACrefDOI} \doi{10.1002/asi.21001} \end{APACrefDOI}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Swofford%
}{%
Swofford%
}{%
{\protect \APACyear {2003}}%
}]{%
paup}
\APACinsertmetastar {%
paup}%
\begin{APACrefauthors}%
Swofford, D\BPBI L.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2003}{}{}.
\newblock
{\BBOQ}\APACrefatitle {PAUP*: phylogenetic analysis using parsimony, version
  4.0 b10} {Paup*: phylogenetic analysis using parsimony, version 4.0
  b10}.{\BBCQ}
\newblock

\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
van Dalen-Oskam%
\ \BBA {} van Zundert%
}{%
van Dalen-Oskam%
\ \BBA {} van Zundert%
}{%
{\protect \APACyear {2007}}%
}]{%
waldelt}
\APACinsertmetastar {%
waldelt}%
\begin{APACrefauthors}%
van Dalen-Oskam, K.%
\BCBT {}\ \BBA {} van Zundert, J.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2007}{06}{}.
\newblock
{\BBOQ}\APACrefatitle {{Delta for Middle Dutch—Author and Copyist Distinction
  in Walewein}} {{Delta for Middle Dutch—Author and Copyist Distinction in
  Walewein}}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Digital Scholarship in the Humanities}{22}{3}{345-362}.
\newblock
\begin{APACrefURL} \url{https://doi.org/10.1093/llc/fqm012} \end{APACrefURL}
\newblock
\begin{APACrefDOI} \doi{10.1093/llc/fqm012} \end{APACrefDOI}
\PrintBackRefs{\CurrentBib}

\end{thebibliography}
