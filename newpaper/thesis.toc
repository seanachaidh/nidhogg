\babel@toc {dutch}{}
\contentsline {section}{\numberline {1}Introductie}{2}{section.1}% 
\contentsline {section}{\numberline {2}Stylometrie}{3}{section.2}% 
\contentsline {subsection}{\numberline {2.1}Stylometrische technieken}{4}{subsection.2.1}% 
\contentsline {subsubsection}{\numberline {2.1.1}POS tagging}{4}{subsubsection.2.1.1}% 
\contentsline {subsection}{\numberline {2.2}Middelnederlands}{5}{subsection.2.2}% 
\contentsline {subsubsection}{\numberline {2.2.1}Tagging}{6}{subsubsection.2.2.1}% 
\contentsline {subsubsection}{\numberline {2.2.2}Lemmatisatie}{6}{subsubsection.2.2.2}% 
\contentsline {section}{\numberline {3}Omvormen tot DNA}{7}{section.3}% 
\contentsline {subsection}{\numberline {3.1}Functiewoorden}{9}{subsection.3.1}% 
\contentsline {subsubsection}{\numberline {3.1.1}Varieteit in woordenschat}{10}{subsubsection.3.1.1}% 
\contentsline {subsection}{\numberline {3.2}Het maken van phylogenetische bomen uit DNA}{11}{subsection.3.2}% 
\contentsline {subsection}{\numberline {3.3}Metrics}{13}{subsection.3.3}% 
\contentsline {subsubsection}{\numberline {3.3.1}Zuiverheid}{13}{subsubsection.3.3.1}% 
\contentsline {section}{\numberline {4}Resultaten en besluit}{14}{section.4}% 
\contentsline {subsection}{\numberline {4.1}Zonder de training}{14}{subsection.4.1}% 
\contentsline {section}{Literatuur}{17}{section*.10}% 
