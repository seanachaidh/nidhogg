
library('rjson')
library('ape')
library('ggtree')

myargs <- commandArgs(TRUE)
tree <- 'fantasy_dna.nexus'
clusters <- 'clusters.json'
outfile <- 'myfile.png'

myjson <- fromJSON(file = clusters)
mytree <- ape::read.nexus(tree)

# testnode <- getMRCA(mytree, myjson['cluster 2'])
# ggtree(mytree) + geom_cladelabel(node=testnode, label="Cluster")

ggtree(mytree) + geom_tiplab(size=2) +
  geom_cladelabel(getMRCA(mytree, myjson[['cluster 4']]), 'Hello', align = TRUE, offset = .8) +
  theme_tree2() +
  xlim(0, 1000) +
  theme_tree()
