#!/usr/bin/env Rscript

library('rjson')
library('ape')
library('ggtree')

myargs <- commandArgs(TRUE)
tree <- myargs[1]
clusters <- myargs[2]
outfile <- myargs[3]

myjson <- fromJSON(file = clusters)
mytree <- ape::read.nexus(tree)

myplot <- ggtree(mytree) + geom_tiplab()

for(x in myjson) {
  clustername <- names(x)
  ancestor <- getMRCA(mytree, x)
  myplot <- myplot + geom_cladelabel(node=ancestor, label="clustername", align = TRUE, offset = .8) +
      theme_tree2() + xlim(0,300) + theme_tree()
}

ggsave(outfile)
