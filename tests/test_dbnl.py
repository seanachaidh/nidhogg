import unittest, os
from nidhogg.utils.dbnl import DBNLPage

class TestDBNL(unittest.TestCase):
    def setUp(self):
        self.mypages = DBNLPage('https://www.dbnl.org/titels/titels_ebook.php?s=t')
    
    def test_numpages(self):
        self.assertEqual(self.mypages.numpages, 47, msg="Het aantal pagina's bij DBNL klopt niet")
    
    def test_booklist(self):
        self.assertTrue(list(self.mypages.booklist), 'Er kon geen boekenlijst opgehaald worden')

    def test_listlen(self):
        got = len(list(self.mypages.booklist))
        self.assertEqual(got, 100, msg='De lengte van de boekenlijst klopt niet')

    def test_url_fetch(self):
        x = self.mypages.all_urls()

        for i, url in enumerate(x):
            print('Boek {0}: {1}'.format(i, url))

        self.assertTrue(True, 'Gewoon als plaatsvervanger')

    def test_download_all_files(self):
        self.mypages.data_mine('dbnl')
        self.assertTrue(True, 'Gewoon als plaatsvervanger')
    
    def test_bytes(self):
        data = self.mypages.get_needed_data()

        print('needed dat: %f' % (data/1000000))

        self.assertTrue(True)