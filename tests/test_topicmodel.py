from nidhogg.utils import topicmodel
from nidhogg.utils.ebooks import EbookCorpus, EPUB
from unittest import TestCase

from operator import itemgetter

class TestTopicModel(TestCase):
    def setUp(self):
        self.corpus = EbookCorpus('corpus')
    
    def test_model(self):
        mymodel = topicmodel.TopicModel(self.corpus)
        topics = mymodel.predict_topics(self.corpus.books[0])
        print(topics)
        print('Som distributie:', sum(map(itemgetter(1), topics)))
        self.assertEqual(len(topics), 200, msg='Lengte van de onderwerpen klopt niet')