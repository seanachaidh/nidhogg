import unittest, shutil, os
from nidhogg.commands import basecommands
from nidhogg.utils import dna
from nidhogg.commands import biocommands
from nidhogg.commands import plotcommands

class TestBalance(unittest.TestCase):

    def test_simple(self):
        args_balance = [
            '--tree',
            'encoding_trees/bigtree.xml',
            '--output',
            'encoding_trees/xanos.xml',
            '--input',
            'mydna.nexus'
        ]
        mycmd = basecommands.BalanceTree(args_balance)
        mycmd.execute()

        # De eigenlijke test
        xanos_tree = dna.DNATreeLoader(os.path.join('encoding_trees', 'xanos.xml'))
        expected_count = xanos_tree.get_gene_amount(1)
        actual_count = xanos_tree.get_actual_gene_amount(1)

        self.assertEqual(expected_count, actual_count, 'counts van het DNA kloppen niet')
        
    def test_multiple(self):
        args_balance = [
            '--tree',
            'encoding_trees/xanos.xml',
            '--output',
            'encoding_trees/xanos.xml',
            '--corpus',
            'epub:fantasy_corpus'
        ]

        args_dna = [
            '--genes', "Gender",
            '--corpus', "epub:fantasy_corpus",
            "--output", "mydna.nexus"
        ]

        shutil.copyfile('encoding_trees/bigtree.xml', 'encoding_trees/xanos.xml')

        for i in range(5):
            print('test multiple iteration:', i)
            mycmd = basecommands.BalanceTree(args_balance)
            mycmd.execute()
            del mycmd
            
            dnacmd = biocommands.CreateDna(args_dna)
            dnacmd.execute()
            del dnacmd

class TestMetrics(unittest.TestCase):
    def setUp(self):
        pass
    
    def test_metric(self):
        commando_argumenten = [
            '--input', 'mydna.nexus',
            '--output', 'testfile.txt',
            '--corpus', 'corpus'
        ]
        my_command = plotcommands.MetricSummary(commando_argumenten)
        my_command.execute()
