import unittest
from nidhogg.utils import tree

class TestTrees(unittest.TestCase):

    def setUp(self):
        self.childA = tree.TreeElement(data='A')
        childX = tree.TreeElement(data='X', parent=self.childA)
        childB = tree.TreeElement(data='B', parent=childX)
        childC = tree.TreeElement(data='C', parent=childX)
        childY = tree.TreeElement(data='Y', parent=self.childA)
        childD = tree.TreeElement(data='D', parent=childY)
        childE = tree.TreeElement(data='E', parent=childY)

        childY.children = [childD, childE]
        childX.children = [childB, childC]
        self.childA.children = [childX, childY]

    def test_list_tree(self):
        expected = '[A, [X, [B, C], Y, [D, E]]]'
        got = str(self.childA.to_nested_list())
        self.assertEqual(expected, got, msg='Nested list niet gelukt')

    def test_newick(self):
        expected = '(A,(X,(B,C),Y,(D,E)))'
        got = self.childA.to_newick_tree()
        self.assertEqual(got, expected, 'newicktree niet correct')


    def test_list_to_tree(self):
        testtree = ['een', 'twee', 'drie', 'vier', 'vijf']
        val = tree.list_to_tree(testtree)

        self.assertEqual(val, '(een,(twee,(drie,(vier,vijf))));', 'list to tree: fout!')