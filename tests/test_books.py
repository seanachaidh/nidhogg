import os, unittest
from nidhogg.utils.ebooks import EPUB, EbookCorpus
from nidhogg.utils.tree import better_neighbour_joining
import dendropy


class TestEbook(unittest.TestCase):
    def setUp(self):
        self.epub = EPUB.load_book(os.path.join('testdata', 'hemel_en_hel.epub'))
        #We laden ook nog een engels boek
        self.eng_epub = EPUB.load_book(os.path.join('testdata', 'wizard.epub'))
        self.hollow_epub = EPUB.load_book(os.path.join('testdata', 'hollow.epub'))

        # Example nexus file
        taxa = dendropy.TaxonNamespace()
        self.chars = dendropy.DnaCharacterMatrix.get(
            path="myfirstdna.nexus",
            schema="nexus",
            taxon_namespace=taxa
        )

    def test_data(self):
         # Hier gaan we nakijken of dat de juiste auteur en titel van het boek gevonden is
         self.assertEqual(self.epub.meta.author, 'Jón Kalman Stefánsson', 'Auteur niet juist')
         self.assertEqual(self.epub.meta.title, 'Hemel En Hel', 'Titel niet juist')

         chapterline = self.epub.toc.chapters[0][0]
         self.assertEqual(chapterline, 'Jón  Kalman Stefánsson', 'Hoofdstukken niet juist')

    def test_joining(self):
        testretval = better_neighbour_joining(self.chars)
        finaltree = testretval[0].to_newick_tree()

        dendrotree = dendropy.Tree.get(data=finaltree, schema='newick')
        ascii_tree = dendrotree.as_ascii_plot()

        with open('testresult.txt', 'w') as f:
            print(ascii_tree, file=f)

        self.assertEquals(True, True, msg='better neighbour joining')

    def test_tokenization(self):
        tokens = self.epub.tokens
        to_test = [t[0] for t in tokens[:3]]
        has_to_be = 'Jón Kalman Stefánsson'.split(' ')
        self.assertListEqual(to_test, has_to_be, 'Tokenization niet gelukt')

    def test_eng_tokenization(self):
        chapter = self.eng_epub.toc.chapters[4]

        for c in self.eng_epub.toc.chapters:
            self.assertNotEqual(len(c), 0, 'Er is een leeg hoofdstuk')

        print(chapter)
        print("Aantal hoofdstukken:", len(self.eng_epub.toc.chapters))
    
    #misschien hiervoor een aparte testverzameling maken?
    def test_corpus(self):
        corpus = EbookCorpus('corpus')
        titels = corpus.all_titles()
        for t in titels:
            print(t)
    def test_hollow(self):
        self.assertTrue(len(self.hollow_epub.toc.chapters) > 1, msg='Hoofstukken van sleepy hollow kloppen niet')
