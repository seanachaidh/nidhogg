import unittest
from nidhogg.utils import general

class TestGeneral(unittest.TestCase):
 
    def test_split_floats(self):
        testval = general.split_up(0, 1, 4)
        expected = [(0, 0.25), (0.25, 0.50), (0.50, 0.75), (0.75, 1)]
        self.assertEqual(list(testval), expected)