import unittest
from nidhogg.utils import metrics

from dendropy import Tree, Node

from nidhogg.utils.ebooks import EbookCorpus

class TestMetrics(unittest.TestCase):
    def setUp(self):
        self.corpus = EbookCorpus('corpus')
        self.testdree = Tree.get(path = "hello_world.nexus", schema="nexus", tree_offset = 0)

    def test_hello(self):
        self.assertEqual(True, True)

    def test_pure(self):
        met = metrics.Purity('hello_world.nexus', 3, self.corpus, 'author')
        print('Resultaat test: %f' % met.calculate_metric())
        self.assertEqual(True, True, msg='placeholder')

    def test_level_iterator(self):
        all_leafs = self.testdree.leaf_nodes()
        visited = []
        iterator = metrics.level_iterator(self.testdree, 3)
        for i in iterator:
            i_leafs = i.leaf_iter()
            for leaf in i_leafs:
                self.assertFalse(leaf in visited)
                visited.append(leaf)
        self.assertFalse(any(leaf not in visited for leaf in all_leafs))

    def test_path_genator(self):
        rootnode = self.testdree.seed_node
        retval = metrics.node_to_lists(rootnode)
        print(retval)
        self.assertEqual(True, True, msg="placeholder")

    def test_rand_measure(self):
        mymetric = metrics.RandMeassure('hello_world.nexus', 3, self.corpus, 'author')
        retval = mymetric.calculate_metric()
        print('Metric result:', retval)
        self.assertEqual(True, True)