import unittest, os
from nidhogg.utils.ebooks import EPUB

from nidhogg.genes.bookfreq import BookFreq
from nidhogg.genes.bookfreq import DNAMapCreator
from nidhogg.genes.bookfreq import DNAMapLoader

from nidhogg.utils.wordlist import WordList

class TestFreq(unittest.TestCase):
    def setUp(self):
        self.wlist = WordList('word_data')
        self.epub  = EPUB.load_book(os.path.join('testdata', 'wizard.epub'))
        self.hollow = EPUB.load_book(os.path.join('testdata', 'hollow.epub'))
        self.epub.remove_all_with_wordlist(self.wlist)

        self.freq = BookFreq.load_freq(self.epub)
        self.hollow_freq = BookFreq.load_freq(self.hollow)
        self.dna = DNAMapCreator('small_corpus', wlist=self.wlist)

    def test_words(self):
        self.assertNotEqual(len(self.freq.syno_freq), 0)
    
    def test_topic_save(self):
        self.dna.safe_topic_map('testfile.dna')
        loaded_dna = DNAMapLoader('testfile.dna')

        for i in range(len(self.dna.fused_freqs)):
            current_orig = self.dna.fused_freqs[i]
            current_saved = loaded_dna.topics[i]

            self.assertEqual(current_orig[0], current_saved[0], 'Saved wordset niet in orde')
            self.assertEqual(current_orig[1], current_saved[1], 'Saved frequency niet in orde')

    def test_dna_creation(self):
        print("creating dna for:", self.freq.freqtitle)
        loaded_dna = DNAMapLoader('testfile.dna')
        mydna = loaded_dna.create_dna(self.freq)
        hollowdna = loaded_dna.create_dna(self.hollow_freq)
        print(mydna)
        print(hollowdna)

