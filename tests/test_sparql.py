import unittest,pdb
from nidhogg.utils.sparql import DBPedia, ResourceRetriever

class TestSPARQL(unittest.TestCase):
	def setUp(self):
		self.connection = DBPedia()
		self.connection.vars.append('x')
	def test_first_query(self):
		result = self.connection.ask(('?x', 'rdfs:label', '"Lion"@en'))
		print(result)
		
		self.assertEqual(True, True)

	def test_info_retriever(self):
		lions = ResourceRetriever('http://dbpedia.org/resource/Lion')
		testinfo = int(lions.info['dbpedia:ontology/wikiPageID'])
		
		self.assertEqual(testinfo, 36896, msg='information retrieval not succeeded')
		
