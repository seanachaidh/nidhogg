import os, unittest
from nidhogg.utils.ebooks import EPUB
from nidhogg.utils.wordlist import WordList

class TestWordList(unittest.TestCase):
    def setUp(self):
        self.wlist = WordList('word_data')
        # Ik gebruik hiervoor bewust een engelstalig boek
        self.ebook = EPUB(os.path.join('testdata', 'wizard.epub'))
    def test_list_length(self):
        self.assertNotEqual(len(self.wlist.words), 0)
    def test_token_search(self):
        toks = self.wlist.search_words(self.ebook)
        self.assertNotEqual(len(toks), 0)
    
    def test_contains_tokens(self):
        self.assertTrue('the' in self.wlist.words)

    def test_token_removal(self):
        #to_remove = self.wlist.search_words(self.ebook)
        #self.ebook.remove_tokens(to_remove)
        self.ebook.remove_all_with_wordlist(self.wlist)
        for t in self.ebook.tokens:
            self.assertNotEqual(t[0], 'of')
