import unittest, os
from nidhogg.utils.MbtPipe import MBTPipe

class TestPipe(unittest.TestCase):
    def setUp(self):
        self.pipe = MBTPipe(os.path.join('notebooks', 'test_tokens.frog'),
            os.path.join('notebooks', 'output_tag', 'output.frog.settings'))
    def test_output(self):
        self.pipe.encode()
        testval = self.pipe.putout()
        self.assertNotEqual('', testval, 'Waardes encoding niet juist')
