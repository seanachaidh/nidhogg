import unittest, os, pdb
from copy import deepcopy
from operator import itemgetter

from nidhogg.utils import dna
from nidhogg.utils import tree
from nidhogg.utils.ebooks import EbookCorpus
from nidhogg.genes import gender
from nidhogg.utils.grapher import create_distribution_graph


class TestDNA(unittest.TestCase):

	def setUp(self):
		self.testalpha = ['A', 'B', 'C', 'D']

		self.childA = dna.DNATreeElement(data='A')
		childX = dna.DNATreeElement(data='X', parent=self.childA)
		childB = dna.DNATreeElement(data='B', parent=childX)
		childC = dna.DNATreeElement(data='C', parent=childX)
		childY = dna.DNATreeElement(data='Y', parent=self.childA)
		childD = dna.DNATreeElement(data='D', parent=childY)
		childE = dna.DNATreeElement(data='E', parent=childY)

		childY.children = [childD, childE]
		childX.children = [childB, childC]
		self.childA.children = [childX, childY]

	def test_generate_neighbourhood(self):
		expected = [
			'AB', 'AC','AD',
			'BA', 'CA','DA'
		]
		got = dna.generate_gene_neighbourhood('AA', self.testalpha)
		for g in got:
			self.assertTrue(g in expected, msg='foutje: {var} niet gevonden'.format(var=g))

	def test_depth(self):
		expected = 3
		got = self.childA.depth
		self.assertEqual(got, expected, 'Diepte niet correct')

	def test_get_all_leaves(self):
		children = self.childA.get_all_leaves()
		leaves_data = [x.data for x in children]
		expected = ['B', 'C', 'D', 'E']
		self.assertEqual(expected, leaves_data, msg='Kinderen niet goed gekregen')

	def test_get_branches(self):
		expected = [
			['B', 'X', 'A'],
			['C', 'X', 'A'],
			['D', 'Y', 'A'],
			['E', 'Y', 'A']
		]
		res = self.childA.get_branches()
		got = [[y.data for y in x] for x in res]
		self.assertEqual(expected, got, msg='Get branches werkte niet goed')

	def test_treesearch(self):
		pathlist = []
		def add_to_path(x: dna.DNATreeElement):
			pathlist.append(x.data)
			return True #Just continue execution
		expected = ['A', 'X', 'B', 'C', 'Y', 'D', 'E']

		self.childA.dfs(add_to_path)
		self.assertEquals(pathlist, expected, msg='Diepte eerst niet gelukt')
	
	def test_generate_dna_tree(self):
		self.childA.save_tree("hello_world.xml")
		self.assertEqual(True, True)
	
	def test_dna_combo(self):
		mygenes = dna.generate_combo(2)
		for i in range(len(mygenes)):
			if i > 0:
				self.assertEqual(tree.hamming_distance(mygenes[i], mygenes[i-1]), 1, msg='Hamming klopt niet')
			self.assertEqual(len(mygenes[0]), 2, msg='lengte gen klopt niet')
		for j in reversed(range(len(mygenes))):
			expected_hamming = (len(mygenes) -1) - j

			got_hamming = tree.hamming_distance(mygenes[j], mygenes[-1])

			self.assertEqual(expected_hamming, got_hamming, msg='hamming {0} klopt niet'.format(j))
	

	def test_conversion(self):
		loaded_genes = dna.DNATreeLoader(os.path.join('dnatree', 'testtree.xml'))
		expected = 'CA'
		got = loaded_genes.convert(1, 0.5)
		self.assertEqual(got, expected, msg='conversie niet gelukt')

	def test_graph_drawing(self):
	   self.childA.generate_networkx_graph("hello_world.png") # only side effects
	   self.assertEqual(True, True)
	
	def test_generate_fulltree(self):
		mytree = dna.generate_dna_tree(2)
		mytree.save_tree("nicetree.xml")
		mytree.generate_networkx_graph("nicetree.png")
		self.assertEqual(True, True)
		
	def test_generate_dna_distribution(self):
		bigtreeDNA = dna.DNATreeLoader(os.path.join('encoding_trees', 'bigtree.xml'))
		distr = bigtreeDNA.generate_dna_distribution(1, gender.Gender, 'smallcorpus')
		create_distribution_graph(distr, os.path.join('graphs', 'distr_graph.png'))

	def test_dna_leveling(self):
		mytree = dna.DNATreeLoader(os.path.join('encoding_trees', 'nicetree.xml'))
		corpus = EbookCorpus('corpus')
		genes = map(itemgetter(1), corpus.apply_genes([('Gender', gender.Gender)]))

		tree2 = deepcopy(mytree)

		tree2.looped_train_encoding(5, 1, genes, 0.5, 0.1)
		tree2.save_copy(os.path.join("datadir", "tested_level.xml"))
