#!/bin/sh

to_train=( encoding_trees/gender_tree.xml encoding_trees/vocvar_tree.xml encoding_trees/sentiment_tree.xml )
genes=( "Gender" "VocVar" "Sentiment" )

for j in {0..2}
do
    for i in {1..10}
    do
        name=${to_train[j]}
        gene=${genes[j]}
        echo "training $name voor $i keer"
        python -m nidhogg balance-tree --corpus fantasy_corpus --gene ${gene} --output ${name} --tree $name
    done
    
done


