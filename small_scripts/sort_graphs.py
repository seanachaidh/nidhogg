#!/usr/bin/env python3
import os, getopt, sys
import pandas as pd
import numpy as np

args = sys.argv[1:]
opt_list, opt_args = getopt.getopt(args, 'x', ['datafile=', 'output=', 'fieldname=', "extension="])

opt_dict = dict(opt_list)

to_sort = opt_dict['--output']
datafile = opt_dict['--datafile']
field = opt_dict['--fieldname']
extension = opt_dict.get('--extension', '.png') #PNG is the default value

print("Field: %s" % field); print("Datafile: %s" % datafile); print("Output: %s" % to_sort)

# os.chdir(to_sort)
datafile = pd.read_csv(datafile, sep="|")
unique_labels = datafile.label.unique()

print("Using the unique labels: %s" % unique_labels)

for i in unique_labels:
    newdir = os.path.join(to_sort, i)
    if not os.path.isdir(newdir):
        print("creating unique directory: %s" %i)
        os.mkdir(newdir)
for row in datafile.itertuples():
    filename = getattr(row, field)
    label = row.label
    if filename.endswith('.epub'):
        new_filename = '.'.join(os.path.basename(filename).split('.')[:-1]) # Een grep expressie is overduidelijk beter hiervoor
    else:
        new_filename = filename

    full_path = os.path.join(to_sort, new_filename) + extension
    full_new_path = os.path.join(to_sort, label, new_filename) + extension

    print("Moving %s to %s" % (full_path, full_new_path))
    os.rename(full_path, full_new_path)
