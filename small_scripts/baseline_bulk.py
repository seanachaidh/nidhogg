#!/usr/bin/env python
from getopt import getopt
from sys import argv
from os.path import join, exists
from os import listdir, makedirs
from nidhogg.commands.plotcommands import NewMetricSummary,CompleteAnalysis

opts = dict(getopt(argv[1:], 'i:o:')[0])

inputdir = opts['-i']
outputdir = opts['-o']

if not exists(outputdir):
    makedirs(outputdir)
for i in range(50):
    print("Performing run %d" % i)
    run_name = "run_" + str(i)
    output_run = join(outputdir, run_name)
    for j in listdir(inputdir):
        print("performing %s for %d" % (j, i))
        input_run = join(inputdir, j)
        analysis = CompleteAnalysis([
            "--input", 'epub:'+ input_run,
            "--output", join(output_run, j),
            "--genes", "TfIdf",
            "--name", "output_%s_%d" % (j, i)
        ])
        analysis.execute()
    metrics_command = NewMetricSummary([
        "--resultfolder", output_run,
        "--cdhit", join(output_run, "metrics_cdhit.csv"),
        "--omega", join(output_run, "metrics_omega.csv")
    ])
    metrics_command.execute()
