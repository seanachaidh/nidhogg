import getopt, rdflib, sys, os, requests
import pandas as pd

opt_list = sys.argv[1:]

# THE MIRROR VARIABLE. POSSIBLY MAKE A PARAMETER FROM THIS
mirror = "http://gutenberg.readingroo.ms/cache/generated/"

longs = ["input=", "output=", "tocsv="]
shorts = 's'

opts = getopt.getopt(opt_list, shorts, longs)[0]
opt_dict = dict(opts)

input_file = opt_dict["--input"]
output_dir = opt_dict["--output"]
to_csv = opt_dict.get("--tocsv", None)
should_sort = ('-s' in opt_dict)


if not os.path.isdir(output_dir):
    os.makedirs(output_dir)

#OPEN AND READ QUERY
with open(input_file, 'r') as f:
    query_buffer = f.read()

#Connecting to the graph
graph = rdflib.graph.Graph(store="SPARQLStore", identifier="http://gutenberg.org")
graph.open("http://localhost:8890/sparql")

#EXECUTING THE QUERY
result = graph.query(query_buffer)
result_table = pd.DataFrame.from_records([(item["title"], item['author'], item["ebook"]) for item in result], columns=['title', 'author', 'ID'])
print("about to download")
print(result_table.to_string())


for title, author, identifier in result_table.itertuples(index=False):
    fetched_id = identifier.split('/')[-1]
    print("Downloading %d with title %s" % (int(fetched_id), title))
    final_url = mirror + fetched_id + '/pg' + fetched_id + '.epub'
    print("Downloading file: %s" % final_url)
    
    output_file_name = fetched_id + '.epub'
    if should_sort:
        if not os.path.isdir(os.path.join(output_dir, author)):
            print("creating folder for author %s" % author)
            os.makedirs(os.path.join(output_dir, author))
        output_file = os.path.join(output_dir, author, output_file_name)
    else:
        output_file = os.path.join(output_dir, output_file_name)
    print("Output file %s" % output_file)
    r = requests.get(final_url)

    with open(output_file, 'wb') as f:
        f.write(r.content)

if not to_csv is None:
    print("Requesting output: %s" % to_csv)
    result_table.to_csv(to_csv, index=False, sep='|')

print("DONE!")
