#!/usr/bin/env python3

import pandas as pd
import getopt, sys, os

from shutil import copyfile

def convert_id(iden: str, prefix: str) -> str:
    return os.path.join(prefix, iden)

def perform_copy(idset):
    for tr in idset:
        filename = os.path.basename(tr)
        original_filename = os.path.join(original, filename)

        print("Performing copy %s -> %s" % (original_filename, tr))
        copyfile(original_filename, tr)

args = sys.argv[1:]

long_opts = ["input=", "percentage=", "testname=", "trainname=", "original="]
short_opts = 's'

opts = getopt.getopt(args, short_opts, long_opts)[0]
opt_dict = dict(opts)
input_file = opt_dict["--input"]
percentage = float(opt_dict["--percentage"])

original = opt_dict["--original"]
test_name = opt_dict["--testname"]
train_name = opt_dict["--trainname"]

output_csv = ('-s' in opt_dict)

print("splitsing %s and %s into %s" % (input_file, test_name, train_name))

frame = pd.read_csv(input_file, sep='|')

authors = frame.author.unique()
counts = frame["author"].value_counts()[0]

# test_size = round(counts*percentage)
# train_size = abs(counts-test_size)

for a in authors:
    subset = frame[frame["author"] == a]
    test_set = subset.sample(frac=percentage)
    train_set = pd.concat([subset, test_set]).drop_duplicates(keep=False)

    if not os.path.isdir(test_name):
        print("Making folder for testset: %s" % test_name)
        os.makedirs(test_name)
    if not os.path.isdir(train_name):
        print("Making folder for trainset: %s" % train_name)
        os.makedirs(train_name)
    
    train_ids = (convert_id(x, train_name) for x in train_set["filename"])
    test_ids = (convert_id(y, test_name) for y in test_set["filename"])

    perform_copy(test_ids)
    perform_copy(train_ids)
