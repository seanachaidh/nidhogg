#!/usr/bin/env python
import sys

import numpy as np

from Bio import SeqIO
from Bio import SeqRecord
from Bio.Seq import Seq

if not len(sys.argv) == 3:
    print("This needs at least an input and an output file")
    sys.exit(1)

infile = sys.argv[1]; outfile = sys.argv[2]

print("compressing %s to %s " % (infile, outfile))

def create_record(input_seq, input_name, desc):
    seqstring = ''.join(input_seq)
    return SeqRecord.SeqRecord(Seq(seqstring), id=input_name, description=desc)

fastafile = list(SeqIO.parse(infile, "fasta"))

datalist = [list(x.seq) for x in fastafile]
names = [x.id for x in fastafile]
descriptions = [x.description for x in fastafile]

matrix = np.array(datalist)

print("old:"); print(matrix)

comparison = (matrix == matrix[0,:]) # Vergelijken met de eerste regel
comparison_array = np.all(matrix == matrix[0,:], axis=0)
compressed_matrix = matrix[:,~comparison_array]

print("new:"); print(compressed_matrix)

gain = len(matrix.T) - len(compressed_matrix.T)
print("Gain: %d" % gain)

seq_iterator = map(create_record, compressed_matrix, names, descriptions)
with open(outfile, "w") as f:
    SeqIO.write(seq_iterator, f, 'fasta')
