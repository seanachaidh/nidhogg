#!/usr/bin/Rscript --vanilla
library("getopt");

spec = matrix(c("input", "i", 1, "character", "output", "o", 1, "character"), byrow=TRUE, ncol=4);
opts = getopt(spec);

my_input = opts$input;
my_output = opts$output;
filter_data <- function(input, output) {
  data = read.csv(input, sep="|");
  filtered_data = data[data$percent == 100.0,];
  filtered_data = filtered_data[-c(1:3)];
  filtered_data = filtered_data[, c(2,3,1,4)];
  
  write.table(filtered_data, file = output, sep = "|", row.names = FALSE);
  return(0);
}
filter_data(my_input, my_output);