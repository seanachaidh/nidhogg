#!/usr/bin/env python
from sys import argv
from os.path import join as path_join
from os.path import exists as path_exists
from os.path import basename as filename
from os.path import dirname

from os import makedirs as create_directories
from os import listdir
from re import sub as substitute
from shutil import copyfile
from getopt import getopt
from ebooklib.epub import read_epub

def fetch_author(book):
    epub = read_epub(book)
    author = epub.get_metadata('DC', 'creator')[0][0]
    return substitute(r'[^a-zA-Z0-9]', '', author)

def create_folder(folder):
    if not path_exists(folder):
        create_directories(folder)

opts = dict(getopt(argv[1:], 'i:o:')[0])
inputdir = opts['-i']
outputdir = opts['-o']

if not path_exists(outputdir):
    print("path %s does not exist. Creating" % outputdir)
    create_directories(outputdir)
to_handle = (path_join(inputdir, x) for x in listdir(inputdir))
books_author = ((x, fetch_author(x)) for x in to_handle)

for b, a in books_author:
    create_folder(path_join(outputdir, a))
    book_file = filename(b)
    final_dir = path_join(outputdir, a, book_file)
    print('copying %s -> %s' % (b, final_dir))
    copyfile(b, final_dir)
