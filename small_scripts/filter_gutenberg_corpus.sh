#!/bin/bash

script_dir=$(dirname $0)

while getopts "i:o:" arg; do
    case "$arg" in
        i)
            indir=${OPTARG}
            ;;
        o)
            outdir=${OPTARG}
            ;;
        *)
            echo "Verkeerd argument ${arg}"
            exit 1
            ;;
    esac
done

if [ ! -e ${outdir} ]; then
    echo "Output dir ${outdir} does not exist. Creating"
    mkdir ${outdir}
fi

to_filter=$(ls ${indir})
for book in ${to_filter}; do
    output_file=$(printf %s/%s ${outdir} ${book})
    echo "Filtering ${book} to ${output_file}"
    python ${script_dir}/filter_gutenberg.py --input $(printf %s/%s ${indir} ${book}) --output ${output_file}
done
