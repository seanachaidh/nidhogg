#!/bin/sh

if [ ! -e dataset ]; then
    mkdir dataset
fi

for x in $(seq 30); do
    echo "Creating dataset ${x}"
    final_folder=$(printf "dataset/corpus_%s" ${x})
    echo "Creating corpus ${final_folder}"

    python download_corpus.py --input "sparql/first_corpus.sparql" --output ${final_folder}
done
