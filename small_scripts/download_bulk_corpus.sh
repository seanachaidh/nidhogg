#!/usr/local/bin/bash

function usage() {
    echo "$0 -i <input> -o <output> -x <min> -y <max> -s <step>"
    exit 1
}

script_dir=$(dirname "$0")

while getopts i:o:x:y:s: opt; do
    case "${opt}" in
        i)
            echo "setting input to ${OPTARG}"
            i=${OPTARG}
            ;;
        o)
            echo "setting output to ${OPTARG}"
            o=${OPTARG}
            ;;
        x)
            echo "setting minimum to ${OPTARG}"
            x=${OPTARG}
            ;;
        y)
            echo "setting maximum to ${OPTARG}"
            y=${OPTARG}
            ;;
        s)
            echo "setting steps to ${OPTARG}"
            s=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done


echo "Parsing of arguments done"
echo "reading input"
inputtext=$(cat ${i})
printf "found input:\n%s" "${inputtext}"

if [ ! -e ${o} ]; then
    echo "Output folder ${o} does not exist. Creating"
    mkdir ${o}
fi

echo "Starting iteration"
printf "x=%d\ny=%d\ns=%d\n" ${x} ${y} ${s}

for iter in $(seq ${x} ${s} ${y}); do
    echo "Downloading corpus for ${iter} authors"
    output_folder=$(printf "%s/corpus_%d" ${o} ${iter})
    echo "Setting output folder ${output_folder}"

    if [ ! -e ${output_folder} ]; then
        echo "Output folder ${output_folder} does not exist. Creating"
        mkdir ${output_folder}
    fi

    csv_file=$(printf "%s/corpus_%d.csv" "${o}" ${iter})

    formatted_text=$(printf "${inputtext}" ${iter})
    printf "Executing query:\n%s" "${formatted_text}"
    echo "${formatted_text}" > tmp.sparql
    python ${script_dir}/download_corpus.py --input "tmp.sparql" --output "${output_folder}" --tocsv "${csv_file}"
done

echo "Done!"