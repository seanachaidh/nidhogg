#!/bin/sh

if [ ! $# -eq 2 ]; then
    echo "not enough arguments"
    exit 1
fi

if [ ! -d $2 ]; then
    echo "Creating output directory $2"
    mkdir $2
fi

input_list=$(find $1 -name "*.epub")

for i in ${input_list}; do
    filename=$(basename ${i})
    newoutput=${2}/$filename
    # echo "Converting ${i} to ${newoutput}"
    python filter_gutenberg.py --input $i --output $newoutput
done
