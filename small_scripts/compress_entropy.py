#!/usr/bin/env python
import sys, getopt

import numpy as np
from Bio import SeqIO
from scipy.stats import entropy
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord

args = sys.argv[1:]
opt_list = getopt.getopt(args, 'ev:', ['input=', 'output='])[0]
opt_dict = dict(opt_list)

infile = opt_dict['--input']
output = opt_dict['--output']
value = int(opt_dict['-v'])
use_ent = ('-e' in opt_dict)

with open(infile, "r") as f:
    data = list(SeqIO.parse(f, "fasta"))

names = [rec.id for rec in data]
descriptions = [rec.description for rec in data]

data_seq = [list(rec.seq) for rec in data]
matrix = np.array(data_seq)

print("Old matrix"); print(matrix)

column_counts = []

for column in np.transpose(matrix):
    counter = [0] * 4
    for d in column:
        if d == 'A':
            counter[0] = counter[0] + 1
        elif d == 'C':
            counter[1] = counter[1] + 1
        elif d == 'G':
            counter[2] = counter[2] + 1
        elif d == 'T':
            counter[3] = counter[3] + 1
    counter.remove(0); counter.remove(0) # Remove all zeroes
    column_counts.append(counter)

count_matrix = np.array(column_counts)

# sums = [sum(x) for x in np.transpose(matrix)]
if use_ent:
    to_check = [entropy(i, base=2) for i in count_matrix]
else:
    to_check = [min(i) for i in count_matrix]
mask = [(x >= value) for x in to_check]

new_matrix = matrix[:, mask]
print("New matrix"); print(new_matrix)

gain = len(np.transpose(matrix)) - len(np.transpose(new_matrix))
percent_gain = (len(matrix) / gain) * 100

print("Feature gain %d; In percent %.2f" % (gain, percent_gain))

seqiter = (SeqRecord(Seq(''.join(sequence)), id=name, description=desc) for name, sequence, desc in zip(names, new_matrix, descriptions))
with open(output, 'w') as f:
    SeqIO.write(seqiter, f, 'fasta')
