#!/usr/bin/RScript --vanilla

library('xtable')
library('getopt')

spec = matrix(c(
	'input', 'i',  1, 'string',
	'output', 'o', 1, 'string',
), byrow=TRUE, ncol=4);

opt = getopt(spec)
