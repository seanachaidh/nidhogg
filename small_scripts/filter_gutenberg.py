#!/usr/bin/env python

import sys, getopt
from copy import deepcopy
import ebooklib
from ebooklib import epub
from lxml import etree

start_id = "id00000"
end_id = "id00010"
header_class = "pgheader"

counter = 0

def convert_element_to_text(element):
    result = element.xpath("./child::text()")
    return ' '.join(result)

def create_empty_copy(book):
    new_book = deepcopy(book)
    new_book.items = [x for x in book.items if not isinstance(x, epub.EpubHtml)]
    return new_book

def update_content(html_file, new_content):
    new_html_file = deepcopy(html_file)
    new_html_file.set_content(new_content)
    return new_html_file

def filter_document(document_text):
    global counter

    content = document_text.get_content()
    html_root = etree.fromstring(content, parser=etree.HTMLParser())
    
    begin_marker = html_root.xpath(".//div[contains(@class, 'pgmonospaced pgheader')]")
    if not begin_marker:
        begin_marker = html_root.xpath(".//p[contains(., '***') and contains(., 'PROJECT GUTENBERG')]")
        if not begin_marker:
            begin_marker = []
    
    for r in begin_marker:
        counter = counter + 1
        if counter == 1:
            # the beginning
            siblings = r.xpath("./preceding-sibling::*")
        elif counter == 2:
            # the end
            siblings = r.xpath("./following-sibling::*")
        for s in siblings: r.getparent().remove(s)
        r.getparent().remove(r)
    
    new_document = update_content(deepcopy(document_text), etree.tostring(html_root, encoding='UTF-8'))
    return new_document
    
longargs = ["input=", "output="]
shortargs = ""
cmd_line = sys.argv[1:]

opts = getopt.getopt(cmd_line, shortargs, longargs)[0]
opt_dict = dict(opts)

infile = opt_dict["--input"]
outfile = opt_dict["--output"]

print("Converting: %s -> %s" % (infile, outfile))

epub_file = epub.read_epub(infile)
html_pages = epub_file.get_items_of_type(ebooklib.ITEM_DOCUMENT)

new_documents = []
for html in html_pages:
    docfile = filter_document(html)
    new_documents.append(docfile)
    if counter == 2: break
    
if counter != 2:
    raise RuntimeError("Mislukt %s" % infile)
    
new_epub_file = create_empty_copy(epub_file)

for x in new_documents:
    new_epub_file.add_item(x)

epub.write_epub(outfile, new_epub_file)
print("done")
