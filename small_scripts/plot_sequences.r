#!/usr/bin/Rscript --vanilla

library("bio3d");
library("DECIPHER");
library("Biostrings");
library("getopt");


to_sequence_list <- function(mymatrix) {
  retval <- c();
  for(i in 1:nrow(mymatrix)) {
    current <- mymatrix[i,];
    tmpseq <- paste(current, collapse = '');
    retval <- c(retval, tmpseq);
  }
  return(Biostrings::DNAStringSet(retval));
}

#First we parse the command line arguments
my_spec <- matrix(c("fastafile", "f", 1, "character", "datafile", "d", 1, "character", "label", "l", 1, "character"), byrow=TRUE, ncol=4);
opts <- getopt(my_spec);

print(paste("working with fastafile", opts$fastafile, "on datafile", opts$datafile, "for label", opts$label));

clusters <- read.csv(opts$datafile, sep='|', header=TRUE);
data <- read.fasta(opts$fastafile);
ids <- data[["ids"]];
ali <- data[["ali"]];

cluster_labels = unique(clusters$label);

for (i in 1:length(cluster_labels)) {
	current_label <- as.character(cluster_labels[i]);
	current_cluster = clusters[cluster$label == current_label];
	current_title_ids <- which(ids %in% current_cluster$title);
	sequences <- ali[current_title_ids,];
	
	mylist <- to_sequence_list(sequences);
	

}

