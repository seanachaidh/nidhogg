#!/usr/bin/env python

from sys import argv, exit
from getopt import getopt
from os.path import join as pathjoin
from os.path import exists as path_exist
from os.path import basename as filename
from os import listdir
from nidhogg.commands.plotcommands import CompleteAnalysis


opts = dict(getopt(argv[1:], 'i:')[0])
inputdir = opts['-i']

if not path_exist(inputdir):
    print("input dir %s does not exist" % inputdir)
    exit(1)
paths = (pathjoin(inputdir, x) for x in listdir(inputdir))

for x in paths:
    print("processing corpus %s" % x)
    cname = filename(x)
    final_result = pathjoin('results', filename(inputdir), cname)
    analysis = CompleteAnalysis([
        "--input", 'epub:' + x,
        "--output", final_result,
        "--genes", "TfIdf",
        "--name", "Parametric " + cname,
        "--tcount", "10"
    ])
    analysis.execute()
