import sys, os, re
import ebooklib
from ebooklib import epub as bookreader
from bs4 import BeautifulSoup
from selectolax.parser import HTMLParser

def clean_line(textline):
    print('\r' + len(textline) * ' ', end='\r')

def has_to_filter(element):
    if element.parent.name in ['style', 'script', '[document]', 'head', 'title']:
        return False
    elif re.match('<!--.*-->', str(element.encode('utf-8'))):
        return False
    elif not element.strip():
        return False
    return True

def get_text_selectolax(epubitem):
    html = epubitem.get_body_content()
    tree = HTMLParser(html)

    if tree.body is None:
        return None

    for tag in tree.css('script'):
        tag.decompose()
    for tag in tree.css('style'):
        tag.decompose()

    text = tree.body.text(separator='\n')
    return text

def flatten_document(item):
    # mybody_content = item.get_body_content().decode('utf-8')
    mybody_content = item.get_body_content()
    soup = BeautifulSoup(mybody_content, features='lxml')
    data = soup.findAll(text=True)

    # htmlfile = etree.HTML(mybody_content)
    # result_tmp = filter(has_to_filter, data)
    # result = list(map(lambda x: x.strip(), result_tmp))

    return data

def process_file(filename):
    try:    
        epub = bookreader.read_epub(os.path.join(infolder, f))

        author = epub.get_metadata('DC', 'creator')
        title = epub.get_metadata('DC', 'title')
        publisher = epub.get_metadata('DC', 'publisher')   
        
        myitems = [x for x in epub.get_items() if x.get_type() == ebooklib.ITEM_DOCUMENT]
        texts = [get_text_selectolax(item) for item in myitems]
    except:
        print("Exception occured on {}".format(f))
        print(f, file=loghandle)
    else:
        print("Found:", f)

if not len(sys.argv) == 3:
    print("this needs an input and output argument argument")
    sys.exit(1)

logfile = sys.argv[2]
infolder = sys.argv[1]

loghandle = open(logfile, 'w')

print('running with input={} and output={}'.format(infolder, logfile))
filelist = os.listdir(infolder)

print('')
current_line = ''

total = len(filelist)
for i, f in enumerate(filelist):
    current_precent = int(round((i/total) * 100))
    clean_line(current_line)
    current_line = '{}%: {}'.format(current_precent, f)
    print(current_line, end='\r')
    process_file(f)


loghandle.close()
