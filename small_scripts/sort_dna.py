
import numpy as np
import pandas as pd
from Bio import SeqIO
from itertools import combinations
from copy import deepcopy
from operator import itemgetter

from Bio import pairwise2
import os

os.chdir('small_scripts')
records = list(SeqIO.parse("fastafile_compr.fasta", "fasta"))

def ind_to_records(idx, records):
    return [records[i] for i in idx]

def records_to_matrix(records) -> pd.DataFrame:
    counter = 0

    reclen = len(records)
    recrange = list(range(len(records)))

    result = np.zeros((reclen, reclen))
    
    for i in recrange:
        percentage = (counter/(reclen**2)*100)
        print("Creating similarity matrix: %d%%" % percentage, end='\r')
        for j in recrange:
            rec1 = records[i]
            rec2 = records[j]

            result[i][j] = hamming(rec1.seq, rec2.seq)
            counter = counter + 1
    print("\ndone!")
    return pd.DataFrame(data=result, index=list(range(len(records))), columns=list(range(len(records))))

def hamming(first, second):
    return sum(map(int, map(lambda x, y: x != y, first, second)))

def iter_matrix(matrix: pd.DataFrame):
    x, y = matrix.shape
    for i in range(x):
        for j in range(y):
            yield (i, j)

def solve(frame: pd.DataFrame):

    print("Begin frame:\n%s" % frame)

    matrix_frame = deepcopy(frame)
    result = []
    max_item = max([((i,j), matrix_frame[i][j]) for i,j in iter_matrix(matrix_frame)], key=itemgetter(1))

    result.append(max_item[0][0])
    result.append(max_item[0][1])

    print("Removing %d" % result[0])

    matrix_frame.drop(result[0], axis=0, inplace=True)
    matrix_frame.drop(result[0], axis=1, inplace=True)
    print("frame:\n%s" % matrix_frame)
    
    current = result[1]

    row = False

    for _ in range(frame.shape[0]-2):
        if row:
            newarr = matrix_frame.loc[:,current]
        else:
            newarr = matrix_frame.loc[current,:]
        newcurr = newarr.idxmax()

        print("Removing %d" % current)

        matrix_frame.drop(current, axis=0, inplace=True)
        matrix_frame.drop(current, axis=1, inplace=True)

        print("frame:\n%s" % matrix_frame)
        
        result.append(newcurr)
        current = newcurr

        row = (not row)
    return result

matrec = records_to_matrix(records)

my_res = solve(matrec)

print("final result %s" % my_res)
