#!/usr/bin/env python
import sys, getopt
import pandas as pd
from ete3 import Tree, NodeStyle, TreeStyle, TreeNode

def get_author(id, df: pd.DataFrame):
    row_author = df.loc[id,:]
    return row_author['real_author']

opts = dict(getopt.getopt(sys.argv[1:], 't:i:o:c:', [])[0])
table = opts['-t']
treefile = opts['-i']
outputfile = opts['-o']
colors = opts['-c']

colvals = [x.strip() for x in open(colors, 'r').readlines()]
dataframe = pd.read_csv(table, sep='|', index_col='title')
authors = set(dataframe['real_author'].to_list())
chosen_colors = colvals[:len(authors)]

color_dict = dict(list(zip(authors, chosen_colors)))

treeobj = Tree(treefile, format=9)
ts = TreeStyle()
ts.show_leaf_name = False
# ts.mode = 'c'
# ts.arc_span = 180
# ts.arc_start = -180

for n in treeobj.traverse():
    if n.is_leaf():
        style = NodeStyle()
        style["shape"] = "sphere"
        style["size"] = 10
        author = get_author(n.name, dataframe)
        used_color = color_dict[author]
        style["fgcolor"] = used_color
        n.set_style(style)

treeobj.render(outputfile, tree_style=ts, dpi=300)