from nidhogg.utils.ebooks import download_corpus

if __name__ == '__main__':
    download_corpus('http://gutenberg.readingroo.ms/cache/generated', 'gutenberg_corpus')