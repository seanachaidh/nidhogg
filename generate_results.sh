#!/bin/sh

function generate_tree {
paup4a165_osx -n << EOF
execute $1;
nj treefile=$1 distance=abs append=yes;
EOF
}

python -m nidhogg create-dna --genes "Gender" --corpus "fantasy_corpus" --output "fantasy_dna.nexus" --format "nexus"
python -m nidhogg create-dna --genes "VocVar" --corpus "fantasy_corpus" --output "fantasy_dna_vocvar.nexus" --format "nexus"
python -m nidhogg create-dna --genes "Sentiment" --corpus "fantasy_corpus" --output "fantasy_dna_sentiment.nexus" --format "nexus"
python -m nidhogg create-dna --genes "LDATopic" --corpus "fantasy_corpus" --output "fantasy_dna_topics.nexus" --format "nexus"

generate_tree "fantasy_dna.nexus"
generate_tree "fantasy_dna_vocvar.nexus"
generate_tree "fantasy_dna_sentiment.nexus"
generate_tree "fantasy_dna_topics.nexus"

# python -m nidhogg metric-summary --corpus fantasy_corpus --input fantasy_dna.nexus --output paper/analyses/summary_fantasy_dna.tex --clusters "3,37,38,34,32,16"
# python -m nidhogg metric-summary --corpus fantasy_corpus --input fantasy_dna_vocvar.nexus --output paper/analyses/summary_fantasy_dna_vocvar.tex --clusters "23,67,64,58,54,44,40,38,20,6,4"
# python -m nidhogg metric-summary --corpus fantasy_corpus --input fantasy_dna_topics.nexus --output paper/analyses/summary_fantasy_dna_topics.tex --clusters "11,12,13,33,34,22"
# python -m nidhogg metric-summary --corpus fantasy_corpus --input fantasy_dna_sentiment.nexus --output paper/analyses/summary_fantasy_dna_sentiment.tex --clusters "3,55"

# Trained section
python -m nidhogg metric-summary --corpus fantasy_corpus --input fantasy_dna.nexus --output paper/analyses/summary_fantasy_dna_trained.tex --clusters "11,12,23,14,15,16,10"
python -m nidhogg metric-summary --corpus fantasy_corpus --input fantasy_dna_vocvar.nexus --output paper/analyses/summary_fantasy_dna_vocvar_trained.tex --clusters "27,57,58,50,40,34"
